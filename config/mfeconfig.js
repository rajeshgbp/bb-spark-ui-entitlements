module.exports = {
    server: {
        ENV: process.env.NODE_ENV || "development",
        PORT: process.env.PORT || 4020,
        serviceName: process.env.SERVICE_NAME || "bb-rentpay-ui-admin",
        servicePath: process.env.SERVICE_PATH || "/mfe/rpadmin"
    },
    mfeManifest: {
        appNumber: 5,
        appType: "internal-app",
        type: "MFC",
        isMain: false,
        version: "1.0.0",
        name: "bb-ui-rp-admin",
        description: "Rentap Admin info for CRE Portal",
        support: "CB CRE Digital| Team Story By JPMC",
        auth: ["client", "admin", "staff", "user"],
        routes: {
            root: {
                path: "/rpadmin",
                displayName: "Rentap Admin",
                navbarDisplayed: true,
                icon: "calculator"
            }
        }
    }
};