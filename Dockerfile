# From      registry-na.cntr.gaia.jpmchase.net/n704007/1rh7-node-10.15.3
FROM    containerregistry-na.jpmchase.net/container-sandbox/blkbrd01/1rh7-nodejs:latest
ENV     NODE_ENV=production
ENV     PORT=4020
EXPOSE  $PORT
COPY    . /src
WORKDIR /src
ENTRYPOINT  ["node", "www/server.js"]