const history = require('history').createBrowserHistory();
const createMemoryHistory = require('history').createMemoryHistory();

export default history;

export {
    createMemoryHistory
};