export const rolesData = {
    userInfo :{
        address: 'street 1 C road Texas USA',
        image:'/app/assets/rp/images/property_default_image.jpg',
    },
    "name": "Custom role",
    "accessLevel": "View Only",
    "general": {
      "addProperties": false,
      "exportData": false
    },
    "usersAndRoles": {
      "accessLevel": "View Only",
      "addEditUsers": false,
      "deleteUsers": false,
      "addEditRoles": false,
      "deleteRoles": false
    },
    "properties": {
        title: 'Properties',
        image:'/app/assets/rp/images/property_default_image.jpg',
        properties: [
            {
                id: 1,
                name: "Arrowgrove",
                entitlements: [
                    {
                        key: 'General',
                        "accessLevel": "View Only",
                        total :2,
                        selected:2,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Units',
                        "accessLevel": "View Only",
                        total :1,
                        selected:1,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Tenants',
                        "accessLevel": "View Only",
                        total :2,
                        selected:2,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Invoices',
                        "accessLevel": "View Only",
                        total :3,
                        selected:3,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Leases',
                        "accessLevel": "View Only",
                        total :4,
                        selected:4,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Accounts',
                        "accessLevel": "View Only",
                        total :1,
                        selected:1,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Reports',
                        "accessLevel": "View Only",
                        total :3,
                        selected:3,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                ]
            },
            {
                id: 2,
                name: "Arrowgrove 2",
                entitlements: [
                    {
                        key: 'General',
                        "accessLevel": "View Only",
                        total :2,
                        selected:2,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Units',
                        "accessLevel": "View Only",
                        total :1,
                        selected:1,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Tenants',
                        "accessLevel": "View Only",
                        total :2,
                        selected:2,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Invoices',
                        "accessLevel": "View Only",
                        total :3,
                        selected:3,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Leases',
                        "accessLevel": "View Only",
                        total :4,
                        selected:4,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Accounts',
                        "accessLevel": "View Only",
                        total :1,
                        selected:1,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                    {
                        key: 'Reports',
                        "accessLevel": "View Only",
                        total :3,
                        selected:3,
                        isSelected:false,
                        actions:[
                            {
                                key:'Edit property details',
                                isSelected:false,
                            },
                            {
                                key:'Delete/remove property from profile',
                                isSelected:false
                            }
                        ]
                    },
                ]
            },
        ]
    }
    
}

//Strings

//ENUM

//API URLS

