import createMuiTheme, { Theme, ThemeOptions, } from '@material-ui/core/styles/createMuiTheme';
import responsiveFontSizes from "@material-ui/core/styles/responsiveFontSizes";
import { PaletteOptions } from "@material-ui/core/styles/createPalette";
import { TypographyOptions } from "@material-ui/core/styles/createTypography";
/* eslint-disable import/no-unresolved */
import { Overrides } from "@material-ui/core/styles/overrides";
/* eslint-disable import/no-unresolved */
import { Purple, Gray, WarmGray, White, Blue, Orange, Red } from "./colors";

declare module "@material-ui/core/styles/createMuiTheme" {
	interface Theme
	{
		mainContentPadding: number;
		deviceQuery: {
			phone: string;
			tablet: string;
			laptop: string;
			deskpot: string;
			bigScreen: string;
		};
	}
	// allow configuration using createMuiTheme
	interface ThemeOptions
	{
		mainContentPadding: number;
		deviceQuery: {
			phone: string;
			tablet: string;
			laptop: string;
			deskpot: string;
			bigScreen: string;
		};
	}
}

declare module "@material-ui/core/styles/responsiveFontSizes" {
	interface Palette
	{
		tertiary?: {
			sand?: React.CSSProperties["color"];
			seal?: React.CSSProperties["color"];
			darkBlue?: React.CSSProperties["color"];
			lilacViolet?: React.CSSProperties["color"];
			darkOrange?: React.CSSProperties["color"];
			chicLime?: React.CSSProperties["color"];
			mediumSeaGreen?: React.CSSProperties["color"];
			carolinaBlue?: React.CSSProperties["color"];
			lightAquaGreen?: React.CSSProperties["color"];
			mediumAquaGreen?: React.CSSProperties["color"];
			indianRed?: React.CSSProperties["color"];
			warmGray?: React.CSSProperties["color"];
			darkPurple?: React.CSSProperties["color"];
			glacierBlue?: React.CSSProperties["color"];
			lightGray?: React.CSSProperties["color"];
			dimGrey?: React.CSSProperties["color"];
			lightPurple?: React.CSSProperties["color"];
			skyBlue?: React.CSSProperties["color"];
			crocodileGrey?: React.CSSProperties["color"];
			manatee?: React.CSSProperties["color"];
			butterFlyBushBlue?: React.CSSProperties["color"];
			wishperBlue?: React.CSSProperties["color"];
		};
		icon?: {
			primary: React.CSSProperties["color"];
			secondary: React.CSSProperties["color"];
			positive: React.CSSProperties["color"];
			negative: React.CSSProperties["color"];
			warning: React.CSSProperties["color"];
			interactive: React.CSSProperties["color"];
			white: React.CSSProperties["color"];
		};
		other: {
			primaryBackground: React.CSSProperties["color"];
			secondaryBackground: React.CSSProperties["color"];
			darkBackground: React.CSSProperties["color"];
			divider: React.CSSProperties["color"];
			stroke: React.CSSProperties["color"];
		};
	}

	interface PaletteOptions
	{
		tertiary?: {
			sand?: React.CSSProperties["color"];
			seal?: React.CSSProperties["color"];
			darkBlue?: React.CSSProperties["color"];
			lilacViolet?: React.CSSProperties["color"];
			darkOrange?: React.CSSProperties["color"];
			chicLime?: React.CSSProperties["color"];
			mediumSeaGreen?: React.CSSProperties["color"];
			carolinaBlue?: React.CSSProperties["color"];
			lightAquaGreen?: React.CSSProperties["color"];
			mediumAquaGreen?: React.CSSProperties["color"];
			indianRed?: React.CSSProperties["color"];
			warmGray?: React.CSSProperties["color"];
			darkPurple?: React.CSSProperties["color"];
			glacierBlue?: React.CSSProperties["color"];
			lightGray?: React.CSSProperties["color"];
			dimGrey?: React.CSSProperties["color"];
			lightPurple?: React.CSSProperties["color"];
			skyBlue?: React.CSSProperties["color"];
			crocodileGrey?: React.CSSProperties["color"];
			manatee?: React.CSSProperties["color"];
			butterFlyBushBlue?: React.CSSProperties["color"];
			wishperBlue?: React.CSSProperties["color"];
		};
		icon?: {
			primary: React.CSSProperties["color"];
			secondary?: React.CSSProperties["color"];
			positive?: React.CSSProperties["color"];
			negative?: React.CSSProperties["color"];
			warning?: React.CSSProperties["color"];
			interactive?: React.CSSProperties["color"];
			white?: React.CSSProperties["color"];
		};
		other: {
			primaryBackground: React.CSSProperties["color"];
			secondaryBackground: React.CSSProperties["color"];
			darkBackground: React.CSSProperties["color"];
			divider: React.CSSProperties["color"];
			stroke: React.CSSProperties["color"];
		};
	}
}

const HTML_FONT_SIZE = 16;

export function pxToRem(value: any)
{
	return `${value / HTML_FONT_SIZE}rem`;
}

export const colors = {
	primary: [
		"#3F3A69", // 0. Purple
		"#af95ce", // 1. Violet
		"#35334e", // 2. Eggplant
		"#7abac5", // 3. Glacier Blue
	],
	secondary: [
		"#ed8b00", // 0. Orange
		"#d7d2cb", // 1. Light Warm Gray
		"#564984", // 2. Dark purple
	],
	tertiary: [
		"#204d6f", // 0. Dark Blue
		"#bbaad0", // 1. Lilac Violet
		"#813d0a", // 2. Dark Orange
		"#e7d778", // 3.Chic Lime
		"#328b85", // 4. Medium Sea Green
		"#689bcf", // 5. Carolina Blue
		"#bee0de", // 6. Light Aquamarine Green
		"#3fa378", // 7. Medium Aquamarine Green
		"#db5537", // 8. Indian Red
		"#8c857b", // 9. Seal Grey
		"#E8E1D9", // 10. Sand
		"#F7F6F5", // 11. Light Gray
		"#9C97C0", // 12. Light Purple
		"#87CEEB", // Sky Blue
		"#ECEAE7", // crocodile Grey
		"#9A99A3", // manatee
		"#5B5397", // butterfly bush blue
		"#EEEDF4", // whishper blue
	]
};

const palette = {
	type: "light",
	primary: {
		main: "#3F3A69",
		light: "#af95ce",
		dark: "#35334e",
		contrastText: "rgba(255,255,255,0.6)",
	},
	secondary: {
		main: "#ed8b00",
		contrastText: "#35334e",
		light: "#f1a130",
	},
	text: {
		primary: "#35334E",
		secondary: "rgba(53,51,78,0.6)",
		disabled: "rgba(53, 51, 78, 0.38)",
	},
	icon: {
		primary: Gray[100],
		secondary: Gray[30],
		positive: Blue[60],
		negative: Red[50],
		warning: Orange[90],
		interactive: Purple[60],
		White: White,
	},
	tertiary: {
		send: "#E8E1D9",
		seal: "#8c857b",
		darkblue: "204d6f",
		lilacViolet: "#bbaad0",
		darkOrange: "#813d0a",
		chicLime: "#E7d778",
		mediumSeaGreen: "#328b85",
		carolinaBlue: "#689bcf",
		lightAquaGreen: "#bee0de",
		mediumAquaGreen: "3fa378",
		indianRed: "#db5537",
		warmGray: "#d7d2cb",
		darkPurple: "#564984",
		glacireBlue: "#7abac5",
		lightGray: "#F7F6F5",
		dimGray: "rgba(51,51,51, .4)",
		lightPurple: "#9C97C0",
		skyBlue: "#87CEEB",
		crocodileGrey: "#ECEAE7",
		manatee: "#9A99A3",
		butterFlyBushBlue: "#585397",
		whisperBlue: "#EEEDF4",
	},
	other: {
		primaryBackground: White,
		secondaryBackground: WarmGray[5],
		darkBckground: Gray[100],
		divider: Gray[15],
		stroke: Gray[30],
	},
};

const typography: TypographyOptions = {
	fontFamily: [
        '"AmplitudeWide-Light"',
        '"AmplitudeWide-Regular"',
		"-apple-system",
		"BlinkMacSystemFont",
		'"Segoe UI"',
		'"Helvetica Neue"',
		"Arial",
		"sans-serif",
		'"Apple Color Emoji"',
		'"Segoe UI Emoji"',
		'"Segoe UI Symbol"',
	].join(","),
	fontWeightLight: 300,
	fontWeightRegular: 400,
	fontWeightMedium: 400,
	fontWeightBold: 700,
	h1: {
		fontSize: pxToRem(96),
		lineHeight: 1.167,
		letterSpacing: "-1.5px",
	},
	h2: {
		fontSize: pxToRem(64),
		lineHeight: 1.25,
		letterSpacing: "-0.5px",
	},
	h3: {
		fontSize: pxToRem(48),
		lineHeight: 1.333,
		letterSpacing: "-0.5px",
	},
	h4: {
		fontSize: pxToRem(36),
		lineHeight: 1.333,
		letterSpacing: "-0.5px",
	},
	h5: {
		fontSize: pxToRem(24),
		lineHeight: 1.333,
		letterSpacing: "-0.2px",
		fontFamily: '"AmplitudeWide-Regular"',
	},
	h6: {
		fontSize: pxToRem(20),
		lineHeight: 1.2,
		letterSpacing: "-0.2px",
	},
	subtitle1: {
		fontSize: pxToRem(16),
		lineHeight: 1.5,
        letterSpacing: "-0.2px",
		fontFamily: '"AmplitudeWide-Regular"',
	},
	subtitle2: {
		fontSize: pxToRem(14),
		lineHeight: 1.429,
		letterSpacing: "-0.2px",
		fontFamily: '"AmplitudeWide-Regular"',
	},
	caption: {
		fontSize: pxToRem(10),
		lineHeight: 1.6,
		letterSpacing: "-0.14px",
		fontFamily: "AmplitudeWide-Light",
	},
	overline: {
		fontSize: pxToRem(14),
		lineHeight: 1.143,
		letterSpacing: "1px",
		fontFamily: '"AmplitudeWide-Regular"',
	},
	body1: {
		fontSize: pxToRem(16),
		lineHeight: 1.5,
		letterSpacing: "-0.2px",
	},
	body2: {
		fontSize: pxToRem(14),
		lineHeight: 1.429,
		letterSpacing: "-0.2px",
	}   
};

const overrides: Overrides = {
	MuiTypography: {
		colorSecondary: {
			color: Gray[60]
		},
	},
	MuiButton: {
		root: {
			borderRadius: "28px",
			// textTransform: "capitaliize",
		},
		label: {
			fontFamily: '"AmplitudeWide-Regular"',
			fontSize: pxToRem(14),
		},
		containedSecondary: {
			color: "#35334e",
		},
		textPrimary: {
			"&:focus": {
				backgroundColor: "#e6e5e8",
			},
		},

	},
	MuiCheckbox: {
		colorSecondary: {
			"&$checked": {
				color: palette.icon.interactive,
			},
		},
	},
	MuiFilledInput: {
		root: {
			"&$focused": {
				backgroundColor: "#fff",
				border: "1px solid #35334e",
			},
			borderTopLeftRadius: 0,
			borderTopRightRadius: 0,
		},
	},
	MuiListItem: {
		root: {
			"&$selected": {
				background: "inherit",
			},
			"&$focus": {
				backgroundColor: "rgba(0,0,0,0.14)",
			},
		},
	},
	MuiTableCell: {
		root: {
			fontSize: pxToRem(16),
			padding: "28px 40px 28px 16px",
		},
		head: {
			fontSize: pxToRem(16),
		},
	}
};

const deviceQuery = {
	phone: "@media screen and (min-device-width: 320px)",
	tablet: "@media screen and (min-device-width: 600px)",
	laptop: "@media screen and (min-device-width: 960px)",
	desktop: "@media screen and (min-device-width: 1280px)",
	bigScreen: "@media screen and (min-device-width: 1920px)"
};

export function createTheme(options: any) 
{
	return responsiveFontSizes(
		createMuiTheme({ ...options })
	);
}

const AppTheme = createTheme({ palette, typography, overrides, deviceQuery });

export default AppTheme;