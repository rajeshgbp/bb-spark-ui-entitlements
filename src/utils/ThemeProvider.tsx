import React from 'react';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';
import NoSsr from '@material-ui/core/NoSsr';
import theme from "./theme";

type ThemeProps = {
    children: any;
}

const ThemeProvider: React.FC<ThemeProps> = ({ children }: ThemeProps) =>
{
    return (
        <NoSsr>
            <MuiThemeProvider theme={theme}>
                <StyledThemeProvider theme={theme}> {children} </StyledThemeProvider>
            </MuiThemeProvider>
        </NoSsr>
    );
};

export default ThemeProvider;