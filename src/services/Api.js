import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { css } from 'glamor';

const AXIOS_TIMEOUT_IN_MS = 50000;
var messages = [];

class ApiService 
{
    getErrorMessage(msg)
    {
        for (let index = 0; index < messages.length; index++) 
        {
            if (messages[index].message === msg)
            {
                return messages[index];
            }        
        }

        return null;
    }

    showError(message)
    {
        let now = Date.now();
        let oldMsg = this.getErrorMessage(message);
        
        if (!oldMsg || (now - oldMsg.timeStamp) > 1000 )
        {
            if (oldMsg)
            {
                oldMsg.timeStamp = now;
            }
            else
            {
                messages.push({message: message, timeStamp: now});
            }

            toast.configure({
                autoClose: 4000,
                position: "bottom-center",
                closeButton: false,
                draggable: false,
                hideProgressBar: true,
                closeOnClick: true,
                // toastClassName: css({
                // fontSize: '13px !important',
                // backgroundColor: '#BA482E !important',
                // padding: '10px !important',
                // "white-space": "pre-line"
                // }),
                role: "alert"
            });

            if(message)
            {
                toast.error(message, {
                    className: css({
                        fontSize: '13px', padding: '10px',
                        backgroundColor: '#BA482E',
                    })});
            }    
        }
    };

    showSuccess(message)
    {
        toast.configure({
            autoClose: 4000,
            position: "bottom-center",
            closeButton: false,
            draggable: false,
            hideProgressBar: true,
            closeOnClick: true,
            // toastClassName: css({
            // fontSize: '13px !important',
            // backgroundColor: '#45799F !important',
            // padding: '10px !important',
            // }),
            role: "alert"
        });

        toast.error(message, {
            className: css({
                fontSize: '13px', padding: '10px',
                backgroundColor: '#45799F',
            })
        });
    }

    showBusy()
    {
        var busyBar = document.getElementById("busy_bar");

        if (busyBar)
        {
            busyBar.style.display = "block";
        }
    }

    hideBusy()
    {
        var busyBar = document.getElementById("busy_bar");

        if (busyBar)
        {
            busyBar.style.display = "none";
        }
    }

    // Services
    getService(uri, func) 
    {
        let header = {
            role: 'ADMIN',
            // rptoken: sessionStorage.getItem('rentpayUserAuthToken') ? sessionStorage.getItem('rentpayUserAuthToken') : null
            rptoken: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiMDZhNzE1NTktZmUxZS00ZDhiLTg2ZWItYmRiZmU3MTczYjU2IiwiZmlyc3ROYW1lIjoiUmF2aSIsImxhc3ROYW1lIjoiTWFyd2FoYSIsImVtYWlsIjoiYWRtaW5AYmx1ZXBhbC5jb20iLCJwaG9uZSI6IisxOTk5OTk5OTk5OSIsInN0b3J5VXNlcklkIjoiMTE1OEE1RjhGNzMxNDgwMDkxRDA5RUJENjc5MzUzNTciLCJyb2xlIjoiUkhBLCIsInV1aWQiOiIxMTU4QTVGOEY3MzE0ODAwOTFEMDlFQkQ2NzkzNTM1NyIsImFkZHJlc3MiOm51bGwsImNpdHkiOm51bGwsInN0YXRlQ29kZSI6bnVsbCwiemlwQ29kZSI6bnVsbCwiZ29MaXZlIjowLCJpc1JlZ2lzdGVyZWQiOjEsImVudGl0bGVtZW50IjpudWxsfSwiaWF0IjoxNjQwMDc1MTI3LCJleHAiOjE2NDAxNjE1Mjd9.JNbOardFrSUZuf3Sl0oN3CHNaEO5c5YsyP6FYIbh26w`
            
        }

        let configurations = {
            headers: header,
            timeout: AXIOS_TIMEOUT_IN_MS
        }
        
        this.showBusy();

        axios.get(uri, configurations).then(result => 
        {
            this.hideBusy();

            if (result.headers.newtoken)
            {
                sessionStorage.removeItem('rentpayUserAuthToken');
                sessionStorage.setItem('rentpayUserAuthToken', result.headers.newtoken);
            }

            func(result.data);
        }).catch((error) => 
        {
            this.hideBusy();
            if (error != null && error.message != null && error.message === 'Network Error')
            {
                this.showError("Network error. Please check the connection to service and try again.");
            }
            else
            {
                this.showError(error.message)
            }
        });
    }

    postService(uri, data, func) 
    {
        let header = {
            role: 'ADMIN',
            rptoken: sessionStorage.getItem('rentpayUserAuthToken') ? sessionStorage.getItem('rentpayUserAuthToken') : null
        }

        let configurations = {
            headers: header,
            timeout: AXIOS_TIMEOUT_IN_MS
        }
        
        this.showBusy();

        axios.post(uri, data, configurations).then(result => 
        {
            this.hideBusy();

            if (result.headers.newtoken)
            {
                sessionStorage.removeItem('rentpayUserAuthToken');
                sessionStorage.setItem('rentpayUserAuthToken', result.headers.newtoken);
            }

            func(result.data);

        }).catch((error) => 
        {
            this.hideBusy();

            let errorMessage = '';
            if (error != null && error.message != null && error.message === 'Network Error')
            {
                errorMessage = "Network error. Please check the connection to service and try again."
            }
            else
            {
                errorMessage = error.message;
            }

            this.showError(errorMessage);
        });
    }

    getCall(uri, func) 
    {
        let header = {
            role: 'ADMIN',
            rptoken: sessionStorage.getItem('rentpayUserAuthToken') ? sessionStorage.getItem('rentpayUserAuthToken') : null
        }

        let configurations = {
            headers: header,
            timeout: AXIOS_TIMEOUT_IN_MS
        }
        
        axios.get(uri, configurations).then(result => 
        {
            if (result.headers.newtoken)
            {
                sessionStorage.removeItem('rentpayUserAuthToken');
                sessionStorage.setItem('rentpayUserAuthToken', result.headers.newtoken);
            }

            func(result.data);
        }).catch((error) => 
        {
            if (error != null && error.message != null && error.message === 'Network Error')
            {
                this.showError("Network error. Please check the connection to service and try again.");
            }
            else
            {
                this.showError(error.message)
            }
        });
    }
}

export default new ApiService();