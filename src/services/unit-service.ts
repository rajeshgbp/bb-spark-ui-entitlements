import API from "./Api";
import config from "./config";

const serviceUrl = `${config.service_urls.propertyManagement}`;

class UnitService 
{
    GetUnits(callback: any)
    {
        let apiUrl = `${serviceUrl}/api/admin/units`;

        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });
    }

}

export default new UnitService();