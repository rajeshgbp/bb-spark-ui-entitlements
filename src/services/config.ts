
// const baseUrl = 'http://ec2-3-87-178-2.compute-1.amazonaws.com';
const baseUrl = 'http://ec2-54-164-115-225.compute-1.amazonaws.com';

let service_urls = {
    userManagement: `${baseUrl}:9091`,
    propertyManagement: `${baseUrl}:9092`,
    payment: `${baseUrl}:9093`,
    scheduler: `${baseUrl}:9094`,
    activityLog: `${baseUrl}:9095`,
    note: `${baseUrl}:9096`,
    task: `${baseUrl}:9097`,
    message: `${baseUrl}:9098`,
    document: `${baseUrl}:9099`,   // /api/doc for story 
    storyUserManagement: `${baseUrl}:9091`, // /api/um for story
};

let serverUrl = "";

let apiUrl = serverUrl + "/api/";

export default {
    API_URL: apiUrl,
    FILE_URL: apiUrl + "pictures/",
    INACTIVE_TIME: 24 * 60 * 60 * 1000,

    PAY_COUNTRY: 'US',

    service_urls: service_urls,
    app_version: "6.0.0, 4/27/2021"
};