import API from "./Api";
import config from "./config";
import { roles, accessLevels } from "../MockupJSONs/roles"

const serviceUrl = config.service_urls.userManagement;

class RolesService {

    GetRoles(callback: any) {
        callback(roles);
    }

    GetAccessLevels(callback: any) {
        callback(accessLevels)
    }

    GetLandlords(callback: any) {
        let apiUrl = `${serviceUrl}/api/admin/landlords`;

        API.getService(apiUrl, function (result: object) {
            callback(result);
        });
    }

    UpdatePayServiceFee(id: string, params: any, callback: any) {

        let apiUrl = `${serviceUrl}/api/admin/update/user/${id}/payment/service`;

        API.postService(apiUrl, params, function (result: object) {
            callback(result);
        });
    }

    InviteLandlord(params: any, callback: any) {

        let apiUrl = `${serviceUrl}/api/invite/external/user`;

        API.postService(apiUrl, params, function (result: object) {
            callback(result);
        });
    }

    BlockUser(id: string, callback: any) {

        let apiUrl = `${serviceUrl}/api/admin/update/user/${id}/block`;

        API.postService(apiUrl, {}, function (result: object) {
            callback(result);
        });
    }

    UnBlockUser(id: string, callback: any) {

        let apiUrl = `${serviceUrl}/api/admin/update/user/${id}/unblock`;

        API.postService(apiUrl, {}, function (result: object) {
            callback(result);
        });
    }

    GetTenants(callback: any) {
        let role = 'Tenant';
        let apiUrl = `${serviceUrl}/api/users/${role}`;

        API.getService(apiUrl, function (result: object) {
            callback(result);
        });
    }

    AuthenticateUser(callback: any) {
        let apiUrl = `${serviceUrl}/api/authenticate`;

        API.getCall(apiUrl, function (result: object) {
            callback(result);
        });
    };

}

export default new RolesService();