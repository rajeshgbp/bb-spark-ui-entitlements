import API from "./Api";
import config from "./config";
import {users,status,role,lastSignIn} from "../MockupJSONs/usersList"
import {usersActivityList} from "../MockupJSONs/usersActivityList"
import {rolesData} from '../utils/constants';

const serviceUrl = config.service_urls.userManagement;

interface Result{
    data ?: any
}
class UserService
{
    
    GetUsers(callback: any) 
    {
        let apiUrl = `${serviceUrl}/api/user`;

        API.getService(apiUrl, function (result: Result) 
        {
            const { data } = result;
            
            callback(data.users || []);
            
        });
        // callback(users);
    }

    GetUserActivities(callback:any){
        callback(usersActivityList)
    }
    
    GetFilterList(callback: any) 
    {
        // let apiUrl = `${serviceUrl}/api/admin/users`;

        // API.getService(apiUrl, function (result: object) 
        // {
        //     callback(result);
        // });
        callback({status,role,lastSignIn});
    }

    GetLandlords(callback: any) 
    {
        let apiUrl = `${serviceUrl}/api/admin/landlords`;

        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });
    }

    UpdatePayServiceFee(id: string, params: any, callback: any) 
    {

        let apiUrl = `${serviceUrl}/api/admin/update/user/${id}/payment/service`;

        API.postService(apiUrl, params, function (result: object) 
        {
            callback(result);
        });
    }

    InviteLandlord(params: any, callback: any) 
    {

        let apiUrl = `${serviceUrl}/api/invite/external/user`;

        API.postService(apiUrl, params, function (result: object) 
        {
            callback(result);
        });
    }

    BlockUser(id: string, callback: any) 
    {

        let apiUrl = `${serviceUrl}/api/admin/update/user/${id}/block`;

        API.postService(apiUrl, {}, function (result: object) 
        {
            callback(result);
        });
    }

    UnBlockUser(id: string, callback: any) 
    {

        let apiUrl = `${serviceUrl}/api/admin/update/user/${id}/unblock`;

        API.postService(apiUrl, {}, function (result: object) 
        {
            callback(result);
        });
    }

    GetTenants(callback: any) 
    {
        let role = 'Tenant';
        let apiUrl = `${serviceUrl}/api/users/${role}`;

        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });
    }

    AuthenticateUser(callback: any)
    {
        let apiUrl = `${serviceUrl}/api/authenticate`;

        API.getCall(apiUrl, function (result: object)
        {
            callback(result);
        });
    };

    getTeamMemberById(callback:any, id:(string|number)){
        let apiUrl = `${serviceUrl}/api/user/${id}`;

        API.getService(apiUrl, function (result: Result) 
        {
            const { data } = result;
            const userRolesAndData = JSON.parse(JSON.stringify(rolesData));
                userRolesAndData.userInfo=data.user;
                callback(userRolesAndData);
        });
        // const userRolesAndData = JSON.parse(JSON.stringify(rolesData));
        // const filteredUser = users.filter((user)=>user.id==id);
        // if(filteredUser.length){
        //     const userInfo = Object.assign(userRolesAndData.userInfo,filteredUser[0]);
        //     userRolesAndData.userInfo=userInfo;
        //     callback(userRolesAndData);
        // }
    }

}

export default new UserService();