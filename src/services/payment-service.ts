import API from "./Api";
import config from "./config";

const serviceUrl = config.service_urls.payment;

class PaymentService
{

    SchedulerNotifications(params: any, callback: any) 
    {
        
        let apiUrl = `${serviceUrl}/api/scheduler/poll/notifications`;
        
        API.postService(apiUrl, params, function (result: object) 
        {
            callback(result);
        });
    }

    DownloadWePayAudit(startDate, endDate) 
    {
        window.open(`${serviceUrl}/api/admin/audit/email?startDate=${startDate}&endDate=${endDate}`);        
    };

    getLegalEntityDetails(id: any, callback: any) 
    {
        
        let apiUrl = `${serviceUrl}/api/legalentity/${id}`;
        
        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });
    }
    
}

export default new PaymentService();