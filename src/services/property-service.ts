import API from "./Api";
import config from "./config";

const serviceUrl = config.service_urls.propertyManagement;

class PropertyService
{

    GetProperties(callback: any)
    {
        let apiUrl = `${serviceUrl}/api/admin/properties`;

        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });
    }

    MakeDefault(id: string, callback: any)
    {
        let apiUrl = `${serviceUrl}/api/update/lease/template/${id}/default`;

        API.postService(apiUrl, {}, function (result: object) 
        {
            callback(result);
        });
    }
    GetPropertyList(callback: any)
    {
        let apiUrl = `${serviceUrl}/api/properties`;

        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });
    };
    
    GetTenantsByLandlordId(id: string, callback: any)
    {
        let apiUrl = `${serviceUrl}/api/tenant/landlord/${id}`;

        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });
    }

    GetLandlordProperties(uuid: string, callback: any)
    {

        let apiUrl = `${serviceUrl}/api/admin/property/landlord/${uuid}`;

        API.getService(apiUrl, function (result: object)
        {
            callback(result);
        });
    }

    GetTenantsByUnits(id: string, callback: any)
    {

        let apiUrl = `${serviceUrl}/api/admin/tenant/unit/${id}`;

        API.getService(apiUrl, function (result: object)
        {
            callback(result);
        });
    }
    
     
    GetUnitsByPropertyId(id: string, callback: any)
    {
        console.log("id", id);
        let apiUrl = `${serviceUrl}/api/admin/unit/property/${id}`;

        API.getService(apiUrl, function (result: object)
        {
            callback(result);
        });
    }

    GetConfig(callback: any)
    {
        let apiUrl = `${serviceUrl}/api/config`;

        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });
    };
}

export default new PropertyService();