import API from "./Api";
import config from "./config";

const serviceUrl = `${config.service_urls.propertyManagement}/api`;

class PortfolioService
{
    CreateLeaseTemplate(params: any, callback: any)
    {

        let apiUrl = `${serviceUrl}/av/lease/template`;

        API.postService(apiUrl, params, function (result: object)
        {
            callback(result);
        });
    }

    UpdateLeaseTemplate(id: string, params: any, callback: any)
    {

        let apiUrl = `${serviceUrl}/av/update/lease/template/${id}`;

        API.postService(apiUrl, params, function (result: object)
        {
            callback(result);
        });
    }

    GetLeaseTemplates(callback: any)
    {
        let apiUrl = `${serviceUrl}/user/lease/template`;

        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });

    }

    GetLeaseTemplateDetails(id: string, callback: any)
    {

        let apiUrl = `${serviceUrl}/lease/template/${id}`;

        API.getService(apiUrl, function (result: object)
        {
            callback(result);
        });
    }

    DeleteLeaseTemplate(id: string, callback: any)
    {

        let apiUrl = `${serviceUrl}/delete/lease/template/${id}`;

        API.postService(apiUrl, {}, function (result: object)
        {
            callback(result);
        });
    }
    
    GetAdminTransactions(callback: any)
    {
        let apiUrl = `${serviceUrl}/admin/transactions`;

        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });

    }
    
    GetOtherCharges(callback: any)
    {
        let apiUrl = `${serviceUrl}/charge/type`;

        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });

    }

    AddChargeType(params: any, callback: any)
    {
        let apiUrl = `${serviceUrl}/charge/type`;

        API.postService(apiUrl, params, function (result: object)
        {
            callback(result);
        });
    }

    EditChargeType(id: string, params: any, callback: any)
    {
        let apiUrl = `${serviceUrl}/update/charge/type/${id}`;

        API.postService(apiUrl, params, function (result: object)
        {
            callback(result);
        });
    }

    DeleteChargeType(id: string, callback: any)
    {
        let apiUrl = `${serviceUrl}/delete/charge/type/${id}`;

        API.postService(apiUrl, {}, function (result: object)
        {
            callback(result);
        });
    }

    UploadDocument(params: any, callback: any) 
    {
        let apiUrl = `${serviceUrl}/av/document`;

        API.postService(apiUrl, params, function (result: object) {
            callback(result);
        });
       
    };

    GetDocuments(id, callback: any) 
    {
        let apiUrl = `${serviceUrl}/document/object/${id}`;
        
        API.getService(apiUrl, function (result: object) 
        {
            callback(result);
        });
    };
    
    DeleteDocument(id, callback: any) 
    {
        let apiUrl = `${serviceUrl}/delete/document/${id}`;

        API.postService(apiUrl, {}, function (result: object) 
        {
            callback(result);
        });
        
    };
}

export default new PortfolioService();