import API from "./Api";
import config from "./config";

const serviceUrl = config.service_urls.document;

class DocumentService 
{

    DownloadDocument(docId)
    {
        window.open(`${serviceUrl}/v1/docs/${docId}`);
    };

    GetDocument(docId) 
    {
        return `${serviceUrl}/v1/docs/${docId}?bpType=img`;        
    };
}

export default new DocumentService();