import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { AppBar, Toolbar, IconButton, Typography, Card, ListItem, ListItemIcon } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { ThemeProvider, makeStyles } from '@material-ui/styles';
import { withRouter } from 'react-router-dom';
import API from "../../../../services/Api";
import { layoutStyles } from "layouts/styles";
import BusyBar from 'containers/BusyBar';
import SearchIcon from '@material-ui/icons/Search';
import Badge from '@material-ui/core/Badge';
import Avatar from '@material-ui/core/Avatar';
import NotificationsIcon from '@material-ui/icons/Notifications';
import deepPurple from '@material-ui/core/colors/deepPurple';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Divider from '@material-ui/core/Divider';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import CheckIcon from '@material-ui/icons/Check';
import DeleteIcon from '@material-ui/icons/Delete';
import Grid from '@material-ui/core/Grid';
import Popover from '@material-ui/core/Popover';
// import Button from '@material-ui/core/Button';
// import ExpandLess from '@material-ui/icons/ExpandLess';
// import ExpandMore from '@material-ui/icons/ExpandMore';
// import Collapse from '@material-ui/core/Collapse';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItemText from '@material-ui/core/ListItemText';
// import ListItemAvatar from '@material-ui/core/ListItemAvatar';
// added
// import Drawer from '@material-ui/core/Drawer';
// import HomeIcon from '@material-ui/icons/Home';
import TimelineIcon from '@material-ui/icons/Timeline';
// import GroupIcon from '@material-ui/icons/Group';
// import NavigateNextIcon from '@material-ui/icons/NavigateNext';
// import BuildIcon from '@material-ui/icons/Build';
// import EventIcon from '@material-ui/icons/Event';
// import FindInPageIcon from '@material-ui/icons/FindInPage';
// import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
// import PublicIcon from '@material-ui/icons/Public';
import HouseIcon from '@material-ui/icons/House';
// import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import MessageIcon from '@material-ui/icons/Message';
// import BusinessIcon from '@material-ui/icons/Business';
// import WorkIcon from '@material-ui/icons/Work';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
// import AssessmentOutlinedIcon from '@material-ui/icons/AssessmentOutlined';
// import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';
import WarningIcon from '@material-ui/icons/Warning';

// import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
// import NotesIcon from '@material-ui/icons/Notes';
// import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
// import AttachMoneyOutlinedIcon from '@material-ui/icons/AttachMoneyOutlined';
// import AcUnitOutlinedIcon from '@material-ui/icons/AcUnitOutlined';
// import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
// import PaymentOutlinedIcon from '@material-ui/icons/PaymentOutlined';
// import PeopleIcon from '@material-ui/icons/People';
// import DescriptionIcon from '@material-ui/icons/Description';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
// import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
// import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
// import HistoryIcon from '@material-ui/icons/History';
// import AnnouncementIcon from '@material-ui/icons/Announcement';
import ClearIcon from '@material-ui/icons/Clear';
import FeedbackIcon from '@material-ui/icons/Feedback';
// import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
// import AppsIcon from '@material-ui/icons/Apps';
// import ListAltIcon from '@material-ui/icons/ListAlt';
import PaymentIcon from '@material-ui/icons/Payment';
import HelpIcon from '@material-ui/icons/Help';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SettingsIcon from '@material-ui/icons/Settings';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Link from '@material-ui/core/Link';
import { makeCommonStyles } from 'styles/common';

const drawerWidth = 350;

const useStyles = makeStyles(theme => ({
    active: {
        backgroundColor: 'red'
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        '&::-webkit-scrollbar': {
            display: 'none',
        },
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
        '&::-webkit-scrollbar': {
            display: 'none',
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));

const themeSmall = createMuiTheme({
    typography: {
        fontSize: 11,
    },
    palette: {
        secondary: { main: deepPurple[600] },
    },
});


const theme1 = createMuiTheme({
    typography: {
        fontFamily: [
            "TN web use only",
        ].join(','),
    },
});

const theme = createMuiTheme({
    overrides: {
        MuiDrawer: {
            anchorLeft: {
                marginTop: 94
            }
        }
    }
});





function myFunction() {
    var dots = document.getElementById("dots");
    var moreText = document.getElementById("more");
    var btnText = document.getElementById("myBtn");

    if (dots.style.display === "none") {
        dots.style.display = "inline";
        btnText.innerHTML = "click for more";
        moreText.style.display = "none";
    } else {
        dots.style.display = "none";
        btnText.innerHTML = "click for less";
        moreText.style.display = "inline";
    }

};

const Topbar = props => {
    const classes = useStyles();
    const { className, onSidebarOpen, ...rest } = props;
    const { history } = props;
    const mainClassStyles = layoutStyles();
    const commonStyles = makeCommonStyles();

    const isLg = useMediaQuery(theme => theme.breakpoints.up('lg'));
    const isSm = useMediaQuery(theme => theme.breakpoints.up('sm'));
    const isMd = useMediaQuery(theme => theme.breakpoints.up('md'));

    function setResponseFromApi(result) {
        // sessionStorage.removeItem('rentpayAuthUserAdmin');
        // sessionStorage.removeItem('rentpayUserAuthToken');
        API.showSuccess(result.data.message);
        history.push('/');
    }

    const [account, setAccount] = useState({});
    const [isApiResponse, setIsApiResponse] = React.useState(false);

    // useEffect(() => {
    //     API.showBusy();

    //     if (sessionStorage.getItem('rentpayAuthUserAdminRole') == 'tenant') {
    //         API.showBusy();

    //         API.get('tenant/' + userDetails.id, function (result) {
    //             API.hideBusy();
    //             setAccount(result.data.data.tenant);
    //             setIsApiResponse(true);
    //         });
    //     }
    //     else if (sessionStorage.getItem('rentpayAuthUserAdminRole') == 'admin') {
    //         setIsApiResponse(true);
    //     }
    //     else {
    //         API.showBusy();
    //         API.get('landlord/' + userDetails.id, setResponseFromApi);
    //     }

    // eslint-disable-next-line
    // }, []);

    function setResponseFromApi(result) {
        setAccount(result.data.data.landlord);
        setIsApiResponse(true);
    }

    const userDetails = JSON.parse(sessionStorage.getItem('rentpayAuthUserAdmin'));

    const handleClick = () => {
        history.push('/signin');
    };

    useEffect(() => {
        document.getElementById("busy_bar").style.display = "none";
    }, []);


    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClickover = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const [open2, setOpen] = React.useState(true);

    const handleClickmore = () => {
        setOpen(!open2);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;


    let user = null;
    // if (userDetails != null && userDetails.role === 'admin') {
    //     user = {
    //         name: userDetails.name,
    //         avatar: userDetails.name.charAt(0).toUpperCase(),
    //         email: userDetails.email,
    //         role: userDetails.role
    //     }
    // }
    // else {
    //     user = {
    //         name: account.firstName + ' ' + account.lastName,
    //         avatar: userDetails.firstName.charAt(0).toUpperCase() + userDetails.lastName.charAt(0).toUpperCase(),
    //         email: account.email,
    //     }
    // }
    // added
    const [anchorEl1, setAnchorEl1] = React.useState(null);

    const handleClickover1 = event => {
        setAnchorEl1(event.currentTarget);
    };

    const handleClose1 = () => {
        setAnchorEl1(null);
    };
    const open1 = Boolean(anchorEl1);
    const id1 = open1 ? 'simple-popover' : undefined;


    // morevert
    const [anchorElmorevert, setAnchorElmorevert] = React.useState(null);

    const handleClickovermorevert = event => {
        setAnchorElmorevert(event.currentTarget);
    };

    const handleClosemorevert = () => {
        setAnchorElmorevert(null);
    };
    const openmorevert = Boolean(anchorElmorevert);
    const idmorevert = open1 ? 'simple-popover' : undefined;



    // morevert
    const [anchorElmorevert1, setAnchorElmorevert1] = React.useState(null);

    const handleClickovermorevert1 = event => {
        setAnchorElmorevert1(event.currentTarget);
    };

    const handleClosemorevert1 = () => {
        setAnchorElmorevert1(null);
    };
    const openmorevert1 = Boolean(anchorElmorevert1);
    const idmorevert1 = open1 ? 'simple-popover' : undefined;


    // morevert
    const [anchorElmorevert2, setAnchorElmorevert2] = React.useState(null);

    const handleClickovermorevert2 = event => {
        setAnchorElmorevert2(event.currentTarget);
    };

    const handleClosemorevert2 = () => {
        setAnchorElmorevert2(null);
    };
    const openmorevert2 = Boolean(anchorElmorevert2);
    const idmorevert2 = openmorevert2 ? 'simple-popover' : undefined;


    // morevert
    const [anchorElmorevert3, setAnchorElmorevert3] = React.useState(null);

    const handleClickovermorevert3 = event => {
        setAnchorElmorevert3(event.currentTarget);
    };

    const handleClosemorevert3 = () => {
        setAnchorElmorevert3(null);
    };
    const openmorevert3 = Boolean(anchorElmorevert3);
    const idmorevert3 = openmorevert3 ? 'simple-popover' : undefined;

    // morevert
    const [anchorElmorevert4, setAnchorElmorevert4] = React.useState(null);

    const handleClickovermorevert4 = event => {
        setAnchorElmorevert4(event.currentTarget);
    };

    const handleClosemorevert4 = () => {
        setAnchorElmorevert4(null);
    };
    const openmorevert4 = Boolean(anchorElmorevert4);
    const idmorevert4 = openmorevert4 ? 'simple-popover' : undefined;

    // morevert
    const [anchorElmorevert5, setAnchorElmorevert5] = React.useState(null);

    const handleClickovermorevert5 = event => {
        setAnchorElmorevert5(event.currentTarget);
    };

    const handleClosemorevert5 = () => {
        setAnchorElmorevert5(null);
    };
    const openmorevert5 = Boolean(anchorElmorevert5);
    const idmorevert5 = openmorevert5 ? 'simple-popover' : undefined;




    const [state, setState] = React.useState({
        left: false,
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const [opendrawer, setOpendrawer] = React.useState(false);
    const [openCommunity, setOpenCommunity] = React.useState(false);
    const [openAnalytics, setOpenAnalytics] = React.useState(false);
    const [openTools, setOpenTools] = React.useState(false);
    const [openPropertymanagenent, setOpenPropertymanagenent] = React.useState(false);
    const [openmanageportfolio, setOpenmanageportfolio] = React.useState(false);
    const [openmanagerent, setOpenmanagerent] = React.useState(false);
    const [openmanageexpenses, setOpenmanageexpenses] = React.useState(false);
    const [openmanagerepairs, setOpenmanagerepairs] = React.useState(false);
    const [openTenantfinder, setOpenTenantfinder] = React.useState(false);
    const [openCommuncation, setOpenCommuncation] = React.useState(false);
    const [openTenantportal, setOpenTenantportal] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpendrawer(!opendrawer);
        setOpenCommunity(false);
        setOpenAnalytics(false);
        setOpenPropertymanagenent(false);
        setOpenTenantportal(false);
        setOpenTools(false);

    };

    const OpenCommunity = () => {
        setOpendrawer(true);
        setOpenCommunity(true);
        setOpenAnalytics(false);
        setOpenPropertymanagenent(false);
        setOpenTenantportal(false);
        setOpenTools(false);


    };
    const OpenCommunityOptions = () => {
        setOpendrawer(false);
        setOpenCommunity(false);
        setOpenAnalytics(false);
        setOpenPropertymanagenent(false);
        setOpenTenantportal(false);
        setOpenTools(false);
    };

    const OpenAnalytics = () => {
        setOpendrawer(true);
        setOpenAnalytics(true);
        setOpenCommunity(false);
        setOpenPropertymanagenent(false);
        setOpenTenantportal(false);

    };


    const OpenTools = () => {
        setOpendrawer(true);
        setOpenTools(true);
        setOpenCommunity(false);
        setOpenAnalytics(false);
        setOpenPropertymanagenent(false);
        setOpenTenantportal(false);

    };

    const OpenPropertymanagenent = () => {
        setOpenPropertymanagenent(true);
        setOpendrawer(true);
        setOpenTools(false);
        setOpenCommunity(false);
        setOpenAnalytics(false);
        setOpenmanageportfolio(false);
        setOpenTenantportal(false);
    };


    const Openmanageportfolio = () => {
        setOpenmanageportfolio(true);
        setOpenmanagerent(false);
        setOpenmanageexpenses(false);
        setOpenmanagerepairs(false);
        setOpenTenantfinder(false);
        setOpenCommuncation(false);
    };



    const Openmanagerent = () => {
        setOpenmanageportfolio(false);
        setOpenmanagerent(true);
        setOpenmanageexpenses(false);
        setOpenmanagerepairs(false);
        setOpenTenantfinder(false);
        setOpenCommuncation(false);
    };


    const Openmanageexpenses = () => {
        setOpenmanageportfolio(false);
        setOpenmanagerent(false);
        setOpenmanageexpenses(true);
        setOpenmanagerepairs(false);
        setOpenTenantfinder(false);
        setOpenCommuncation(false);
    };


    const Openmanagerepairs = () => {
        // setOpenmanagerepairs(!openmanagerepairs);
        setOpenmanageportfolio(false);
        setOpenmanagerent(false);
        setOpenmanageexpenses(false);
        setOpenmanagerepairs(true);
        setOpenTenantfinder(false);
        setOpenCommuncation(false);
    };


    const OpenTenantfinder = () => {
        // setOpenTenantfinder(!openTenantfinder);
        setOpenmanageportfolio(false);
        setOpenmanagerent(false);
        setOpenmanageexpenses(false);
        setOpenmanagerepairs(false);
        setOpenTenantfinder(true);
        setOpenCommuncation(false);
    };


    const OpenCommuncation = () => {
        // setOpenCommuncation(!openCommuncation);
        setOpenmanageportfolio(false);
        setOpenmanagerent(false);
        setOpenmanageexpenses(false);
        setOpenmanagerepairs(false);
        setOpenTenantfinder(false);
        setOpenCommuncation(true);
    };



    const OpenTenantportal = () => {
        setOpenTenantportal(true);
        setOpenPropertymanagenent(false);
        setOpendrawer(true);
        setOpenTools(false);
        setOpenCommunity(false);
        setOpenAnalytics(false);
        setOpenmanageportfolio(false);
    };



    const [color, setColor] = useState(false);
    const handleClickColor = () => {
        setColor(!color);

    };


    const [color1, setColor1] = useState(false);
    const handleClickColor1 = () => {
        setColor1(!color1);

    };

    const [color2, setColor2] = useState(false);
    const handleClickColor2 = () => {
        setColor2(!color2);

    };

    const [color3, setColor3] = useState(false);
    const handleClickColor3 = () => {
        setColor3(!color3);

    };

    const [color4, setColor4] = useState(false);
    const handleClickColor4 = () => {
        setColor4(!color4);

    };

    function gotoInvoice(){
    }


    return (
        <div>
            {isLg || isSm || isMd ?
                (
                    <AppBar
                        {...rest}
                        {...rest}
                        position="fixed"
                        className={clsx(mainClassStyles.topbarRoot, className)}
                        style={{backgroundColor:'white'}}



                    >

                        <div style={{ display: "flex" }}>
                            <Toolbar>
                                {/* <Hidden lgUp> */}
                                {/* <IconButton
                                    className={mainClassStyles.themeBlackTextColor}
                                    onClick={onSidebarOpen}> */}
                                <MuiThemeProvider theme={theme}>
                                    <IconButton
                                        aria-label="open drawer"
                                        onClick={handleDrawerOpen}
                                        edge="start"
                                        className={clsx(classes.menuButton, {

                                        })} >{opendrawer ? <MenuIcon /> : <MenuIcon />}</IconButton>
                                </MuiThemeProvider>

                                {/* </IconButton> */}
                                {/* </Hidden> */}
                                <RouterLink to="/rpadmin/landlords">
                                    <img src="/app/assets/rp/images/story-logo-2-dark.svg" style={{ width: '160px', height: '50px' }} />
                                </RouterLink>
                                <div className={mainClassStyles.flexGrow} />
                            </Toolbar>

                            <div style={{ display: "flex" }} className={mainClassStyles.kycIcon}>

                                <IconButton className={mainClassStyles.themeBlackTextColor}>
                                    <SearchIcon /> 
                                </IconButton>

                                <IconButton className={mainClassStyles.themeBlackTextColor} onClick={()=>gotoInvoice()} >
                                    <Badge color="error" overlap="circle" badgeContent=" " variant="dot" >
                                        <MessageIcon  />
                                    </Badge>
                                </IconButton>

                                <IconButton aria-describedby={id} color="primary" onClick={handleClickover}>
                                    <Badge color="error" overlap="circle" badgeContent=" " variant="dot" >
                                        <NotificationsIcon className={mainClassStyles.themeBlackTextColor} />
                                    </Badge>
                                </IconButton>

                                {isApiResponse ? (
                                    userDetails.role === 'admin' ?
                                        (
                                            <ThemeProvider>
                                                <ThemeProvider theme={themeSmall}>
                                                    <Typography color="secondary" display="inline">
                                                        <IconButton className={mainClassStyles.themeBlackTextColor}>
                                                            <Avatar alt="Person" style={{ height: '30px', width: '30px' }} component={RouterLink}  >
                                                                {user.name !== null && user.name !== "" ? (<span>{user.name.charAt(0).toUpperCase()}</span>) : (<span></span>)}
                                                                {/* {account.lastName != null && account.lastName != "" ? (<span>{account.lastName.charAt(0).toUpperCase()}</span>) : (<span></span>)} */}
                                                            </Avatar>
                                                            {/* <Avatar alt="Remy Sharp" src="/app/assets/rp/images/profile.PNG" style={{ height: '30px', width: '30px' }} className={mainClassStyles.marginRight5} /> */}
                                                        </IconButton>
                                                    </Typography>
                                                </ThemeProvider>

                                                <ThemeProvider theme={themeSmall}>
                                                    <Typography className={mainClassStyles.marginTop} color="secondary" display="inline">
                                                        {user.name}
                                                    </Typography>
                                                </ThemeProvider>

                                                {/* <IconButton className={mainClassStyles.themeBlackTextColor}>
                                                    <ArrowDropDownIcon />
                                                </IconButton> */}
                                            </ThemeProvider>
                                        ) :
                                        (
                                            <ThemeProvider>
                                                <ThemeProvider theme={themeSmall}>
                                                    <Typography color="secondary" display="inline">
                                                        <IconButton className={mainClassStyles.themeBlackTextColor}>
                                                            {account.picture === null || account.picture === 'null' || account.picture === '' ?
                                                                (
                                                                    <Avatar alt="Person" style={{ height: '30px', width: '30px' }} component={RouterLink} to="/settings" >
                                                                        {account.firstName !== null && account.firstName !== "" ? (<span>{account.firstName.charAt(0).toUpperCase()}</span>) : (<span></span>)}
                                                                        {account.lastName !== null && account.lastName !== "" ? (<span>{account.lastName.charAt(0).toUpperCase()}</span>) : (<span></span>)}
                                                                    </Avatar>
                                                                ) :
                                                                (
                                                                    <Avatar alt="Person" style={{ height: '30px', width: '30px' }} component={RouterLink} to="/settings"
                                                                        src={''}
                                                                    >
                                                                    </Avatar>
                                                                )}

                                                            {/* <Avatar alt="Remy Sharp" src="/app/assets/rp/images/profile.PNG" style={{ height: '30px', width: '30px' }} className={mainClassStyles.marginRight5} /> */}
                                                        </IconButton>
                                                    </Typography>
                                                </ThemeProvider>

                                                <ThemeProvider theme={themeSmall}>
                                                    <Typography className={mainClassStyles.marginTop} color="secondary" display="inline">
                                                        {account.firstName} {account.lastName}
                                                    </Typography>
                                                </ThemeProvider>


                                            </ThemeProvider>
                                        )
                                ) : (<div></div>)}
                                <IconButton className={mainClassStyles.themeBlackTextColor}
                                    aria-describedby={id1} color="primary" onClick={handleClickover1}>
                                    <ArrowDropDownIcon />
                                </IconButton>
                            </div>

                        </div>
                        <div style={{ position: 'fixed',
                            top: '0',
                            left: '0',
                            opacity: '1',
                            zIndex: 998,
                            height: '100%',
                            width: '100%', }} id="busy_bar"><BusyBar test="true"></BusyBar></div>
                        <Divider style={{ height: '2px' }} />


                    </AppBar>

                )
                :
                (
                    <AppBar
                        {...rest}
                        className={clsx(mainClassStyles.mobileTopbarRoot, className)} style={{backgroundColor:'#35334e'}}>

                        <div className={mainClassStyles.applyTextCenter}>
                            <Toolbar>
                                <Grid container>
                                    <Grid item xs={2} >
                                        <IconButton
                                            className={mainClassStyles.themeWhiteColor}
                                            style={{color: '#FFF'}}
                                            onClick={onSidebarOpen}>
                                            <MenuIcon />

                                        </IconButton>
                                    </Grid>
                                    <Grid item xs={10}>
                                        <RouterLink to="/rpadmin/landlords">
                                            <div className={mainClassStyles.applyTextCenter}>
                                                <img src="/app/assets/rp/images/story-logo-2-light.svg" style={{ width: '160px', height: '50px' }} />
                                            </div>
                                        </RouterLink>
                                    </Grid>
                                </Grid>
                                <div className={mainClassStyles.flexGrow} />
                            </Toolbar>
                        </div>
                        <div style={{ position: 'fixed',
                            top: '0',
                            left: '0',
                            opacity: '1',
                            zIndex: 998,
                            height: '100%',
                            width: '100%', }} id="busy_bar"><BusyBar test="true"></BusyBar></div>
                        <Divider style={{ height: '2px' }} />
                    </AppBar>
                )
            }

            <Popover
                id={id}

                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            // className={mainClassStyles.marginTopLeftRightBottom2}

            >
                <Card square style={{ width: "420px", }}  >
                    <ThemeProvider >
                        <div className={mainClassStyles.marginTopLeftRightBottom2}>
                            <Typography align="center" ><small><b>NOTIFICATIONS (2)</b></small></Typography>
                        </div>

                        <div
                            style={color1 ? { backgroundColor: "white" } : { backgroundColor: "#e7e6f0" }}>

                            {
                                color1 ? (<div>

                                </div>) : (<div> <Card square style={{ width: 4, height: "65px", position: "absolute", backgroundColor: "#6a63a1" }}>
                                </Card></div>)
                            }
                            <Divider />
                            {/* </Card> */}
                            <div style={{ marginTop: "10px" }}>
                                <Grid container style={{ marginLeft: "9px", }} spacing={3}>
                                    <Grid item>
                                        <Typography >
                                            < PaymentIcon fontSize="large" />
                                        </Typography>
                                    </Grid>
                                    <Grid item style={{ position: "absolute" }}>
                                        <WarningIcon style={{ color: "#ba482e", marginTop: 18, marginLeft: "23px", }} />
                                    </Grid>
                                    <Grid item lg={8} sm={8} md={8} xs={8} >
                                        {!color1 ? (<div>
                                            <Typography style={{ fontSize: "15px" }}>
                                                <b> Doug Petno's lease has expired.</b>
                                            </Typography>
                                        </div>) : (<div>
                                            <Typography style={{ fontSize: "15px" }}>
                                                Doug Petno's lease has expired.
                                    </Typography> </div>)}

                                        <Grid container  >
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ fontSize: '14px' }}>RentPay &nbsp;</Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ marginTop: -3 }}>
                                                    <FiberManualRecordIcon style={{ fontSize: "7px", }} />
                                                </Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ fontSize: '14px' }}> &nbsp;3 hours ago</Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>

                                    <Grid item >
                                        <Typography color="textSecondary" className={commonStyles.pointer}>
                                            <MoreVertIcon className={mainClassStyles.marginTop1} onClick={handleClickovermorevert1} />
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </div>
                            <div style={{ marginTop: "10px" }}>
                            </div>
                        </div>

                        <div

                            style={color ? { backgroundColor: "white" } : { backgroundColor: "#e7e6f0" }}>

                            {
                                color ? (<div>

                                </div>) : (<div> <Card square style={{ width: 4, height: "108px", position: "absolute", backgroundColor: "#6a63a1" }}>
                                </Card></div>)
                            }
                            <Divider />

                            <div style={{ marginTop: "10px", }}>
                                <Grid container style={{ marginLeft: "9px" }} spacing={3}>

                                    <Grid item>
                                        <HouseIcon fontSize="large" className={mainClassStyles.marginTop2} />
                                    </Grid>
                                    <Grid item lg={8} sm={8} md={8} xs={8} >
                                        {!color ? (<div>
                                            <Typography style={{ fontSize: "15px" }}>
                                                <b>Congratulations, Jill! Your loan has been approved and processed for Birchwood Apartments.</b>
                                            </Typography> </div>) : (<div>
                                                <Typography style={{ fontSize: "15px" }}>
                                                    Congratulations, Jill! Your loan has been approved and processed for Birchwood Apartments.
                                    </Typography>
                                            </div>)}

                                        <Grid container  >
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ fontSize: '14px' }}>Loans &nbsp;</Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ marginTop: -3 }}>
                                                    <FiberManualRecordIcon style={{ fontSize: "7px", }} />
                                                </Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ fontSize: '14px' }}> &nbsp;2 hours ago</Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item className={mainClassStyles.marginTop3}>
                                        <Typography color="textSecondary" onClick={handleClickovermorevert} className={commonStyles.pointer}>
                                            <MoreVertIcon />
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </div>
                            <div style={{ marginTop: "10px" }}>
                            </div>
                        </div>
                        {/* <Divider /> */}
                        <div
                            style={color2 ? { backgroundColor: "white" } : { backgroundColor: "#e7e6f0" }}>
                            {
                                color2 ? (<div>

                                </div>) : (<div> <Card square style={{ width: 4, height: "86px", position: "absolute", backgroundColor: "#6a63a1" }}>
                                </Card></div>)
                            }
                            <Divider />
                            <div style={{ marginTop: "10px", }}>
                                <Grid container style={{ marginLeft: "9px" }} spacing={3}>
                                    <Grid item>
                                        <SupervisorAccountIcon fontSize="large" className={mainClassStyles.marginTop2} />
                                    </Grid>
                                    <Grid item lg={8} sm={8} md={8} xs={8} >
                                        {!color2 ? (<div>
                                            <Typography style={{ fontSize: "15px" }}>

                                                <b>Community Discussions: Saving money on regular maintenance and repairs.</b>
                                            </Typography> </div>) : (<div>
                                                <Typography style={{ fontSize: "15px" }}>

                                                    Community Discussions: Saving money on regular maintenance and repairs.
                                         </Typography> </div>)}

                                        <Grid container  >
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ fontSize: '14px' }}>Community &nbsp;</Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ marginTop: -3 }}>
                                                    <FiberManualRecordIcon style={{ fontSize: "7px", }} />
                                                </Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ fontSize: '14px' }}> &nbsp;3 days ago</Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item >
                                        <Typography color="textSecondary" className={mainClassStyles.marginTop3} onClick={handleClickovermorevert2}>
                                            <MoreVertIcon className={commonStyles.pointer} />
                                        </Typography>
                                    </Grid>

                                </Grid>
                            </div>
                            <div style={{ marginTop: "10px" }}>

                            </div>
                        </div>
                        {/* <Divider style={{ backgroundColor: "#f1f1f1" }} /> */}
                        <div

                            style={color3 ? { backgroundColor: "white" } : { backgroundColor: "#e7e6f0" }}>

                            {
                                color3 ? (<div>

                                </div>) : (<div> <Card square style={{ width: 4, height: "108px", position: "absolute", backgroundColor: "#6a63a1" }}>
                                </Card></div>)
                            }
                            <Divider />
                            <Grid container style={{ padding: 10 }} spacing={3}>
                                <Grid item>
                                    <img src="/app/assets/rp/images/story-logo-2-dark.svg" style={{ width: '60px', height: 60, position: "absolute", marginTop: 12 }} />
                                </Grid>
                                <Grid item lg={9} sm={9} md={9} xs={9} >
                                    {color3 ? (<div>
                                        <Typography style={{ fontSize: "15px", marginLeft: 45 }}>
                                            The system will go through weekly maintenance on Monday Jun. 1. 2020 from 12:00 AM - 4:00 AM PDT.
                                </Typography>
                                    </div>) : (<div>
                                        <Typography style={{ fontSize: "15px", marginLeft: 45 }}>
                                            <b> The system will go through weekly maintenance on Monday Jun. 1. 2020 from 12:00 AM - 4:00 AM PDT.</b>
                                        </Typography>
                                    </div>)}

                                    <Grid container style={{ marginLeft: 45 }} >
                                        <Grid item>
                                            <Typography color="textSecondary" style={{ fontSize: '14px' }}>Story announcements &nbsp;</Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography color="textSecondary" style={{ marginTop: -3 }}>
                                                <FiberManualRecordIcon style={{ fontSize: "7px", }} />
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography color="textSecondary" style={{ fontSize: '14px' }}> &nbsp;4 hours ago</Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item className={mainClassStyles.marginTop3 + " " + mainClassStyles.marginLeft3} >
                                    <Typography color="textSecondary" onClick={handleClickovermorevert3}>
                                        <MoreVertIcon className={commonStyles.pointer} />
                                    </Typography>
                                </Grid>
                            </Grid>

                        </div>
                        {/* <Divider /> */}
                        <div
                            style={color4 ? { backgroundColor: "white" } : { backgroundColor: "#e7e6f0" }}>
                            {
                                color4 ? (<div>

                                </div>) : (<div> <Card square style={{ width: 4, height: "110px", position: "absolute", backgroundColor: "#6a63a1" }}>
                                </Card></div>)
                            }

                            <Divider />
                            <div style={{ marginTop: "10px" }}>

                                <Grid container style={{ marginLeft: "10px", marginBottom: "1px" }} spacing={3}>
                                    <Grid item>
                                        <TimelineIcon fontSize="large" className={mainClassStyles.marginTop3} />
                                    </Grid>
                                    <Grid item lg={8} sm={8} md={8} xs={8} >
                                        {!color4 ? (<div>
                                            <Typography style={{ fontSize: "15px" }}>
                                                <b>  Market Metrics is now up and running again. Our apologies for the unforeseen system error.   </b>
                                            </Typography> </div>) : (<div>
                                                <Typography style={{ fontSize: "15px" }}>
                                                    Market Metrics is now up and running again. Our apologies for the unforeseen system error.
                                    </Typography> </div>)}

                                        <Grid container  >
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ fontSize: '14px' }}>Market Metrics&nbsp;</Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ marginTop: -3 }}>
                                                    <FiberManualRecordIcon style={{ fontSize: "7px", }} />
                                                </Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography color="textSecondary" style={{ fontSize: '14px' }}> &nbsp; 6 hours ago</Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item className={mainClassStyles.marginTop3}>
                                        <Typography color="textSecondary" onClick={handleClickovermorevert4}>
                                            <MoreVertIcon className={commonStyles.pointer} />
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </div>
                        </div>
                    </ThemeProvider>
                </Card>
                <Card square>
                    <div className={mainClassStyles.marginTopLeftRightBottom1}>
                        <Grid container justify={"flex-end"} spacing={1}>
                            <Grid item>
                                <RouterLink>
                                    <Typography style={{ marginTop: "2px" }} onClick={handleClose}>
                                        <small>  <Link>VIEW ALL NOTIFICATIONS </Link></small>
                                    </Typography>
                                </RouterLink>
                            </Grid >
                            <Grid item>
                                <Typography className={commonStyles.marginTopsmall}>
                                    <Link>   <ChevronRightIcon /> </Link>
                                </Typography>
                            </Grid >
                        </Grid>
                    </div>
                </Card>
            </Popover>

            {/* morevert */}
            <Popover
                id={idmorevert}
                open={openmorevert}
                anchorEl={anchorElmorevert}
                onClose={handleClosemorevert}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
            >

                <Grid container className={mainClassStyles.marginTopLeftRightBottom2} onClick={handleClickColor} >
                    <Grid item>
                        {color ? (<div>
                            <Typography color="textSecondary">
                                <ClearIcon />
                            </Typography>
                        </div>) : (<div>
                            <Typography color="textSecondary">
                                <CheckIcon />
                            </Typography>
                        </div>)}
                    </Grid>
                    <Grid item className={mainClassStyles.marginLeft3}>
                        {color ? (<div>
                            <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert}>
                                Mark as unread
                      </Typography>
                        </div>) : (<div>
                            <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert}>
                                Mark as read
                      </Typography>
                        </div>)}
                    </Grid>
                </Grid>
                <Grid container className={mainClassStyles.marginTopLeftRightBottom2} >
                    <Grid item>
                        <Typography color="textSecondary">
                            <DeleteIcon />
                        </Typography>
                    </Grid>
                    <Grid item className={mainClassStyles.marginLeft3}>
                        <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert}>
                            Delete notification
                   </Typography>
                    </Grid>
                </Grid>
            </Popover >
            {/* morevert */}
            <Popover
                id={idmorevert1}
                open={openmorevert1}
                anchorEl={anchorElmorevert1}
                onClose={handleClosemorevert1}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
            >

                <Grid container className={mainClassStyles.marginTopLeftRightBottom2} onClick={handleClickColor1} >
                    <Grid item>
                        {color1 ? (<div>
                            <Typography color="textSecondary">
                                <ClearIcon />
                            </Typography>
                        </div>) : (<div>
                            <Typography color="textSecondary">
                                <CheckIcon />
                            </Typography>
                        </div>)}
                    </Grid>
                    <Grid item className={mainClassStyles.marginLeft3}>
                        {color1 ? (<div>
                            <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert1}>
                                Mark as unread
                      </Typography>
                        </div>) : (<div>
                            <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert1}>
                                Mark as read
                      </Typography>
                        </div>)}
                    </Grid>
                </Grid>
                <Grid container className={mainClassStyles.marginTopLeftRightBottom2} >
                    <Grid item>
                        <Typography color="textSecondary">
                            <DeleteIcon />
                        </Typography>
                    </Grid>
                    <Grid item className={mainClassStyles.marginLeft3} >
                        <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert1}>
                            Delete notification
                   </Typography>
                    </Grid>
                </Grid>
            </Popover >



            {/* morevert */}
            <Popover
                id={idmorevert2}
                open={openmorevert2}
                anchorEl={anchorElmorevert2}
                onClose={handleClosemorevert2}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
            >

                <Grid container className={mainClassStyles.marginTopLeftRightBottom2} onClick={handleClickColor2} >
                    <Grid item>
                        {color2 ? (<div>
                            <Typography color="textSecondary">
                                <ClearIcon />
                            </Typography>
                        </div>) : (<div>
                            <Typography color="textSecondary">
                                <CheckIcon />
                            </Typography>
                        </div>)}
                    </Grid>
                    <Grid item className={mainClassStyles.marginLeft3}>
                        {color2 ? (<div>
                            <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert2}>
                                Mark as unread
                      </Typography>
                        </div>) : (<div>
                            <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert2}>
                                Mark as read
                      </Typography>
                        </div>)}
                    </Grid>
                </Grid>
                <Grid container className={mainClassStyles.marginTopLeftRightBottom2} >
                    <Grid item>
                        <Typography color="textSecondary">
                            <DeleteIcon />
                        </Typography>
                    </Grid>
                    <Grid item className={mainClassStyles.marginLeft3}  >
                        <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert2}>
                            Delete notification
                   </Typography>
                    </Grid>
                </Grid>
            </Popover >


            {/* morevert */}
            <Popover
                id={idmorevert3}
                open={openmorevert3}
                anchorEl={anchorElmorevert3}
                onClose={handleClosemorevert3}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
            >

                <Grid container className={mainClassStyles.marginTopLeftRightBottom2} onClick={handleClickColor3} >
                    <Grid item>
                        {color3 ? (<div>
                            <Typography color="textSecondary">
                                <ClearIcon />
                            </Typography>
                        </div>) : (<div>
                            <Typography color="textSecondary">
                                <CheckIcon />
                            </Typography>
                        </div>)}
                    </Grid>
                    <Grid item className={mainClassStyles.marginLeft3}>
                        {color3 ? (<div>
                            <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer}  onClick={handleClosemorevert3}>
                                Mark as unread
                      </Typography>
                        </div>) : (<div>
                            <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert3}>
                                Mark as read
                      </Typography>
                        </div>)}
                    </Grid>
                </Grid>
                <Grid container className={mainClassStyles.marginTopLeftRightBottom2} >
                    <Grid item>
                        <Typography color="textSecondary">
                            <DeleteIcon />
                        </Typography>
                    </Grid>
                    <Grid item className={mainClassStyles.marginLeft3}>
                        <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert3}>
                            Delete notification
                   </Typography>
                    </Grid>
                </Grid>
            </Popover >


            {/* morevert */}
            <Popover
                id={idmorevert4}
                open={openmorevert4}
                anchorEl={anchorElmorevert4}
                onClose={handleClosemorevert4}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
            >

                <Grid container className={mainClassStyles.marginTopLeftRightBottom2} onClick={handleClickColor4} >
                    <Grid item>
                        {color4 ? (<div>
                            <Typography color="textSecondary">
                                <ClearIcon />
                            </Typography>
                        </div>) : (<div>
                            <Typography color="textSecondary">
                                <CheckIcon />
                            </Typography>
                        </div>)}
                    </Grid>
                    <Grid item className={mainClassStyles.marginLeft3}>
                        {color4 ? (<div>
                            <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert4}>
                                Mark as unread
                      </Typography>
                        </div>) : (<div>
                            <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert4}>
                                Mark as read
                      </Typography>
                        </div>)}
                    </Grid>
                </Grid>
                <Grid container className={mainClassStyles.marginTopLeftRightBottom2} >
                    <Grid item>
                        <Typography color="textSecondary">
                            <DeleteIcon />
                        </Typography>
                    </Grid>
                    <Grid item className={mainClassStyles.marginLeft3}>
                        <Typography style={{ fontSize: "14px" }} className={commonStyles.pointer} onClick={handleClosemorevert4}>
                            Delete notification
                   </Typography>
                    </Grid>
                </Grid>
            </Popover >









            <Popover
                id={id1}
                open={open1}
                anchorEl={anchorEl1}
                onClose={handleClose1}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            // className={mainClassStyles.marginTopLeftRightBottom2}

            >
                <Card style={{ width: '300px', height: '270px', overflow: 'auto' }} className={classes.root}  >
                    <ThemeProvider>
                        <ListItem button>
                            <ListItemIcon>
                                <AccountCircleIcon color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Profile & Settings" color='primary' />
                        </ListItem>

                        <Divider />
                        <ListItem button>
                            <ListItemIcon>
                                <NotificationsIcon color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Notifications" color='primary' />
                        </ListItem>
                        <Divider />
                        <ListItem button>
                            <ListItemIcon>
                                <SettingsIcon color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Settings" color='primary' />
                        </ListItem>
                        <Divider />
                        <ListItem button>
                            <ListItemIcon>
                                <HelpIcon color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Help" color='primary' />
                        </ListItem>
                        <Divider />
                        <ListItem button>
                            <ListItemIcon>
                                <FeedbackIcon color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Add Feedback" color='primary' />
                        </ListItem>
                        <Divider />
                        <ListItem button>
                            <ListItemIcon>
                                <ExitToAppIcon color='primary' />
                            </ListItemIcon>
                            <ListItemText primary="Logout" color='primary' />
                        </ListItem>
                    </ThemeProvider>
                </Card>

            </Popover>

        </div >
    );
};

Topbar.propTypes = {
    className: PropTypes.string,
    onSidebarOpen: PropTypes.func
};


export default withRouter(Topbar);