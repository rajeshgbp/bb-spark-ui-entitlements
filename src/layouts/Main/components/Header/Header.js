import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { layoutStyles } from "layouts/styles";
import Grid from '@material-ui/core/Grid';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';


const themeMedium = createMuiTheme({
    typography: {
        fontSize: 18,
    },
    palette:
    {
        secondary: { main: '#eeeeee' },
        primary: { main: '#9e9e9e' },
    },
});

const themeSmall = createMuiTheme({
    typography: {
        fontSize: 12,
        fontWeight: 300,
    },
    palette:
    {
        secondary: { main: '#eeeeee' },
    },
});


const Header = props => 
{
    const { className, ...rest } = props;
    const headerData = props.headerData;
    const headertitle = props.headertitle;

    const mainClassStyles = layoutStyles();
    const isLg = useMediaQuery(theme => theme.breakpoints.up('lg'));
    const isSm = useMediaQuery(theme => theme.breakpoints.up('sm'));
    const isMd = useMediaQuery(theme => theme.breakpoints.up('md'));

    return (
        <div {...rest} >
            {isLg || isMd || isSm ? 
            (
                <div className={mainClassStyles.headerColor}>
                    <Grid className={[mainClassStyles.marginLeft15, mainClassStyles.marginTopBottom2]}>
                        <ThemeProvider theme={themeMedium}>
                            <Typography>
                                {headertitle}
                            </Typography>
                        </ThemeProvider>
                        <ThemeProvider theme={themeSmall}>
                            <Typography color="secondary" className={mainClassStyles.marginTop1}>{headerData} </Typography>
                        </ThemeProvider>
                    </Grid>
                </div>
            ):
            (
                <div></div>
            )
            }
            
        </div>
    );
};

Header.propTypes = {
    className: PropTypes.string,
    headerData: PropTypes.string
};

export default Header;