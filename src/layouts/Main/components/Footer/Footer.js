import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { createMuiTheme } from '@material-ui/core/styles';
import { Typography, Link } from '@material-ui/core';
import { layoutStyles } from "layouts/styles";
import Grid from '@material-ui/core/Grid'
import { ThemeProvider } from '@material-ui/styles';

const Footer = props => {
    const { className, ...rest } = props;

    const mainClassStyles = layoutStyles();
    const emptyTheme = createMuiTheme({
        typography:
        {
            fontSize: 11,
        },
        palette:
        {
            secondary: { main: '#e0e0e0' },
            primary: { main: '#9e9e9e' }
        },
    });
    const mediumtheme = createMuiTheme({
        typography:
        {
            fontSize: 12,
        },
        palette:
        {
            primary: { main: '#e0e0e0' }
        }

    });


    return (
        <div {...rest} className={clsx(mainClassStyles.Footercolor, className)} >
            <div className={mainClassStyles.gridContent}>
                <div className={mainClassStyles.marginTopLeftRightBottom2}>

                    <Grid container spacing={2}   >
                        <Grid item xs={12} sm={6} md={6} lg={5} >
                            <div className={(mainClassStyles.gridPadding)}>
                                <div>
                                    <img src="/app/assets/rp/images/story-logo-2-light.svg" style={{ width: '200px', height: '65px' }} />
                                </div>
                                <div className={mainClassStyles.marginTop4}>
                                    <div>
                                        <ThemeProvider theme={emptyTheme}>
                                            <Typography variant="caption" color="primary" >
                                                © 2019 JPMorgan Chase & co.All rights reserved.
                                            </Typography>
                                        </ThemeProvider>
                                    </div>
                                    <div className={mainClassStyles.marginTop2}>
                                        <ThemeProvider theme={emptyTheme}>

                                            <Typography variant="caption" color="primary">
                                                "JPMorgan," "JPMorgan Chase," the JPMorgan Chase logo,"Story," and "Story by J.P.Morgan" are trademarks of JPMorgan Chase Bank,N.A. JPMorgan Chase Bank,N.A. is a Wholly-owned subsidiary of JP Morgan Chase & Co.
                                                "
                                            </Typography>
                                        </ThemeProvider>
                                    </div>
                                </div>
                            </div>
                        </Grid>
                        <Grid item xs={4} sm={6} md={6} lg={2}>
                            <div>
                                <ThemeProvider theme={mediumtheme}>
                                    <Typography variant="h6" color="primary">
                                        HELP
                                    </Typography>
                                    <Typography variant="body1" color="primary" className={mainClassStyles.marginTop2}>
                                        Send FeedBack
                                    </Typography>
                                </ThemeProvider>
                            </div>
                        </Grid>
                        <Grid item xs={4} sm={6} md={6} lg={2}>
                            <div>
                                <ThemeProvider theme={mediumtheme}>
                                    <Typography variant="h6" color="primary">
                                        J.P. MORGAN SITES
                                    </Typography>
                                    <Typography variant="body1" color="primary" className={mainClassStyles.marginTop2}>
                                        JPMorgan Chase & Co.
                                    </Typography>
                                    <Typography variant="body1" color="primary" className={mainClassStyles.marginTop1}>
                                        J.P.Morgan
                                    </Typography>
                                    <Typography variant="body1" color="primary" className={mainClassStyles.marginTop1}>
                                        Chase
                                    </Typography> 
                                    <Typography variant="body1" color="primary" className={mainClassStyles.marginTop1}>
                                        J.P. Morgan CRE
                                    </Typography>
                                </ThemeProvider>

                            </div>
                        </Grid>
                        <Grid item xs={4} sm={6} md={6} lg={3}>
                            <ThemeProvider theme={mediumtheme}>
                                <Typography variant="h6" color="primary">
                                    LEGAL
                                </Typography>
                                <Typography variant="body1" color="primary" className={mainClassStyles.marginTop2}>
                                    Privacy & Security
                                </Typography>
                                <Typography variant="body1" color="primary" className={mainClassStyles.marginTop1}>
                                    Terms of Use
                                </Typography>
                                <Typography variant="body1" color="primary" className={mainClassStyles.marginTop1}>
                                    Regulatory Disclosures
                                </Typography> 
                                <Typography variant="body1" color="primary" className={mainClassStyles.marginTop1}>
                                    Cookie Policy
                                </Typography>
                                <Typography variant="body1" color="primary" className={mainClassStyles.marginTop1}>
                                    Data Privacy Protocol
                                </Typography>
                                <Typography variant="body1" color="primary" className={mainClassStyles.marginTop1}>
                                    Accessibility
                                </Typography>
                            </ThemeProvider>
                        </Grid>
                    </Grid>

                </div>
            </div>

        </div>
    );
};

Footer.propTypes = {
    className: PropTypes.string
};

export default Footer;
