import React, { forwardRef, useEffect, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import Drawer from '@material-ui/core/Drawer';
import { NavLink as RouterLink } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import PeopleIcon from '@material-ui/icons/People';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import ReceiptIcon from '@material-ui/icons/Receipt';
import { Grid, Button, List } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import { layoutStyles } from "layouts/styles";

import Scrollable from 'hide-scrollbar-react';

import HomeWorkIcon from '@material-ui/icons/HomeWork';
import DescriptionIcon from '@material-ui/icons/Description';
import DomainIcon from '@material-ui/icons/Domain';
import ApartmentIcon from '@material-ui/icons/Apartment';

const useStyles = makeStyles(theme => ({
    active: {
        '& $icon': {
            color: '#50cbc4'
        },
        '& $text':
        {
            color: '#50cbc4'
        }
    },
    text:
    {

    },
}));

const CustomRouterLink = forwardRef((props, ref) => (
    <div
        // ref={ref}
        style={{ flexGrow: 1 }}
    >
        <RouterLink {...props} />
    </div>
));


const Sidebar = props =>
{
    const { open, variant, onClose, className, ...rest } = props;
    const mainClassStyles = layoutStyles();
    const { history } = props;
    const classes = useStyles();


    const pages = [
        {
            title: 'Users',
            href: '/entitlements',
            icon: <PeopleIcon />
        },
        {
            title: 'Roles',
            href: '/entitlements/roles',
            icon: <ApartmentIcon />
        }
    ];

    const [menuItems, setMenuItems] = useState([]);

    useEffect(() =>
    {
        setMenuItems(pages)
        //     // eslint-disable-next-line
    }, []);

    function handleClick(e, operation)
    {
        e.preventDefault();
        if (props.location.pathname === operation)
        {
            history.go(0)
        }
        else
        {
            history.push(operation)
        }
    };


    return (
        <div id="app-sidebar">

            <Drawer
                anchor="left"
                classes={{ paper: mainClassStyles.drawer }}
                onClose={onClose}
                open={open}
                onClick={onClose}
                variant={variant}>

                <div {...rest} className={clsx(mainClassStyles.sidebarRoot, className)}>
                    <Scrollable>
                        <List>

                            {menuItems.map(menu => (
                                <Grid container key={menu.title}>
                                    <Button
                                        activeClassName={classes.active}
                                        className={mainClassStyles.sideNavBarbutton}
                                        component={CustomRouterLink}
                                        to={menu.href}
                                        onClick={(e) =>
                                        {
                                            handleClick(e, menu.href)
                                        }}>

                                        <Tooltip title={menu.title} placement="right" arrow>
                                            <div className={mainClassStyles.sideNavBarIcon}>{menu.icon}</div>
                                        </Tooltip>
                                    </Button>
                                </Grid>
                            ))}

                        </List>
                    </Scrollable>
                </div>

            </Drawer>


        </div>
    );
};

Sidebar.propTypes = {
    className: PropTypes.string,
    onClose: PropTypes.func,
    open: PropTypes.bool.isRequired,
    variant: PropTypes.string.isRequired
};

export default withRouter(Sidebar);