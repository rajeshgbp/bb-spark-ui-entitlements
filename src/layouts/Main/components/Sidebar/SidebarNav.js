import React, { forwardRef } from 'react';
import { NavLink as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { List, ListItem, Button, colors, Typography } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import { mainStyles } from "../../../../common/styles";



const CustomRouterLink = forwardRef((props, ref) => (
    <div
        ref={ref}
        style={{ flexGrow: 1 }}
    >
        <RouterLink {...props} />
    </div>
));

const SidebarNav = props => 
{
    const { pages, className, ...rest } = props;

    const mainClassStyles = mainStyles();

    return (
        <List
            {...rest}
            className={clsx(mainClassStyles.sideNavBarRoot, className)}
        >
            {pages.map(page => (
                <ListItem
                    className={mainClassStyles.item}
                    disableGutters
                    key={page.title}>
                    <Button
                        activeClassName={mainClassStyles.active}
                        className={mainClassStyles.sideNavBarbutton}
                        component={CustomRouterLink}
                        to={page.href}>
                        <div className={mainClassStyles.sideNavBarIcon}>{page.icon}</div>
                        <ThemeProvider><Typography >{page.title}</Typography></ThemeProvider>
                    </Button>
                </ListItem>
            ))}
        </List>
    );
};

SidebarNav.propTypes = {
    className: PropTypes.string,
    pages: PropTypes.array.isRequired
};

export default SidebarNav;