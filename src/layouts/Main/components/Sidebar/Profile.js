import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Avatar, Typography } from '@material-ui/core';

import config from "../../../../services/config";
import API from "../../../../services/Api";
import { mainStyles } from "../../../../common/styles";

import { UserService } from 'services';

const Profile = props => 
{
    const { className, ...rest } = props;

    const { history } = props;

    const mainClassStyles = mainStyles();

    const userDetails = JSON.parse(sessionStorage.getItem('rentpayAuthUserAdmin'));
    const rentpayUserAuthToken = sessionStorage.getItem('rentpayUserAuthToken');

    if(!rentpayUserAuthToken)
    {
        API.showError('Error. Please sign-in to continue');
    }

    const [account, setAccount] = useState({});

    function setResponseFromApi(result)
    {
        let user = {
            id: result.data.landlord.id,
            id: result.data.landlord.id,
            address: result.data.landlord.address,
            email: result.data.landlord.email,
            firstName: result.data.landlord.firstName,
            lastName: result.data.landlord.lastName,
            phone: result.data.landlord.phone,
            notifications: result.data.landlord.settings.notifications,
            wepay_created: result.data.landlord.wepay_created == '' ? false : result.data.landlord.wepay_created,
            kyc_completed: result.data.landlord.kyc_completed == '' ? false : result.data.landlord.kyc_completed,
            payment_revoked: !result.data.landlord.payment_revoked  ? false : result.data.landlord.payment_revoked,
            payment_created: !result.data.landlord.payment_created ? '' : result.data.landlord.payment_revoked,
        };

        sessionStorage.setItem('rentpayAuthUserAdmin', JSON.stringify(user));

        setAccount(result.data.landlord);
    }

    useEffect(() => 
    {
        API.showBusy();
    }, []);

    const user = {
        name: account.firstName+ ' ' + account.lastName,
        avatar: userDetails.firstName.charAt(0).toUpperCase() +  userDetails.lastName.charAt(0).toUpperCase(),
        email: account.email
    };

    return (
        <div {...rest} className={clsx(mainClassStyles.profileRoot, className)} >
            <Avatar alt="Person" className={mainClassStyles.profileAvatar} component={RouterLink} to="/settings" > {user.avatar}</Avatar>
            {/* <Avatar alt="Person" className={mainClassStyles.avatar} to="/settings" 
            component={RouterLink}>{userDetails.firstName.charAt(0).toUpperCase() +  userDetails.lastName.charAt(0).toUpperCase()}
            </Avatar> */}

            <Typography className={mainClassStyles.marginTop1} variant="h4" > {user.name} </Typography>
            <Typography variant="body2">{user.email}</Typography>
        </div>
    );
};

Profile.propTypes = {
    className: PropTypes.string
};

export default Profile;