import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { useTheme } from '@material-ui/styles';
import { useMediaQuery } from '@material-ui/core';

import { Sidebar, Topbar, Footer } from './components';
import { layoutStyles } from "layouts/styles";

const Main = props => 
{
	const { children } = props;

	const mainClassStyles = layoutStyles();

	const theme = useTheme();
	const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
		defaultMatches: true
	});

	const [openSidebar, setOpenSidebar] = useState(false);

	const handleSidebarOpen = () => 
	{
		setOpenSidebar(true);
	};

	const handleSidebarClose = () => 
	{
		setOpenSidebar(false);
	};

	const shouldOpenSidebar = isDesktop ? true : openSidebar;

	return (
		<div
			className={clsx({
				[mainClassStyles.minimalRoot]: true,
				[mainClassStyles.shiftContent]: isDesktop
			})}
		>
			<Topbar id="app-topbar" onSidebarOpen={handleSidebarOpen} />

			<div id="app-sidebar">
				<Sidebar 
					onClose={handleSidebarClose}
					open={shouldOpenSidebar}
					variant={isDesktop ? 'persistent' : 'temporary'}
				/>
			</div>

			<main id="app-body" className={mainClassStyles.minimaContent}>

				{children}

				<Footer />
			</main>
		</div>
	);
};

Main.propTypes = {
	children: PropTypes.node
};

export default Main;
