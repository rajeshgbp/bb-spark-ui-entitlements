import { makeStyles } from '@material-ui/styles';
// import styled from "styled-components";

// const Button = styled.button`
// background: ${props => props.primary ? "#000" : "#fff"};   
// color: ${props => props.primary ? "#fff" : "#000"};    
// font-size: 1em;   margin: 1em;   padding: 0.25em 1em;   
// border: 2px solid #2d2d2d;   border-radius: 3px; 
// `;



export const layoutStyles = makeStyles(theme => ({
    '@global':
    {
        body:
        {
            backgroundColor: theme.palette.background.default,
        },
        '.Toastify__toast--success': {
            background: '#8BC7AE'
        },
        '.Toastify__toast--warning': {
            background: '#ED8B00'
        }
    },
    buttons:
    {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    cancelButton:
    {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
    },
    removeButton:
    {
        marginTop: theme.spacing(2),
        // marginBottom: theme.spacing(1),
    },
    chooseImageButton:
    {
        marginTop: theme.spacing(2),
        backgroundColor: '#ffb300',
        color: '#000000',
        // paddingLeft: theme.spacing(2),
        // paddingRight: theme.spacing(2),
        // marginTop: theme.spacing(1),
        marginBottom: theme.spacing(2),
    },
    actionButton:
    {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
        backgroundColor: '#ffffff',
        color: '#000000',
        paddingLeft: theme.spacing(3),
        paddingRight: theme.spacing(3)
    },
    buttonGrey:
    {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
        backgroundColor: '#616161',
        color: '#fafafa',
    },
    buttonYellow:
    {
        marginTop: theme.spacing(3),
        backgroundColor: '#ffb300',
        color: '#000000',
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
    },
    themeGreyTextColor:
    {
        color: '#424242'
    },
    themeDarkTextColor:
    {
        color: '#00695f'
    },
    themeBlackTextColor:
    {
        color: '#000000',
    },
    themeWhiteColor:
    {
        color: '#FFF'
    },
    //Buttons in Unit Details
    greenBackground:
	{
		backgroundColor: '#e8f5e9',
		color: '#388e3c'
	},
	yellowBackground:
	{
		backgroundColor: '#fff9c4',
		color: '#f9a825'
	},
	redBackground:
	{
		backgroundColor: '#ffcdd2',
		color: '#e53935',
    },
    grayBackground:
    {
        backgroundColor: '#eeeeee',
		color: '#616161',

    },
    // All Units
    root:
    {
        backgroundColor: theme.palette.background.default,
        height: '100%',
        padding: theme.spacing(2)
    },
    serachRoot:
    {
        borderRadius: '4px',
        alignItems: 'center',
        padding: theme.spacing(1),
        display: 'flex',
        flexBasis: 420
    },
    form:
    {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    footerRoot: 
    {
        padding: theme.spacing(4)
    },
    minimalRoot:
    {
        paddingTop: 56,
        height: '100%',
        [theme.breakpoints.up('sm')]: 
        {
            paddingTop: 64
        }
    },
    minimaContent: 
    {
        height: '100%'
    },
    shiftContent: 
    {
        marginTop: -5,
        paddingLeft: 65,
        marginRight: -8,
        paddingBottom: -2,
    },
    drawer: 
    {
        width: 85,
        [theme.breakpoints.up('lg')]: {
            marginTop: 64,
            height: 'calc(100% - 64px)'
        }
    },
    sidebarRoot: 
    {
        backgroundColor: theme.palette.white,
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        padding: theme.spacing(2)
    },
    divider: 
    {
        margin: theme.spacing(2, 0)
    },
    sideNavBarRoot: {},
    item: 
    {
        display: 'flex',
        paddingTop: 0,
        paddingBottom: 0
    },
    sideNavBarbutton: 
    {
        color: '#37474f',
        padding: '10px 8px',
        justifyContent: 'flex-start',
        textTransform: 'none',
        letterSpacing: 0,
        width: '100%',
        fontWeight: theme.typography.fontWeightMedium
    },
    sideNavBarIcon: 
    {
        color: theme.palette.icon,
        width: 24,
        height: 24,
        display: 'flex',
        alignItems: 'center',
        marginRight: theme.spacing(1)
    },
    active: 
    {
        // color: theme.palette.primary.main,
        // fontWeight: theme.typography.fontWeightMedium,
        '& $icon': {
            color: '#4db6ac'
        },
        '& $text': {
            color: '#4db6ac'
        },
    },
    topbarRoot: 
    {
        [theme.breakpoints.up('lg')]: {
            color: '#26a69a',
            backgroundColor: '#ffffff',
            },
        [theme.breakpoints.between('sm', 'md')]: {
            color: '#26a69a',
            backgroundColor: '#ffffff',
        },
        color: '#FFFFFF',
        boxShadow: 'none',
        backgroundColor: '#35334e'
    },
    mobileTopbarRoot: 
    {
        color: '#FFFFFF',
        boxShadow: 'none',
        backgroundColor: '#35334e'
    },
    signInButton:
    {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        marginLeft: "auto",
        marginRight: 10,
        borderRadius: 10,
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
    },
    kycIcon:
    {
        marginLeft: "auto",
        marginRight: 10,
        marginTop: 4
    },
    flexGrow: 
    {
        flexGrow: 1
    },
    profileRoot:
    {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        minHeight: 'fit-content'
    },
    welcomeRoot:
    {
        margin: theme.spacing(2),
        padding: theme.spacing(2)
    },
    formRoot:
    {
        backgroundColor: theme.palette.common.white,
        margin: theme.spacing(2),
        padding: theme.spacing(2)
    },
    termsPaper:
    {
        maxHeight: 300, 
        overflow: 'auto', 
        padding: theme.spacing(2), 
    },
    content: 
    {
        paddingTop: 150,
        textAlign: 'center'
    },
    nonFoundImage: 
    {
        marginTop: 50,
        display: 'inline-block',
        maxWidth: '100%',
        width: 560
    },
    applyTextCenter:
    {
        textAlign: 'center',
    },
    marginTop1:
    {
        marginTop: theme.spacing(1)
    },
    marginTop3:
    {
        marginTop: theme.spacing(3),
    },
    marginBottom1:
    {
        marginBottom: theme.spacing(1)
    },
    marginTop2:
    {
        marginTop: theme.spacing(2)
    },
    marginTop4:
    {
        marginTop: theme.spacing(4)
    },
    marginBottom2:
    {
        marginBottom: theme.spacing(2)
    },
    marginTopBottom2:
    {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    dividerSpacing:
    {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1)
    },
    dividerSpacingTop2:
    {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(1)
    },
    cardPricing:
    {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'baseline',
        marginBottom: theme.spacing(2),
    },
    marginTopLeftRightBottom2:
    {
        marginTop: theme.spacing(2),
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    marginLeft2:
    {
        marginLeft: theme.spacing(2),
    },
    marginRight2:
    {
        marginRight: theme.spacing(2),
    },
    marginLeftRight2:
    {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
    },
    marginLeft3:
    {
        marginLeft: theme.spacing(3),
    },
    marginTopBottom1:
    {
        marginBottom: theme.spacing(1),
        marginTop: theme.spacing(1)
    },
    marginTop1Bottom2:
    {
        marginBottom: theme.spacing(2),
        marginTop: theme.spacing(1)
    },
    marginTopBottom3:
    {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    spacing1:
    {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1)
    },
    icon:
    {
        display: 'flex',
        alignItems: 'center',
        marginRight: theme.spacing(1)
    },
    searchIcon: 
    {
        marginRight: theme.spacing(1),
        color: theme.palette.text.secondary
    },
    iconColor:
    {
        color: '#009688'
    },
    input: 
    {
        flexGrow: 1,
        fontSize: '14px',
        lineHeight: '16px',
        letterSpacing: '-0.05px'
    },
    // mailIcon:
    // {
    //     display: 'flex',
    //     alignItems: 'center',
    //     marginRight: theme.spacing(3)
    // },
    media: 
    {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    mediaHeight: 
    {
        height: 150,
    },
    bgColorWhite:
    {
        backgroundColor: theme.palette.common.white,
        padding: theme.spacing(2)
    },
    cardHeader: {
        backgroundColor: theme.palette.common.white,
    },
    marginTopLeftRight2:
    {
        marginTop: theme.spacing(2),
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
    },
    marginTopLeftRightBottom1:
    {
        marginTop: theme.spacing(1),
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    cardSpacing:
    {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(4),
    },
    paddingLeftTopBottom2:
    {
        paddingLeft: theme.spacing(2),
        paddingTop: theme.spacing(1),
        paddingRight: theme.spacing(2),
    },
    padding2:
    {
        paddingLeft: theme.spacing(2),
        paddingTop: theme.spacing(1),
        paddingRight: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    paddingActionItem:
    {
        paddingLeft: '15%',
        paddingRight: '15%'
    },
    padding0:
    {
        padding: 0
    },
    paddingTopBottom0:
    {
        padding: 0
    },
    paddingLeft0:
    {
        paddingLeft: 0
    },
    padding:
    {
        padding: theme.spacing(2)
    },
    minWidth0:
    {
        minWidth: 0
    },
    marginTopLess:
    {
        marginTop: -30
    },
    speedDial: {
        position: 'fixed',
        '&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft': {
            bottom: theme.spacing(2),
            right: theme.spacing(2),
        },
        '&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight': {
            top: theme.spacing(2),
            left: theme.spacing(2),
        },
    },
    expandOpen:
    {
        transform: 'rotate(180deg)',
    },
    fab:
    {
        backgroundColor: '#f9a825',
        padding: 0,
        margin: 0, 
        width: 35, 
        height: 35,
        borderRadius: '20%'
    },
    addIconFab:
    {
        backgroundColor: '#f9a825',
        padding: 0,
        marginLeft: 10, 
        width: 35, 
        height: 35,
        borderRadius: '20%'
    },
    addIconFabZindex:
    {
        backgroundColor: '#f9a825',
        padding: 0,
        zIndex: 1, 
        position: 'absolute',
        marginLeft: 10, 
        marginTop: 8, 
        width: 35, 
        height: 35,
        borderRadius: '20%'
    },
    paperProps:
    {
        maxHeight: 48 * 4.5,
        width: 200,
    },
    avatar:
    {
        width: 80,
        height: 80,
        marginRight: theme.spacing(3)
    },
    profileAvatar:
    {
        width: 60,
        height: 60,
        color: '#fff',
        backgroundColor: '#673ab7',
    },
    avatarFull: 
    {
        width: 100,
        height: 100
    },
    avatarCircle: 
    {
        width: 80,
        height: 80,
        // marginRight: theme.spacing(2)
    },
    avatarImage: 
    {
        // margin: 10,
        width: 40,
        height: 40,
        color: '#fff',
        backgroundColor: '#bdbdbd',
    },
    hide:
    {
        display: 'none',
    },
    dots:
    {
        maxWidth: 50,
        flexGrow: 1,
    },
    marginRight0:
    {
        marginRight: 0
    },
    spacingForDiv:
    {
        marginTop: theme.spacing(4),
    },
    //Skeleton
    yellowCircle:
    {
        width: 8,
        height: 8,
        borderRadius: '50%',
        backgroundColor: '#ffc107', 
        marginRight: 5, 

    },
    darkYellowCircle:
    {
        width: 8,
        height: 8,
        borderRadius: '50%',
        backgroundColor: '#ff9800', 
        marginRight: 5,
    },
    skeletonYellow:
    {
        backgroundColor: '#ffc107', 
        marginRight: 5, 
        marginLeft: 10,
        width: 8,
        height: 8,
        borderRadius: '50%'
    },
    skeletonDarkYellow:
    {
        backgroundColor: '#ff9800', 
        marginRight: 5, 
        marginLeft: 10,
        width: 8,
        height: 8,
        borderRadius: '50%'
    },
    skeletonGreen:
    {
        backgroundColor: '#43a047', 
        marginRight: 5, 
        marginLeft: 10,
        width: 8,
        height: 8,
        borderRadius: '50%'
    },
    skeletonDarkGreen:
    {
        backgroundColor: '#1b5e20', 
        marginRight: 5, 
        marginLeft: 10,
        width: 8,
        height: 8,
        borderRadius: '50%'
    },
    listSkeletonYellow:
    {
        backgroundColor: '#ffc107', 
        marginRight: 10, 
        width: 8,
        height: 8,
        borderRadius: '50%'
    },
    listSkeletonDarkYellow:
    {
        backgroundColor: '#ff9800', 
        marginRight: 10, 
        width: 8,
        height: 8,
        borderRadius: '50%'
    },
    listSkeletonGreen:
    {
        backgroundColor: '#43a047', 
        marginRight: 10, 
        width: 8,
        height: 8,
        borderRadius: '50%'
    },
    listSkeletonDarkGreen:
    {
        backgroundColor: '#1b5e20', 
        marginRight: 10, 
        width: 8,
        height: 8,
        borderRadius: '50%'
    },
    //table
    table:
    {
        overflowX: 'auto',
        width: '100%'
    },
    // Box
    box: 
    {
        marginTop: theme.spacing(3),

        paddingLeft: theme.spacing(3),
        paddingTop: theme.spacing(1),
        paddingRight: theme.spacing(3),
        paddingBottom: theme.spacing(1),
        width: 'fit-content',
        borderColor: theme.palette.grey[500],
        border: `1px solid`,
        borderRadius: theme.shape.borderRadius,
        backgroundColor: theme.palette.background.paper,
        color: theme.palette.text.secondary,
        '& svg': {
            margin: theme.spacing(2),
        },
        '& hr': {
            margin: theme.spacing(0, 0.5),
        },
    },
     // Filter CSS
    appBar: 
    {
        position: 'relative',
        backgroundColor: '#ffffff'
    },
    filetrRoot:
    {
        backgroundColor: theme.palette.common.white,
        margin: theme.spacing(2),
        padding: theme.spacing(2)
    },
    filterDividerSpacing:
    {
        // margin: theme.spacing(1),
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3)
    },
    filterButtonActive:
    {
        backgroundColor: '#e0e0e0',
        borderColor: '#e0e0e0',
        marginTop: theme.spacing(2),
        marginRight: theme.spacing(2),
    },
    filterButtonSpacing:
    {
        marginTop: theme.spacing(2),
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        color: '#000000'
    },
    // buttons:
    // {
    //     display: 'flex',
    //     justifyContent: 'flex-end',
    // },
    applyButton:
    {
        marginTop: theme.spacing(2),
        marginLeft: theme.spacing(1),
        color: theme.palette.grey['50'],
        backgroundColor: '#00bfa5'
    },
    filterBody:
    {
        backgroundColor: theme.palette.background.default,
    },
    // filterButton:
    // {
    //     marginTop: theme.spacing(2),
    //     marginRight: theme.spacing(2),
    // },
    button:
    {
        marginTop: theme.spacing(2),
        marginRight: theme.spacing(2),
        color: '#00bfa5'
    },
    filterButtonLink:
    {
        color: '#000000'
    },
    //Bar collors
    yellowColor:
    {
        backgroundColor: '#ffc107'
    },
    darkYellowColor:
    {
        backgroundColor: '#ff9800'
    },
    greenColor:
    {
        backgroundColor: '#43a047'
    },
    darkGreenColor:
    {
        backgroundColor: '#1b5e20'
    },
    filterCloseIconColor:
    {
        color: '#000000'
    },
    marginRight5:
    {
        marginRight: 5
    },
    filterIconColor:
    {
        color: '#00695f'
    },
    cancelIconColor:
    {
        marginRight: 5, 
        color: '#9e9e9e'
    },
    lightGreyColor:
    {
        color: '#9e9e9e'
    },
    redColor:
    {
        color: '#e65100'
    },
    lighGreenLinkColor:
    {
        color: '#4db6ac'
    },
    lightGreenFilterIcon:
    {
        color: '#00bfa5'
    },
    small: {
        width: theme.spacing(4),
        height: theme.spacing(4),
      },
    marginTop:
    {
        marginTop: '20px',
    },
    Footercolor:{
        color: '#e0e0e0',
        backgroundColor: '#35334e',
        // marginTop: theme.spacing(4)
    },
    gridPadding:{
        paddingRight:"25px",
        paddingLeft:"90px"
    },
    gridContent:{
        boxSizing:"border-box",
        paddingTop:"30px",
        paddingBottom:"30px"
    },
    marginTop4:{
        marginTop: theme.spacing(4)
    },
    headerColor: {
        color: '#FFFFFF',
        padding: '16px',
        boxShadow: 'none',
        backgroundColor: '#35334e'
    },
    marginLeft15:
    {
        marginLeft: theme.spacing(5),
    },
    headersignInButton:
    {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        marginLeft: "auto",
        marginRight: 10,
        borderRadius: 55,
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        width:100,
        height:40,
        [theme.breakpoints.up('lg')]: {
            color: '#000',
            borderColor: '#e0e0e0'
            },
        [theme.breakpoints.between('sm', 'md')]: {
            color: '#000',
            borderColor: '#e0e0e0'
        },
        color: '#fff',
        borderColor: '#e0e0e0'
    },
}));

export default layoutStyles;