import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';

interface Props {
    layout: any;
	component: any;
	path: any;
}

const RouteWithLayout: React.FC<Props> = props =>
{
	const { layout: Layout, component: Component, ...rest } = props;

	return (
		<Route exact {...rest}
			render={matchProps => (
				<Layout>
					<Component {...matchProps} />
				</Layout>
			)}
		/>
	);
};

RouteWithLayout.propTypes = {
	component: PropTypes.any.isRequired,
	layout: PropTypes.any.isRequired,
	path: PropTypes.string
};

export default RouteWithLayout;