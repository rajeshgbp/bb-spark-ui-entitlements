import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import ThemeProvider from './utils/ThemeProvider';

import Routes from './Routes';
import history from './history';

import { API, UserService, PropertyService } from "./services";
import { UserType } from "./common/Constants";
import config from "./services/config";

// const BASE_PATH = '/app'; // TODO in JPMC network
const BASE_PATH = '';

// testing the commit removal

const App: React.FC<{}> = () =>
{
	function loadWePayScript() 
	{
		const wepaylibElemant = document.getElementById('wepaylib');
		const wepayScriptSrc = 'https://cdn.wepay.com/wepay.min.js';
		if (!wepaylibElemant)
		{
			const script = document.createElement('script');
			script.src = wepayScriptSrc;
			script.id = 'wepaylib';
			document.body.appendChild(script);

			script.onload = () =>
			{
				console.log('WePay cdn loaded.');
			};
		}
		else
		{
			console.log('WePay cdn existes.');
		}
	}

	const [isValid, setIsValid] = React.useState(false);

	function isUserRoleExists(roles, role) 
	{
		let index = roles.search(role);

		if (index >= 0)
		{
			return true;
		}

		return false;
	}

	function getConfig()
	{
		PropertyService.GetConfig(function (result)
		{
			if (result.status === 1)
			{
				let defaultData = result.data.lease;

				sessionStorage.setItem('rentpayDefaultDaysConfig', JSON.stringify(defaultData));
			}
			else
			{
				API.showError(result.message);
			}
		});
	}

	function getAuthenticate() 
	{
		sessionStorage.removeItem('rentpayUserAuthToken');
		sessionStorage.removeItem('rentpayAuthUserAdmin');
		
		UserService.AuthenticateUser(function (result)
		{
			if (result.status == 1)
			{
				let userDetails = result.data.user;

				if (userDetails.role.toUpperCase() == 'ADMIN' || isUserRoleExists(userDetails.role, UserType.ADMIN))
				{
					let user = {
						...userDetails,
						role: result.data.role.toUpperCase(),
						isOnboarded: false
					}
					sessionStorage.setItem('rentpayUserAuthToken', result.data.token);
	
					sessionStorage.setItem('rentpayAuthUserAdmin', JSON.stringify(user));
	
					setIsValid(true);
				}
				else
				{
					API.showError("Access denied for this application.");
				}
				getConfig();
			}
			else
			{
				API.showError(result.message);
			}
		});
	}

	React.useEffect(() =>
	{
		// loadWePayScript();
		getAuthenticate();
		console.log(config.app_version);
	}, []);

	return (
		<ThemeProvider >
			{/* {isValid ? */}
				<Router basename={BASE_PATH}>
					<Route history={history}>
						<Routes />
					</Route>
				</Router>
				{/* :
				''
			} */}
		</ThemeProvider>
	);
};

export default App;