import React from 'react';
import { Switch, Redirect } from 'react-router-dom';

import { Main as MainLayout } from './layouts';
import RouteWithLayout from './layouts/RouteWithLayout';


import {
	InviteUser,
	Roles,
	Users,
	ViewRole
} from './containers';
import ViewUserDetails from 'containers/NewUser/view-user-service';

const Routes: React.FC = () =>
{
	return (
		<Switch>
			<Redirect exact from="/" to="/entitlements" />			
			
			{/* Admin */}
			{/* <RouteWithLayout component={Users} layout={MainLayout} path="/entitlements/users" /> */}
			<RouteWithLayout component={ViewUserDetails} layout={MainLayout} path="/entitlements/user/:id" />
			<RouteWithLayout component={ViewUserDetails} layout={MainLayout} path="/entitlements/user/edit/:id" />
			<RouteWithLayout component={Roles} layout={MainLayout} path="/entitlements/roles" />
			<RouteWithLayout component={InviteUser} layout={MainLayout} path="/entitlements/inviteUser" />
			<RouteWithLayout component={ViewRole} layout={MainLayout} path="/entitlements/role/:id" />
            
			{/* <Redirect to="/welcomepage" /> */}
			<RouteWithLayout component={Users} layout={MainLayout} path="/entitlements" />

		</Switch>
	);
};

export default Routes;