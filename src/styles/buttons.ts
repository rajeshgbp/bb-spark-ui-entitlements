import { makeStyles } from '@material-ui/styles';
import Theme from 'utils/theme';

export const makeButtonStyles = makeStyles((theme: typeof Theme) => ({
    primary: {
        borderRadius: "20px",
        backgroundColor: '#ED8B00',
        color: '#2F2D46',
        paddingLeft: "20px",
        paddingRight: "20px",
        marginRight: "0px",
        marginTop: "0px",
        textAlign: "center"
    },
    
    PropertieslistviewFabButton: {
        borderRadius: '55px',
        top: theme.spacing(0),
        textTransform: 'none',
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        paddingLeft: 14,
        paddingRight: 24,
        paddingTop: 3,
        paddingBottom: 6
    },
    secondary: {
        borderRadius: "20px",
        border: '1px solid #D7D2CB',
        color: '#6B63A1',
        paddingLeft: "20px",
        paddingRight: "20px",
        marginRight: "0px",
        marginTop: "0px",
        textAlign: "center",
    },

    tertiary: {
        borderRadius: "20px",
        // border: '1px solid #D7D2CB',
        color: '#6B63A1',
        paddingLeft: "20px",
        paddingRight: "20px",
        marginRight: "0px",
        marginTop: "0px",
        textAlign: "center",
    },
    actionButton:
    {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        marginLeft: theme.spacing(1),
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4),
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
        textTransform: 'none',
        borderRadius: '55px',
        backgroundColor: '#ed8b00',
        color: '#000000',
    },
    fab: {
        position: 'absolute',
        top: theme.spacing(23),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "170px",
    },
    notebutton: {
        color: 'white', 
        marginTop: -7,
        borderRadius: 50,
    },
    bttButtom: {
        borderRadius: 50, 
        color: '#827baf', 
        backgroundColor: '#fff',
    },

    uploadButton:
    {
        backgroundColor: theme.palette.background.default,
        height: '100%',
        padding: theme.spacing(2),
        display: 'flex',
        justifyContent: 'flex-end',
    },
    cancelButton:
    {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        marginLeft: theme.spacing(1),
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4),
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
        textTransform: 'none',
        borderRadius: '55px',
    },
    settingButton:
    {
        borderRadius: '55px', 
        color: '#766ea8'
    },
    settingspaymentButton:
    {
        borderRadius: '55px', 
        textTransform: 'none', 
        backgroundColor: '#ed8b00', 
        paddingLeft: 14, 
        paddingRight: 24, 
        paddingTop: 4, 
        paddingBottom: 4
    },
    popoverButton:
    {
       borderRadius: 20, 
       backgroundColor: '#ed8b00', 
       marginLeft: 75, 
       height: 39, 
       borderColor: '#ed8b00'  
    },
    radioWidth:
    {
        width: '3%'
    },
    fabButton: {
        borderRadius: '55px',
        textTransform: 'none',
        backgroundColor: '#ed8b00',
        paddingLeft: 14,
        paddingRight: 24,
        paddingTop: 4,
        paddingBottom: 4,
    },
    dialogButton: {
        borderRadius: '55px',
        textTransform: 'none',
        backgroundColor: '#ed8b00',
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 8,
        paddingBottom: 8,
    },
    deleteButton: {
        borderRadius: '55px',
        textTransform: 'none',
        backgroundColor: '#ba482e',
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 8,
        paddingBottom: 8,
        '&:hover': {
            backgroundColor: '#ba482e',
          },
    },
    disableButton: {
        borderRadius: '55px',
        textTransform: 'none',
        backgroundColor: '#e8e1d9',
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 8,
        paddingBottom: 8,
    },
    selectAllButton: {
        borderRadius: '55px',
        textTransform: 'none',
        backgroundColor: 'white',
        marginTop: 12,
    },
    listviewFabButton: {
        borderRadius: '55px',
        textTransform: 'none',
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        paddingLeft: 14,
        paddingRight: 24,
        paddingTop: 6,
        paddingBottom: 6
    },
    submitButton:
    {
        // textTransform: 'none',
        backgroundColor: '#ed8b00',
        color: '#000000',
    },
    entityFab: {
        position: 'absolute',
        top: theme.spacing(23),
        right: theme.spacing(7),
        backgroundColor: '#ed8b00',
        color: '#000000',
        borderRadius: "26px",
        paddingLeft: "20px",
        paddingRight: "20px",
        height: "45px",
        width: "170px",
    },
}));
