import { makeStyles } from '@material-ui/styles';
import Theme from 'utils/theme';

export const makeIconStyles = makeStyles((theme: typeof Theme) => ({
	primary: {
		fontSize: 14,
		marginTop: 4
	},
	gray: {

		color: "lightGray"
	},
	red:{
		color:'#F4593D'
	},
	imageAvatar: {

		width: "60px",
		height: "60px",
	},
	sendAvatar: {

		width: "20px",
		height: "20px",
		backgroundColor: "#5d8bb9",
		//  fontSize:"12px",
	},
	dividerPadding: {
		padding: "2px"
	},
	dividerColor: {
		backgroundColor: "lightGray"
	},
	checkColor: {
		padding: 0,
	},
	searchIcon: {
		padding: theme.spacing(0, 2),
		height: '100%',
		position: 'absolute',
		pointerEvents: 'none',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	radioButtonAlign:
	{
		color: "#5d8bb9",
		marginLeft: 3,
	},
	TextFieldwidth: {
		width: "100%",
	},
	switchBottom: {
		marginTop: -8,
	},
	textFieldAlign: {
		marginRight: 19,
		marginTop: 16,
	},
	textFieldWidth1: {
		width: "250px",

	},

	createIconLeft:
	{
		marginLeft: "77px"
	},

	sendIconSizeXXXS: {
		fontSize: "12px",
	},
	warningIcon: {
		color: "#ba482e",
		marginTop: -7,
	},
	AlertText: {
		color: '#868594',
		marginTop: -7,
	},
	TextLeftMD: {
		marginLeft: "325px"
	},
	TextLeftLG: {
		marginLeft: "325px"
	},
	arrowIcon: {
		fontSize: "16px"
	},

	headerAvatar: {
		height: 68,
		width: 68
	},
	messageAvatar: {
		width: 35,
		height: 35
	},
	iconmiddle: {
		fontSize: 30,
		marginTop: 10,
		marginLeft: 10,
	},
	iconmiddleX: {
		fontSize: 30,
		marginTop: 20,
		marginLeft: 17,
	},
	fontsizelg: {
		fontSize: 30,
		marginLeft: 17,
		marginTop:3,
	},
	avatar4: {
		width: theme.spacing(4),
		height: theme.spacing(4),
	},
	avatar6: {
		width: theme.spacing(6),
		height: theme.spacing(6),
	},
	avatar7: {
		width: theme.spacing(7),
      height: theme.spacing(7),
	},
	avatar8: {
		width: theme.spacing(8),
		height: theme.spacing(8),
	},
	avatar10: {
		width: theme.spacing(10),
		height: theme.spacing(10),
	},
	avatarLarge: {

	},
	primarySize: {
		fontSize: 7
	},
	mediumSize: {
		fontSize: 8
	},
	bulletIconTop: {
		marginTop:"-1px"
	},

	iconMarginTop: {
		marginTop: "4px"
	},

	signalIcon: {
		width: "18px",
		height: "18px",
		backgroundColor: "#5d8bb9",
	},
	morevert: {
		width: "18px",
		height: "18px",
	},
	rotate90deg: {
		fontSize: "15px",
		transform: "rotate(90deg)",
	},
	warningIcon1:
	{
		fontSize: 22,
		color: '#ba482e'
	},
	locationIcon:
	{
		fontSize: 20,
	},
	mailIcon:
	{
		fontSize: 24,
	},
	phoneIcon:
	{
		fontSize: 24,
	},
	addIcon:
	{
		fontSize: 24,
		color: '#5a5397'
	},
	infooutlinedIcon:
	{
		color: '#6b63a1'
	},
	checkcircleIcon:
	{
		color: '#638db6'
	},
	darkgrey: {
		color: "rgb(109, 108, 125)"
	},
	arrowforwardiosIcon:
	{
		fontSize: 16,
		color: '#c4c3ca'
	},
	erroroutlineIcon:
	{
		transform: 'rotate(180deg)'
	},
	settingsAvatar:
	{
		width: 100,
		height: 100
	},
	settingsIcons:
	{
		fontSize: 24,
		marginTop: -1,
		color: '#766ea8'
	},
	FiberManualRecordIcon: {
		fontSize: 9,
	},
	orange:
	{
		color: '#de7700'
	},
	checkRoundedIcon: {
		color: '#5e8cb9'
	},
	invoiceAvatar: {
		width: 43,
		height: 43,
		marginTop: 2
	},
	meetingRoomIcon: {
		color: '#828190'
	},
	saveIcon: {
		color: "#6b63a1",
	},
	listViewWarningIcon: {
		color: '#ba482e',
	},
	wifiAvatar: {
		width: 20,
		height: 20,
		backgroundColor: '#5d8bb9',
		marginTop: 1
	},
	wifiAvatar1: {
		width: 20,
		height: 20,
		backgroundColor: 'grey', 
		marginTop: 1,
	},
	MoreHorizIcon: {
		fontSize: 20,
	},
	WiFiIcon: {
		fontSize: 15,
		transform: 'rotate(90deg)',
	},
	listviewAvatar: {
		marginLeft: 2,
		width: 64,
		height: 64,	 
	},
	iconCellWidth: {
		width: '1%',
		paddingRight: 15
	},
	ListIcon: {
		marginLeft: 4,
		marginTop: 4
	},
    blue: {
      color: "#6B63A1"
	},
	tableIcon: {
		fontSize: 14,
	},


}));