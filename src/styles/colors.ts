import { makeStyles } from '@material-ui/styles';
import Theme from 'utils/theme';

export const makeColorStyles = makeStyles((theme: typeof Theme) => ({

    white: {
        color: 'white'
    },
    headerColor: {
        color: '#FFFFFF',
        padding: '16px',
        boxShadow: 'none',
        backgroundColor: '#35334e'
    },

    checkIconColor: {
        color: "#5d8bb9",
    },
    checkcirlecolor: {
        color: '#638db6'
    },
    warningIconColor: {
        color: "#ba492f",
    },
    backgroundWhite: {
        backgroundColor: 'white'
    },
    gray: {
        color: "#9e9e9e",
    },
    pauseIcon: {
        color: "#ed8b00",
    },
    whiteBackground: {
        backgroundColor: 'white'
    },
    liteGreyBackground: {
        backgroundColor: '#f4f3f8'
    },
    medilumGreyBackground: {
        backgroundColor: '#e7e6f0'
    },
    purple: {
        color: '#766ea8'
    },
    red:
    {
        color: '#ba482e',
    },
    checkBox:
    {
        color: '#4e477a',
    },
    linkColor:
    {
        color: '#5a5397'
    },
    avatarBg:
    {
        backgroundColor: "#6a687b"
    },
    silver:
    {
        color: '#c4c3ca'
    },
    uploadColor:
    {
        color: "	#6b63a1"
    },
    warningIcon:
    {
        color: '#ba482e'
    },
    redPoint:
    {
        backgroundColor: '#ba482e',
        marginRight: 10,
        width: 15,
        height: 15,
        borderRadius: '50%'
    },
    violetPoint:
    {
        backgroundColor: '#40396c',
        marginRight: 10,
        width: 15,
        height: 15,
        borderRadius: '50%'
    },
    silverPoint:
    {
        backgroundColor: '#cecbe0',
        marginRight: 10,
        width: 15,
        height: 15,
        borderRadius: '50%'
    },
    lightBlack:
    {
        color: '#302E47'
    },
    blackDot:
    {
        backgroundColor: '#403a66',
        marginRight: 10,
        width: 15,
        height: 15,
        borderRadius: '50%'
    },
    vilotDot:
    {
        backgroundColor: '#5a5397',
        marginRight: 10,
        width: 15,
        height: 15,
        borderRadius: '50%'
    },
    blueDot:
    {
        backgroundColor: '#8c86b6',
        marginRight: 10,
        width: 15,
        height: 15,
        borderRadius: '50%'
    },
    silverDot:
    {
        backgroundColor: '#cecbe0',
        marginRight: 10,
        width: 15,
        height: 15,
        borderRadius: '50%'
    },
    currencyblueColor:
    {
        color: '#4a7b9c'
    },
    currencyredColor:
    {
        color: '#ba482e'
    },
    vilot: {
        color: '#322e45',
        fontSize: 15
    },
    tablecell: {
        backgroundColor: '#ecece4'
    },
    progressbarColor: {
        color: '#5d8bb9',
    },
    selectedTableRow: {
        backgroundColor:"#e6e5e9"
    },
   
    AvatarBackground: {
        backgroundColor: "gray"
    },

    textSecondary:{
        color:"gray",
    },
    lightOrange:{
         backgroundColor: "#f9dcb3",
    },
    black : {
        color:"black"
    },
    saveIcon: {
        color: '#828190'
    },
    headerDatacolor:{
        color:"#b5b4bd"
    },
    purple70:{
        backgroundColor:"#5B5397"
    },

}));