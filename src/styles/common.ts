import { makeStyles } from '@material-ui/styles';

import Theme from 'utils/theme';

export const makeCommonStyles = makeStyles((theme: typeof Theme) => ({

    contentRoot: {
        backgroundColor: '#f7f6f5',
        height: '100%',
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(5),
        [theme.breakpoints.up('lg')]: {
            paddingLeft: theme.spacing(6),
            paddingRight: theme.spacing(6),
            paddingTop: theme.spacing(3),
            paddingBottom: theme.spacing(5),
        },
        minHeight: 425
    },
    marginLeftXXS: {
        marginLeft: theme.spacing(1),
    },
    marginLeftXS: {
        marginLeft: theme.spacing(2),
    },
    marginLeftXXXS: {
        marginLeft: 5
    },
    marginLeftSM: {
        marginLeft: theme.spacing(3),
    },
    marginLeftMD: {
        marginLeft: theme.spacing(4),
    },
    marginLeftLG: {
        marginLeft: theme.spacing(4),
    },
    marginBottomXS: {
        marginBottom: theme.spacing(2),
    },
    marginTopXS: {
        marginTop: theme.spacing(2),
    },
    paddingTop4: {
        paddingTop: theme.spacing(4),
    },
    marginRightXS: {
        marginRight: theme.spacing(2),
    },
    marginTopXXS: {
        marginTop: theme.spacing(1),
    },
    marginTopXXXS: {
        marginTop: '6px',
    },
    marginTopsmall: {
        marginTop: "3px"
    },
    marginBottomXXS: {
        marginBottom: theme.spacing(1),
    },
    marginRightXXS: {
        marginRight: theme.spacing(1),
    },
    marginRight36: {
        marginRight: 36
    },
    marginTopSm: {
        marginTop: theme.spacing(3),
    },
    marginBottomSm: {
        marginBottom: theme.spacing(3),
    },
    marginRightSm: {
        marginRight: theme.spacing(3),
    },
    marginXXS: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        marginRight: theme.spacing(1),
        marginLeft: theme.spacing(1),
    },
    marginXS: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        marginRight: theme.spacing(2),
        marginLeft: theme.spacing(2),
    },
    marginSM: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        marginRight: theme.spacing(3),
        marginLeft: theme.spacing(3),
    },
    marginMD: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        marginRight: theme.spacing(4),
        marginLeft: theme.spacing(4),
    },
    marginLeft2: {
        marginLeft: theme.spacing(2),
    },
    marginLeft1: {
        marginLeft: theme.spacing(1),
    },
    marginRight2: {
        marginRight: theme.spacing(2),
    },
    marginLeftRight2: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
    },
    marginLeft3: {
        marginLeft: theme.spacing(3),
    },
    marginLeftLg: {
        marginLeft: theme.spacing(4),
    },
    marginLeftXLg: {
        marginLeft: theme.spacing(5),
    },
    marginTopBottom1: {
        marginBottom: theme.spacing(1),
        marginTop: theme.spacing(1)
    },
    marginTop1Bottom2: {
        marginBottom: theme.spacing(2),
        marginTop: theme.spacing(1)
    },
    marginTopBottom3: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    marginTop1: {
        marginTop: theme.spacing(1)
    },
    marginTop2: {
        marginTop: 13,
    },
    marginTop3: {
        marginTop: theme.spacing(3),
    },
    marginBottom1: {
        marginBottom: theme.spacing(1)
    },
    marginTop4: {
        marginTop: theme.spacing(4)
    },
    marginTopBottom2: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    marginTopLeftRight2: {
        marginTop: theme.spacing(2),
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
    },
    gridMargin: {
        marginLeft: theme.spacing(4),
        marginTop: '6px',
        marginBottom: theme.spacing(2)
    },
    heightXXS: {
        height: 0
    },
    heightXS: {
        height: 1
    },
    heightSm: {
        height: 2
    },
    spacing1: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1)
    },
    marginTopOver: {
        marginTop: -110,
    },
    marginTopOverLarge: {
        marginTop: -140,
    },
    marginTopOverLG: {
        marginTop: -97,
    },
    marginTopOverXXS: {
        marginTop: -3,
    },
    marginTopS3: {
        marginTop: 3,
    },
    marginRight5: {
        marginRight: 5
    },

    noPadding: {
        padding: 0
    },
    dividerSpacingXs: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(1)
    },
    noBorderRadius: {
        borderRadius: 0
    },
    textFieldBackroundColor: {
        backgroundColor: '#f7f6f5',
    },
    paddingRightXXS: {
        paddingRight: theme.spacing(1),
    },
    paddingRightXS: {
        paddingRight: theme.spacing(2),
    },
    paddingRightSM: {
        paddingRight: theme.spacing(3),
    },
    paddingRightMD: {
        paddingRight: theme.spacing(4),
    },
    paddingRightLG: {
        paddingRight: theme.spacing(5),
    },
    paddingBottomXXS: {
        paddingBottom: 0,
    },
    marginTopMD: {
        marginTop: theme.spacing(4)
    },
    marginTopLeftRightBottom2: {
        marginTop: theme.spacing(2),
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    headerColor: {
        color: '#FFFFFF',
        padding: '16px',
        boxShadow: 'none',
        backgroundColor: '#35334e'
    },
    typographyStyles3: {
        marginTop: theme.spacing(3),
        color: '#302E47',
        paddingBottom: 6
    },
    typographyStyles4: {
        marginTop: theme.spacing(4),
        color: '#302E47',
        paddingBottom: 6
    },
    radioButtonFormsXXS: {
        marginTop: theme.spacing(1),
        marginLeft: 24,
        marginBottom: 7
    },
    radioButtonFormsXS: {
        marginTop: theme.spacing(2),
        marginLeft: 24,
        marginBottom: 7
    },
    Noscrollbar: {
        '&::-webkit-scrollbar':
        {
            display: 'none',
            // flexWrap: 'nowrap',
        }
    },
    stepper: {

        fontSize: 15,
        color: '#766ea8'
    },
    fontFamily: {
        fontFamily: [
            "TN web use only",
        ].join(','),
    },
    headerMargin: {
        marginTop: 9,
        marginBottom: 9,
    },
    radioButtonMargin: {
        marginLeft: -6
    },
    actionButtonDisabled: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        marginLeft: theme.spacing(1),
        paddingLeft: theme.spacing(4),
        paddingRight: theme.spacing(4),
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
        textTransform: 'none',
        borderRadius: '55px',
        backgroundColor: '#e8e1d9',
        color: '#c4c3c9',
    },
    marginLeftXsBottomSm: {
        marginLeft: theme.spacing(2),
        marginBottom: theme.spacing(3)
    },
    marginTopXxsBottomMd: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(4)
    },
    marginTopXxsBottomXs: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(2)
    },
    marginRightSmTopSm: {
        marginRight: theme.spacing(3),
        marginTop: theme.spacing(3)
    },

    marginTopLeftRightBottom1: {
        marginTop: theme.spacing(1),
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },

    hide: {
        display: 'none',
    },
    StepHeader: {
        color: '#302E47',
        fontSize: 22,
        marginBottom: 13
    },
    marginLeftRight3: {
        marginLeft: 7,
        marginRight: 7
    },
    StepContent: {
        marginLeft: 5
    },
    padding0:
    {
        padding: 0
    },
    marginToplarge: {
        marginTop: "40px"
    },
    grayBackground:
    {
        backgroundColor: '#eeeeee',
        color: '#616161'
    },
    lightGreyBackground: {
        backgroundColor: '#eceae7',
    },
    noPaddingRight:
    {
        paddingRight: '0px'
    },
    marginLeft15:
    {
        marginLeft: theme.spacing(5),
    },
    marginLeft13:
    {
        marginLeft: 13,
    },
    marginTopBottomXsLeftLg:
    {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        marginLeft: theme.spacing(5)
    },
    dividerSpacingTop2:
    {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(1)
    },

    TextLeft: {
        marginLeft: "80px",
    },

    fab: {
        position: 'absolute',
        top: theme.spacing(23),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "170px",
    },
    leasefab: {
        position: 'absolute',
        top: theme.spacing(23.5),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "170px",
    },
     invoicefab: {
        position: 'absolute',
        top: theme.spacing(21),
        right: theme.spacing(8),
        borderRadius: "20px",
        backgroundColor: '#ED8B00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#2F2D46',
        height: "45px",
        width: "180px",

    },
    chatfab: {
        position: 'absolute',
        top: theme.spacing(17.5),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "157px",
    },
    Propertyfab: {
        position: 'absolute',
        top: theme.spacing(24),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "170px",
    },
    propertiesFabButtonMargin: {
        marginTop: -50,
    },
    invoicefabnew: {
        position: 'absolute',
        top: theme.spacing(24.5),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "170px",
    },
    unitsfab: {
        position: 'absolute',
        top: theme.spacing(24.5),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "170px",
    },
    propwithtabsfab: {
        position: 'absolute',
        top: theme.spacing(27),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "170px",
    },
    Propdetailsfab: {
        position: 'absolute',
        top: theme.spacing(35),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "170px",

    },
    editUserFab: {
        position: 'absolute',
        top: theme.spacing(35),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "100px",

    },
    Propdetails: {
        position: 'absolute',
        top: theme.spacing(32.5),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "130px",

    },
    Propdetailsfab1: {
        position: 'absolute',
        top: theme.spacing(31.6),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "130px",

    },
     detailsfab: {
        position: 'absolute',
        top: theme.spacing(34),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
          },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "170px",

    },
    color: {
        backgroundColor: theme.palette.background.paper,
    },
    inputRoot: {
        color: 'inherit',
    },

    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '6ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
    seletedcolor: {
        backgroundColor: '#bbe0cf',
        borderRadius: '50%'
    },
    lightgreencirclelg: {
        backgroundColor: '#bbe0cf',
        borderRadius: '50%'
    },
    brownCircle: {
        background: '#dadae1',
        color: 'black',
        width: 50,
        height: 50,
        borderRadius: '50%'
    },
    greencircleXlg: {
        background: '#bbe0cf', color: '#2c2d44',
        width: 50,
        height: 50,
        borderRadius: '50%'
    },
    graycircle: {
        backgroundColor: 'gray', marginTop: 9,
        width: 6,
        height: 6,
        borderRadius: '50%'
    },
    waringColor: {
        color: '#ba482e'
    },
    waringcircle: {
        backgroundColor: '#ba482e', marginRight: 10,
        width: 8,
        height: 8,
        borderRadius: '50%'
    },
    bluecircle: {
        backgroundColor: '#5c8bb9', marginRight: 10,
        width: 8,
        height: 8,
        borderRadius: '50%'
    },
    bluecirclecolor: {
        color: '#5c8bb9'
    },
    blueclirclelg: {
        backgroundColor: '#d0dff2',
        width: 25,
        height: 25,
        borderRadius: '50%'
    },
    orangecircle: {
        backgroundColor: '#d67c02', marginRight: 10,
        width: 8,
        height: 8,
        borderRadius: '50%'
    },
    orangecircleXlg: {
        background: '#f9dcb3', color: '#2c2d44',
        width: 50,
        height: 50,
        borderRadius: '60%'
    },
    orangecircleXXlg: {
        background: '#f9dcb3', color: '#2c2d44',
        width: 79,
        height: 79,
        borderRadius: '50%'
    },
    lightorangecircle: {
        background: '#f9dcb3',
        color: '#2c2d44',
        borderRadius: '50%'
    },
    yellowcirclelg: {
        backgroundColor: '#f1ecb3',
        width: 30,
        height: 30,
        borderRadius: '50%'
    },
    pinkcirclelg: {
        backgroundColor: '#f7d1ca',
        width: 30,
        height: 30,
        borderRadius: '50%'
    },
    pinkcircleXlg: {
        background: '#f7d1ca', color: '#2c2d44',
        width: 50,
        height: 50,
        borderRadius: '50%'
    },
    width: {
        width: '37px'
    },
    boxEdge: {
        borderRadius: "25px",
        backgroundColor: "#2f2d46",
        color: "lightGray",
    },
    boxEdgeBottomLeft: {
        borderRadius: "25px 25px 25px 0px",
        backgroundColor: "#dfdee2",
    },
    boxEdgeLeft: {
        borderRadius: "0px 25px 25px 0px",
        backgroundColor: "#dfdee2"
    },

    boxEdgeLeftTop: {
        borderRadius: "0px 25px 25px 25px",
        backgroundColor: "#dfdee2"
    },
    boxEdgerightBottom: {
        borderRadius: "25px 25px 0px 25px",
        backgroundColor: "#2f2d46",
        color: "lightGray"
    },
    boxEdgeRightTop: {
        borderRadius: "25px 0px 25px 25px",
        backgroundColor: "#2f2d46",
        color: "lightGray",
    },
    boxLeftEdge: {
        borderRadius: "25px",
        backgroundColor: "#dfdee2",
    },
    cardRadius: {
        borderRadius: "25px",
        backgroundColor: "#dfdee2",
    },
    marginTopMd: {
        marginTop: "8px"
    },
    avatar: {
        width: 60,
        height: 60
    },
    FullHeight: {
        height: '100%'
    },
    tenantAvatar: {
        width: 100,
        height: 100
    },
    marginTopLeft: {
        marginTop: '-5%',
        marginLeft: 10
    },
    marginTopXXSLeftBottom:
    {
        marginTop: theme.spacing(1),
        marginLeft: 4,
        marginBottom: 17
    },
    marginTopXSLeftBottom:
    {
        marginTop: theme.spacing(2),
        marginLeft: 4,
        marginBottom: 17
    },
    formControl: {
        minWidth: 230,
    },
    radioButtonWidth: {
        width: '3%'
    },
    radioButtonLabelWidth: {
        width: '97%'
    },
    stepperButtonMargin: {
        marginTop: 35,
    },
    stepperButton: {
        fontSize: 14,
        color: '#322e45',
    },

    pointer: {
        cursor: 'pointer',
    },
    marginLeftXsmall: {

        marginLeft: "1px"
    },
    dynamicList: {
        backgroundColor: '#d5d5da',
        marginBottom: -16
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    paddingLeftRight: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
    },
    paddingleftrightXlg: {
        paddingLeft: 12,
        paddingRight: 12
    },
    paddingrightXlg: {
        paddingRight: 15
    },
    noPaddingTop: {
        paddingTop: 0,
    },
    pdtmarginTop:
    {
        marginTop: "120px"
    },
    alignCenter:
    {
        align: "center"
    },
    marginToppx:
    {
        marginTop: "5px"
    },
    marginTop5:
    {
        marginTop: 5
    },
    marginTopXXSBottom:
    {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(4)
    },
    marginLeftpadding:
    {
        padding: "unSet",
        marginLeft: theme.spacing(3),
    },
    tabPadding:
    {
        padding: "unSet",
    }, 
    marginTopn60: { 
        marginTop: -60 
    },
    autoWidth: {
        width: "100%",
    },
    paddingLeftAvatar:
    {
        marginLeft: 11
    },
    paddingLeft:
    {
        paddingLeft: '15%'
    },
    marginLeftXXLg: {
        marginLeft: theme.spacing(6),
    },
    marginTopspace: {
        marginTop: '29%'
    },
    extraHeight: {
        height: "57px"
    },
    extraHeight1: {
        height: "223px"
    },
    onboardingRadio: {
        marginLeft: -7
    },
    fullWidth:
    {
        width: "100%"
    },
    paymentWidth:
    {
        width: '97%'
    },
    noPaddingtop:
    {
        paddingTop: 0
    },
    stepperMargin:
    {
        marginLeft: 34
    },
    extraMargin:
    {
        marginLeft: 90,
        marginTop: -20,
        marginBottom: 10
    },
    dividerMargin:
    {
        marginLeft: 7,
        marginRight: 7,
        marginTop: 24
    },
    stepperImage:
    {
        height: 55, width: '100%'
    },
    marginTopOver1: {
        marginTop: -97,
    },
    widthLarge: {

        width: '200px'
    },
    marginLeft12:
    {
        marginLeft: 12
    },
    paddingXXXS:
    {
        padding: 2
    },
    paddingRightSmall:
    {
        paddingRight: 11
    },
    paddingRightXXXS:
    {
        paddingRight: 8
    },
    nopaddingTopBottom: {
        paddingBottom: 0,
        paddingTop: 0
    },
    nopaddingLeftRight: {
        paddingLeft: 0,
        paddingRight: 0
    },
    nopaddingLeft: {
        paddingLeft: 0,
    },
    widthExtralarge:
    {
        width: "600px"
    },
    amountTextField: {
        width: 160,
        marginLeft: 6,
        marginTop: -10,
    },
    dateTextField: {
        width: 203
    },
    uploadPhoto: {
        border: "1px solid grey",
        width: "100%",
        borderStyle: "dashed",
        height: "150px",
    },
    paddingLeftSmall: {
        paddingLeft: '10px',
    },

    paddingTop5: {
        paddingTop: 5
    },
    paddingTop10: {
        paddingTop: 10
    },
    paddingTopLeft5: {
        paddingTop: 5,
        paddingLeft: 5
    },
    paddingTopBottom10: {
        paddingTop: 10,
        paddingButtom: 10
    },
    paddingTop5Left14: {
        paddingTop: 5,
        paddingLeft: 14
    },
    headerFilfter: {
        width: 160,
        marginTop: 2,
        marginBottom: 2,
        height: 40,
    },
    paddingWelcomesetup:{
        paddingLeft: theme.spacing(3),
        paddingRight: theme.spacing(3),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        borderRadius:50
    },
    filterLabel: {
        marginTop: -10,
    },
    marginTopRightMD: {
        marginRight: 16,
        marginTop: 19,

    },
    paddingXLarge: {
        padding: "23px",
    },

    notespopoverwidth: {
        width: 465
    },
    ViewList: {
        marginLeft: 4,
        marginTop: 4,
    },
    selecteditem: {
        backgroundColor: '#dcdae9',
        borderColor: '#6b63a1',
        width: "2.1rem",
        height: "2rem",
    },
    notselecteditem: {
        backgroundColor: '',
        borderColor: 'lightGrey',
        width: "2.1rem",
        height: "2rem",
    },
    chatWindowWidth: {
        width: "500px",
    },
    position: {
        position: 'absolute',
        width: 430,
        zIndex: 1,
    },
    marginTop30: {
        marginTop: -30,
    },
    marginTop25: {
        marginTop: -25,
    },
    marginLeftRight10per: {
        marginLeft: '10%',
        marginRight: '10%'
    },
    marginLeftRight10TopBottom5per: {
        marginLeft: '10%',
        marginRight: '10%',
        marginTop: '5%',
        marginBottom: '5%'
    },
    histoypinon: {
        height: '100%',
        width: 4,
    },
    histoypinonselected: {
        height: '100%',
        width: 4,
        backgroundColor: '#766ea8'
    },
    pinonMain: {
        backgroundColor: '#f4f3f8',
        width: '100%'
    },
    cellDescription: {
        width: "20%"
    },
    cellRate: {
        width: "17%"
    },
    cellwidth4: {
        width: "4%"
    },
    cellAmount: {
        width: "15%"
    },
    paddingRightxsSmall: {
        paddingRight: '2px'
    },

    marginLeft03Per: {
        marginLeft: '0.3%'
    },
    margin83per: {
        width: '79%'
    },
    margin58per: {
        width: '67%'
    },
    margin40per: {
        width: '70%'
    },
    margin82per: {
        width: '81%'
    },
    marginwith3per: {
        width: '3%'
    },
    marginwith5per: {
        width: '3.5%'
    },
    marginwith4per: {
        width: '6%'
    },
    marginTop03per: {
        marginTop: '0.3%'
    },
    width95per: {
        width: '94.07%'
    },
    width70per: {
        width: '92.5%'
    },
    width58per: {
        width: '58%'
    },
    tabspadding: {
        paddingBottom: 0,
        paddingLeft: '3.5%'
    },
    tabspadding0: {
        paddingBottom: 0
    },
    cellPadding: {
        paddingTop: '16px',
        paddingBottom: '16px'
    },
    tablewidth26: {
        width: "26%"
    },
    tablewidth39: {
        width: "39%"
    },
    tablewidth22: {
        width: "22%"
    },
    tablewidth15: {
        width: "15%"
    },
    tablewidth24: {
        width: "24%"
    },
    occupiedTableCellWidth: {
        width: "10%"
    },    
    tenantNameWidth: {
        width: '260px'
    },
    leaseNameWidth: {
        width: '500px'
    },
    descriptionTableCell: {
        width: '22%'
    },
    rateTableCell: {
        width: '14%'
    },
    quantityTableCell: {
        width: '7%'
    },
    invoiceTextfieldWidth: {
        width: 250,
    },
    progressbarWidth: {
        width: 285,
    },
    cardBorderColor: {
        borderTop: '0.2px solid lightGrey'
    },
    marginRight30: {
        marginRight: 30
    },
    Avatar: {
        width: 57,
        height: 57
    },
    absolutePositon: {
        position: "absolute",
        width: 223,
        zIndex: 1
    },
    paddingTop4per: {
        paddingTop: '4%'
    },
    steps: {
        marginLeft: 31,
    },
    subSteps: {
        marginLeft: 34,
    },
    cardHeight: {
        height: 55,
        width: '100%'
    },
    marginLeftXXLarge: {
        marginLeft: 24,
    },
    checkBoxMargin: {
        marginTop: -8,
    },
    selectedTableRow: {
        backgroundColor: "#e6e5e9"
    },
    whitecolor: {
        backgroundColor: 'white'
    },
    DoneIconColor: {
        color: 'white',
        backgroundColor: '#673ab7'
    },
    width500: {
        width: 500
    },
    popoverWidth: {
        width: 515
    },
    marginTopS4: {
        marginTop: 1,
    },
    notificationFormControl: {

        minWidth: 220,
        marginBottom: 4

    },
    popupStateMargin: {
        marginRight: 6,
        marginTop: 18
    },
    fabButtonMargin: {
        marginTop: -50,
    },
    dialogTextfieldWidth: {
        width: 263,
    },
    overFlowauto: {
        overflow: 'auto'
    },
    selectedChip: {
        background: '#e8e5f0',
        color: '#6f6a8a'
    },
    selectedlease: {
        background: '#e8e5f0',
        color: '#2c2d44'
    },
    selectedprofile: {
        background: '#e8e5f0',
        color: '#6f6a8a'
    },
    popover: {
        overflow: 'auto',
        height: 350
    },
    noMarginRight: {
        marginRight: 0,
    },
    graphheight:{
        height:'175px'
    },
    bargraphHeight: {
        height: '180px',
    },
    linegraphHeight: {
        height: '180px',
    },
    marginTopXLarge: {
        marginTop: 20
    },
    tabWidth: {
        marginRight: 76
    },
    firstTabWidth: {
        marginRight: 90
    },
    radioMargin: {
        marginLeft:32,
        marginTop:7,
        marginBottom:7
    },
    chatdoneIcon:{
        width: 36, height: 36,
         marginTop: "4px",
         backgroundColor:"#6b63a1",
    },
    Avatarsquare: {
        width: 70, 
        height: 70,
        borderRadius: 15
 },
    Avatartoprightradius10: {
        width: 70,
        height: 70,
        borderRadius: 15,
        borderTopRightRadius: 10
 },
    daysRedditTextFieldMargin: {
        marginRight: 21,
        marginTop: 16,
    },
    dueDateRedditTextFieldMargin: {
        marginRight: 19,
        marginTop: 16,
    },
    dashboardcard: {
        height: 315,
    },
    tabletext:{
        maxWidth: 150, 
        whiteSpace: 'nowrap'
    },
    leaseTemplate: {
        position: 'absolute',
        top: theme.spacing(23),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
            },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "170px",
    },
    templateDialog: {
        width: "500px",
    },
    OtherCharges: {
        position: 'absolute',
        top: theme.spacing(23),
        right: theme.spacing(8),
        backgroundColor: '#ed8b00',
        '&:hover': {
            backgroundColor: "#ed8b00",
        },
        color: '#000000',
        borderRadius: "26px",
        height: "45px",
        width: "170px",
    },
    textuppercase:{
        textTransform:"uppercase"
    },
    tabelCellAmount: {
        width: "14%" 
    },

    tabelAmountCell: {
        width: "17%" 
    },
    textAlignRight: {
        textAlign:"right"
    },

    textAlignCenter: {
        textAlign:"center"
    },
    collapseHeight: {
        maxHeight:"350px",
        overflow:"auto"
    },
    textTransform:{
        textTransform:"none"
    },

    tenantCardHeight:{
        height:"200px"
    },

    helperTextfontSize:{
        fontSize:13,
        color:'#6a697a'
    },
    errorHelperTextfontSize:
    {
        fontSize:13,
        color:'#BA482E'
    },
    centerAlign:{
        display:'flex',
        alignItems:'center'
    },
    rightAlign:{
        display:'flex',
        justifyContent:'flex-end',
        alignItems:'center'
    },
    height3:{
        height:'3rem'
    }
}));