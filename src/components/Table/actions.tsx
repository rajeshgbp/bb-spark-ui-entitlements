import React from 'react';
import Typography from '../../components/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { makeIconStyles } from "../../styles/icons";
import { makeCommonStyles } from "../../styles/common";

interface Props
{
    row: any;
    actions?: any;
    closePopUp?: any;
};


const Actions: React.FC<Props> = (props) =>
{
    const { row, actions, closePopUp } = props;

    const commonStyles = makeCommonStyles();
    const iconStyles = makeIconStyles();

    function onSelectAction(act: any) 
    {
        if (act.callbackArguments)
        {
            if (act.callbackArguments.length === 0)
            {
                act.callback();
            }
            else if (act.callbackArguments.length === 1)
            {
                act.callback(row[act.callbackArguments[0]]);
            }
            else if (act.callbackArguments.length === 2)
            {
                act.callback(row[act.callbackArguments[0]], row[act.callbackArguments[1]]);
            }
            else if (act.callbackArguments.length === 3)
            {
                act.callback(row[act.callbackArguments[0]], row[act.callbackArguments[1]], row[act.callbackArguments[2]]);
            }

            closePopUp();
        }
    }

    function cellDisplayContent(column: any) 
    {
        let value = '';

        if (column.customCell && column.customColumns)
        {
            if (column.customColumns.length === 0)
            {
                value = column.customCell();
            }
            else if (column.customColumns.length === 1)
            {
                value = column.customCell(row[column.customColumns[0]]);
            }
            else if (column.customColumns.length === 2)
            {
                value = column.customCell(row[column.customColumns[0]], row[column.customColumns[1]]);
            }
            else if (column.customColumns.length === 3)
            {
                value = column.customCell(row[column.customColumns[0]], row[column.customColumns[1]], row[column.customColumns[2]]);
            }
        }

        return value;
    }

    return (
        <Box >
            {actions && actions.map((action, index) => (
                <>
                    <div className={commonStyles.marginTopLeftRightBottom2} key={index}>
                        <Button style={{textTransform: 'none'}} data-testid= {index == 0 ? "action" : "action" + index }    onClick={() => onSelectAction(action)} startIcon={action.icon} ><Typography >&nbsp;{action && action.customColumns && action.customColumns.length > 0 ? cellDisplayContent(action) : action.text}</Typography></Button>
                    </div>

                    {action.isDivider ? <Divider /> : ''}
                </>
            ))}

        </Box>
    );
};


export default Actions;