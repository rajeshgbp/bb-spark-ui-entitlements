import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Link from '@material-ui/core/Link';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Typography from '../../components/Typography';
import PopupState, { bindTrigger, bindPopover } from 'material-ui-popup-state';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { Checkbox } from '@material-ui/core'
import Popover from '@material-ui/core/Popover';
import IconButton from '@material-ui/core/IconButton';
// import { makeButtonStyles } from "styles/buttons";
import { makeCommonStyles } from "../../styles/common";
// import { makeIconStyles } from "styles/icons";
import { makeColorStyles } from "../../styles/colors";
import { ArrowUpward, ArrowDownward } from '@material-ui/icons'


import Actions from './actions';

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
});

interface Props {
    columns: any;
    rows: any;
    actions?: any;
    onSelectAll?: any;
    onSelectItem?: any;
    source?: any;
    compRef?: any;
    sortingDetails?:any;
    handleSorting?:any;
};

const DynamicTable: React.FC<Props> = ({ columns, rows, actions, onSelectItem, source, compRef,sortingDetails,handleSorting }: Props) => {
    const classes = useStyles();
    const commonStyles = makeCommonStyles();
    const colorStyles = makeColorStyles();
    const [noInvoiceActionList, setNoInvoiceActionList] = React.useState([])

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
   
    React.useEffect(() => {
        window.scrollTo(0, 0);
        if (source === 'unitsList') {
            let DuplicateActions = [...actions]
            DuplicateActions.splice(1, 1)
            setNoInvoiceActionList([...DuplicateActions])
        }
    }, []);


    const handleChangePage = (event, newPage) => {
        // window.scrollTo(0, 0);
        // if (compRef.current) 
        // {
        //     compRef.current.scrollIntoView();
        // }
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    function onSelectCell(event: any, column: any, row: any, index: number) {
        if (event.key === 'Enter') {

            event.preventDefault();
            if (column.callback && column.callbackArguments) {
                if (column.callbackArguments.length === 0) {
                    column.callback();
                }
                else if (column.callbackArguments.length === 1) {
                    column.callback(row[column.callbackArguments[0]]);
                }
                else if (column.callbackArguments.length === 2) {
                    column.callback(row[column.callbackArguments[0]], row[column.callbackArguments[1]]);
                }
                else if (column.callbackArguments.length === 3) {
                    column.callback(row[column.callbackArguments[0]], row[column.callbackArguments[1]], row[column.callbackArguments[2]]);
                }
            }
        }
        else {

            event.preventDefault();
            if (column.callback && column.callbackArguments) {
                if (column.callbackArguments.length === 0) {
                    column.callback();
                }
                else if (column.callbackArguments.length === 1) {
                    column.callback(row[column.callbackArguments[0]]);
                }
                else if (column.callbackArguments.length === 2) {
                    column.callback(row[column.callbackArguments[0]], row[column.callbackArguments[1]]);
                }
                else if (column.callbackArguments.length === 3) {
                    column.callback(row[column.callbackArguments[0]], row[column.callbackArguments[1]], row[column.callbackArguments[2]]);
                }
            }
        }
    }

    function cellDisplayContent(column: any, row: any) {
        let value = '';

        if (column.customCell && column.customColumns) {
            if (column.customColumns.length === 0) {
                value = column.customCell();
            }
            else if (column.customColumns.length === 1) {
                value = column.customCell(row[column.customColumns[0]]);
            }
            else if (column.customColumns.length === 2) {
                value = column.customCell(row[column.customColumns[0]], row[column.customColumns[1]]);
            }
            else if (column.customColumns.length === 3) {
                value = column.customCell(row[column.customColumns[0]], row[column.customColumns[1]], row[column.customColumns[2]]);
            }
        }
        else if (column.format && typeof row[column.id] === 'number') {
            value = column.format(row[column.id]);
        }
        else {
            value = row[column.id];
        }

        return value;
    }

    function showIcon(column: any, row: any) {
        if (column.showIcon && column.iconArguments) {
            if (column.iconArguments.length === 0) {
                return (
                    <Grid item>
                        {column.showIcon()}
                    </Grid>
                );
            }
            else if (column.iconArguments.length === 1) {
                return (
                    <Grid item>
                        {column.showIcon(row[column.iconArguments[0]])}
                    </Grid>
                );
            }
            else if (column.iconArguments.length === 2) {
                return (
                    <Grid item>
                        {column.showIcon(row[column.iconArguments[0]], row[column.iconArguments[1]])}
                    </Grid>
                );
            }
            else if (column.iconArguments.length === 3) {
                return (
                    <Grid item>
                        {column.showIcon(row[column.iconArguments[0]], row[column.iconArguments[1]], row[column.iconArguments[2]])}
                    </Grid>
                );
            }
        }
        return '';
    }

    function actionMenuWithNoDeactivate(details) {
        if (details.status === 'Deactivated') {
            return true
          }
          else {
            return false;
          }
    }

    return (
        <Paper className={classes.root}>
            <TableContainer >
                <Table aria-label="sticky table" size="small">
                    <TableHead  >
                        <TableRow  >
                            {/* <TableCell style={{ width: 1 }} className={colorStyles.tablecell}>
                                <Checkbox /><div className={commonStyles.hide}>Checkbox</div></TableCell> */}
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    className={colorStyles.tablecell}
                                    onClick={() => {
                                        if (!column.ignoreSorting) {
                                            handleSorting(column.id)
                                        }
                                    }}
                                >
                                    <Grid container alignItems='center'>
                                        <Typography variant="body2" weight="bold" align={column.alignText ? column.alignText : 'left'}> {column.label}</Typography>
                                        &nbsp;
                                        {
                                            (sortingDetails && sortingDetails.columnId === column.id) && (
                                                <>
                                                    {
                                                        sortingDetails.orderBy === 'ASC' ? (<ArrowUpward />) : (<ArrowDownward />)
                                                    }
                                                </>
                                            )
                                        }
                                    </Grid>
                                </TableCell>
                            ))}
                            {actions ?
                                <TableCell align="right" className={colorStyles.tablecell}><Typography variant="body2" weight="bold">Action</Typography></TableCell>
                                : ''
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
                            return (
                                <TableRow hover tabIndex={-1} key={row.code}>

                                    {columns.map((column, idx) => {
                                        const value = row[column.id];
                                        return (
                                            <TableCell key={column.id} align={column.align ? column.align : 'left'} >
                                                <Grid container wrap="nowrap" alignItems='center'>

                                                    {showIcon(column, row)}
                                                    <Grid item xs zeroMinWidth >
                                                        {column.callback && column.callbackArguments ?
                                                            (
                                                                <Typography noWrap variant="body2" className={column.callback ? commonStyles.pointer : ''} tabIndex={0} data-testid={'clickEvent' + idx} onKeyPress={(event) => onSelectCell(event, column, row, index)} onClick={(event) => onSelectCell(event, column, row, index)} align={column.alignText ? column.alignText : 'left'}>

                                                                    {cellDisplayContent(column, row)}

                                                                </Typography>
                                                            ) :
                                                            (
                                                                <Typography noWrap variant="body2" className={column.callback ? commonStyles.pointer : ''} style={{ verticalAlign: 'middle', display: 'flex', alignItems: 'center' }} align={column.alignText ? column.alignText : 'left'}>

                                                                    {cellDisplayContent(column, row)}

                                                                </Typography>
                                                            )}

                                                    </Grid>
                                                </Grid>

                                            </TableCell>
                                        );
                                    })}
                                    {
                                        actions ?
                                            <TableCell align="right">
                                                <Typography>

                                                    <PopupState variant="popover" popupId="demo-popup-popover">
                                                        {(popupState) => (
                                                            <div>

                                                                <IconButton data-testid={"MoreVertIcon" + index} aria-label={"Dynamic table actions" + index} {...bindTrigger(popupState)} >
                                                                    <MoreVertIcon aria-hidden="false" />
                                                                </IconButton>

                                                                {actions && actions.length > 0 ?
                                                                    <Popover {...bindPopover(popupState)}
                                                                        anchorOrigin={{ vertical: 'bottom', horizontal: 'left', }}
                                                                        transformOrigin={{ vertical: 'top', horizontal: 'right', }}
                                                                    >
                                                                        {actionMenuWithNoDeactivate(row) ?
                                                                            (
                                                                                <Actions actions={actions.filter(i=>i.text !== 'Deactivate')} row={row} closePopUp={popupState.close} />
                                                                            ) :
                                                                            (
                                                                                <Actions actions={actions} row={row} closePopUp={popupState.close} />
                                                                            )
                                                                        }

                                                                    </Popover>

                                                                    :
                                                                    ''
                                                                }
                                                            </div>
                                                        )}
                                                    </PopupState>

                                                </Typography>
                                            </TableCell>
                                            : ''
                                    }
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer >
            <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={rows.length}
                labelRowsPerPage={"Results per page:"}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                    inputProps: { 'aria-label': 'rows per page' },
                    native: true,
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </Paper >
    );
};

DynamicTable.propTypes = {
    columns: PropTypes.object,
    rows: PropTypes.object
};

export default DynamicTable;