import React from 'react';
import TextField from '@material-ui/core/TextField';
import { createStyles, fade, Theme, makeStyles } from '@material-ui/core/styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import { OutlinedInputProps } from '@material-ui/core/OutlinedInput';
import SearchIcon from '@material-ui/icons/Search';
import Typography from 'components/Typography';
import { makeCommonStyles } from "styles/common";
import { makeIconStyles } from "styles/icons";
import Chip from '@material-ui/core/Chip';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';
import SendIcon from '@material-ui/icons/Send';
import { makeColorStyles } from "styles/colors";
import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';
import {
    FormControl, MenuItem, Select, InputLabel, Button,
    ListItemText, Checkbox, Divider
} from '@material-ui/core'
import './textField.scss'

const textFieldStyles = makeStyles((theme) => ({
    root: {
        borderBottom: "2px solid #9e9e9e",
        overflow: 'hidden',
        // borderRadius: 0,
        backgroundColor: 'white',
        width: 'auto',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:hover': {
            backgroundColor: '#fff',
        },
        '&$focused': {
            backgroundColor: '#fff',
        },
        '&$error': {
            borderBottom: '1px solid #BA482E',
        },
        "& input::-webkit-clear-button, & input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button": {
            display: "none",
            "-webkit-appearance": "none"
        },

        "& input": {
            "-moz-appearance": "textfield",
            margin: 0
        }
    },
    label: {
        "&$focusedLabel": {
            color: "#6B63A1"
        },
        '&$error': {
            color: "#BA482E"
        }
    },
    error: {},
    focusedLabel: {},
    focused: {},
}));



const useStylesReddit = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            border: '1px solid #e2e2e1',
            overflow: 'hidden',
            borderRadius: 0,
            backgroundColor: 'white',
            transition: theme.transitions.create(['border-color', 'box-shadow']),
            '&:hover': {
                backgroundColor: '#fff',
            },
            '&$focused': {
                backgroundColor: '#fff',
                boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
                borderColor: theme.palette.primary.main,
            }, '&$error': {
                border: '1px solid #BA482E',
            },
            "& input::-webkit-clear-button, & input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button": {
                display: "none",
                "-webkit-appearance": "none"
            },
        },
        label: {
            "&$focusedLabel": {
                color: "#6B63A1"
            },
            '&$error': {
                color: "#BA482E"
            }
        },
        error: {},
        focused: {},
    }));

const useStylesForDateField = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            border: '1px solid #e2e2e1',
            overflow: 'hidden',
            borderRadius: 0,
            backgroundColor: 'white',
            transition: theme.transitions.create(['border-color', 'box-shadow']),
            '&:hover': {
                backgroundColor: '#fff',
            },
            '&$focused': {
                backgroundColor: '#fff',
                boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
                borderColor: theme.palette.primary.main,
            }, '&$error': {
                border: '1px solid #BA482E',
            },
            "& input::-webkit-clear-button, & input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button": {
                display: "none",
                "-webkit-appearance": "none"
            },
        },
        label: {
            "&$focusedLabel": {
                color: "#6B63A1"
            },
            '&$error': {
                color: "#BA482E"
            }
        },
        error: {},
        focused: {},
        input: {
            cursor: "pointer",
            "&::-webkit-calendar-picker-indicator": {
                cursor: "pointer"
            }
        },
    }));

const useStylesRedditdisabled = makeStyles((theme) => ({
    root: {
        border: '1px solid #e2e2e1',
        overflow: 'hidden',
        borderRadius: 0,
        backgroundColor: 'white',
        width: 'auto',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:hover': {
            backgroundColor: '#fff',
        },
        '&$focused': {
            backgroundColor: '#fff',
            boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
            borderColor: theme.palette.primary.main,
        },
    },
    focused: {},
}));

const useStylesRedditListView = makeStyles((theme) => ({
    root: {
        marginTop: -3,
        fontSize: 14,
        border: '1px solid #e2e2e1',
        overflow: 'hidden',
        borderRadius: 0,
        backgroundColor: 'white',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:hover': {
            backgroundColor: '#fff',
        },
        '&$focused': {
            backgroundColor: '#fff',
            boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
            borderColor: theme.palette.primary.main,
        },

    },
    focused: {},
}));

const useStylesRedditdisabled1 = makeStyles((theme) => ({
    root: {
        border: '1px solid #e2e2e1',
        overflow: 'hidden',
        borderRadius: 0,
        backgroundColor: 'white',
        width: 'auto',
        height: "70px",
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:hover': {
            backgroundColor: '#fff',
        },
        '&$focused': {
            backgroundColor: '#fff',
            boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
            borderColor: theme.palette.primary.main,
        },
    },
    focused: {},
}));

function AmountInput(props) {
    // Amount format
    const { inputRef, onChange, ...other } = props;

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={values => {
                onChange({
                    target: {
                        value: values.value,
                    },
                });
            }}
            thousandSeparator
            isNumericString
            decimalScale={2}
            fixedDecimalScale={true}
            allowNegative={false}
        />
    );
};

AmountInput.propTypes = {
    inputRef: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
};
interface Props {
    className?: any;
    label?: any;
    required?: true;
    defaultValue?: string;
    endAdornment?: any;
    variant?: any;
    id?: string;
    InputProps?: any;
    value?: any;
    onChange?: any;
    select?: true;
    SelectProps?: any;
    helperText?: string;
    size?: any;
    onClick?: any;
    multiline?: any;
    placeholder?: string;
    disabled?: any;
    areaLabel?: any;
    autoFocus?: any;
    validators?: any;
    maxLength?: any;
    errorMessages?: any;
    min?: any;
    max?: any;
    validationContent?: any;
}

export const RedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const { id, maxLength } = TextFieldProps;

    return (
        <>
            <TextField
                inputProps={{ "data-testid": id, maxLength: maxLength }}
                InputProps={{ classes, id, disableUnderline: true, } as Partial<OutlinedInputProps>} {...TextFieldProps}
            />
        </>
    );
};


export const LeasetemplateNameField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const { id, maxLength } = TextFieldProps;

    return (
        <>
            <TextValidator
                inputProps={{ "data-testid": id, maxLength: maxLength }}
                InputLabelProps={{
                    classes: {
                        root: classes.label,
                        error: classes.error
                    },
                }}
                FormHelperTextProps=
                {{
                    style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
                }}
                InputProps={{ classes, id, disableUnderline: true, } as Partial<OutlinedInputProps>} {...TextFieldProps}
            />
        </>
    );
};


export const SelectTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const { id } = TextFieldProps;

    return (
        <>
            <TextField
                inputProps={{ "data-testid": id }}
                InputProps={{ classes, id, disableUnderline: true, } as Partial<OutlinedInputProps>}
                {...TextFieldProps}
            />
        </>
    );
};

export const MultiSelectTextField: any = (TextFieldProps) => {
    const classes = useStylesReddit();
    const { id, selectedValue, onChange, label, values, showAll } = TextFieldProps;

    return (
        <FormControl
            variant="outlined" fullWidth
        >
            <InputLabel id="multi-select-label" style={selectedValue.length === 0 ? { marginBottom: '30px' } : { margin: '-15px 5px 5px 0px' }}>{label}</InputLabel>
            <Select labelId="multi-select-label"
                inputProps={{
                    name: "multiSelect",
                    id: "multiSelect"
                }}
                value={selectedValue}
                onChange={onChange}
                renderValue={(selected: any) => {
                    if (selected.includes('All')) {
                        return 'All'
                    }
                    else
                        return selected.join(', ')
                }}
                multiple
                id={id}
            >
                <MenuItem value={'All'}>
                    <Checkbox checked={selectedValue.includes('All')} />
                    <ListItemText primary={'All'} />
                </MenuItem>
                {
                    values.map(i => (
                        <MenuItem key={i.id} value={i.id}>
                            <Checkbox checked={selectedValue.includes(i.id)} />
                            <ListItemText primary={i.value} />
                        </MenuItem>
                    ))
                }
                <Divider />
                <MenuItem value={'clear'}>
                    <Button disabled={selectedValue.length === 0} fullWidth disableFocusRipple disableRipple>Clear Selection</Button>
                </MenuItem>
            </Select>
        </FormControl>
    );
};

export const LeaseSelectTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const { id } = TextFieldProps;

    return (
        <>
            <TextValidator
                inputProps={{ "data-testid": id }}
                InputProps={{ classes, id, disableUnderline: true, } as Partial<OutlinedInputProps>}
                InputLabelProps={{
                    classes: {
                        root: classes.label,
                        error: classes.error
                    },
                }}
                FormHelperTextProps=
                {{
                    style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
                }}
                {...TextFieldProps}
            />
        </>
    );
};

export const SquareFeetField1: React.FC<Props> = (TextFieldProps) => {
    const classes = textFieldStyles();
    const { id, value, validationContent } = TextFieldProps;

    var accepts8DigitsAnd2Decimals = /^\d{0,8}(\.\d{1,2})?$/;

    ValidatorForm.addValidationRule('accepts8DigitsAnd2Decimals', (value) => {
        if ((accepts8DigitsAnd2Decimals.test(value) == false) && (value.toString().length > 0)) {
            return false;
        }
        else if (value > 0) {
            return true;
        };

    });

    ValidatorForm.addValidationRule('valueGreaterThanZero', (value) => {

        if (value) {

            if (value <= 0) {
                return false;
            }
            else if (value > 0) {
                return true;
            }
        }

    });
    return (
        <div>
            <TextValidator
                id={id}
                onFocus={e => e.target.select()}
                inputProps={{ min: 0, style: { textAlign: 'right' }, 'data-testid': id, 'aria-label': 'Charge Rate', maxLength: 15 }}
                InputProps={{
                    inputComponent: AmountInput,
                    startAdornment: <InputAdornment position="start" >
                        <Typography color="textSecondary" variant="body2">$</Typography>
                    </InputAdornment>,
                    classes, disableUnderline: true
                } as Partial<OutlinedInputProps>}
                InputLabelProps={{
                    classes: {
                        root: classes.label,
                        error: classes.error
                    },
                }}
                FormHelperTextProps=
                {{
                    style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
                }}
                {...TextFieldProps}
                validators={['required', 'valueGreaterThanZero', 'accepts8DigitsAnd2Decimals']}
                errorMessages={['This field is required', 'Value must be greater than zero', `Maximum number of ${validationContent} is 99,999,999.99`]}
            />
        </div >
    );
};

export const ValidateField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const { id, maxLength } = TextFieldProps;
    return (
        <div>
            <TextValidator
                id={id}
                onFocus={e => e.target.select()}
                InputProps={{ classes, disableUnderline: true, } as Partial<OutlinedInputProps>} {...TextFieldProps}
                inputProps={{ 'data-testid': id, maxLength: maxLength }}
                InputLabelProps={{
                    classes: {
                        root: classes.label,
                        error: classes.error
                    },
                }}
                FormHelperTextProps=
                {{
                    style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
                }}
                validators={['required', 'trim']}
                errorMessages={['This field is required', 'The field should not be empty']}
            />
        </div >
    );
};

export const SquareFeetField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    return (
        <div>
            <TextField onFocus={e => e.target.select()} inputProps={{ min: 0, style: { textAlign: 'left' } }} InputProps={{
                endAdornment: <InputAdornment position="start" >
                    <Typography color="textSecondary" variant="body2">sq. ft.</Typography>
                </InputAdornment>, classes, disableUnderline: true,
            } as Partial<OutlinedInputProps>} {...TextFieldProps} />
        </div >
    );
};

export const SearchIconRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const commonStyles = makeCommonStyles();
    return <TextField onFocus={e => e.target.select()} InputProps={{
        endAdornment: (
            <InputAdornment position="end">
                <SearchIcon aria-hidden="false" className={commonStyles.pointer} />
            </InputAdornment>
        ), classes, disableUnderline: true
    }} {...TextFieldProps} />;
};

export const DaysRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const commonStyles = makeCommonStyles();

    return <TextField InputProps={{ classes, disableUnderline: true, endAdornment: <InputAdornment position="end"><Typography color="textSecondary" className={commonStyles.daysRedditTextFieldMargin} >days</Typography></InputAdornment>, }} {...TextFieldProps} />;
};

export const LeaseDaysRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const commonStyles = makeCommonStyles();

    return <TextValidator
        InputProps={{ classes, disableUnderline: true, endAdornment: <InputAdornment position="end"><Typography color="textSecondary" className={commonStyles.daysRedditTextFieldMargin} >days</Typography></InputAdornment>, }}
        InputLabelProps={{
            classes: {
                root: classes.label,
                error: classes.error
            },
        }}
        FormHelperTextProps=
        {{
            style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
        }}
        {...TextFieldProps} />;
};

export const DollarRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const commonStyles = makeCommonStyles();
    const { id, validationContent } = TextFieldProps;

    var accepts8DigitsAnd2Decimals = /^\d{0,8}(\.\d{1,2})?$/;

    ValidatorForm.addValidationRule('accepts8DigitsAnd2Decimals', (value) => {
        if ((accepts8DigitsAnd2Decimals.test(value) == false) && (value.toString().length > 0)) {
            return false;
        }
        else if (value > 0) {
            return true;
        };

    });

    ValidatorForm.addValidationRule('valueGreaterThanZero', (value) => {

        if (value) {

            if (value <= 0) {
                return false;
            }
            else if (value > 0) {
                return true;
            }
        }

    });

    return (
        <>
            <TextValidator
                id={id}
                inputProps={{ "data-testid": id }}
                onFocus={e => e.target.select()}
                InputProps={{
                    classes, disableUnderline: true,
                    inputComponent: AmountInput,
                    startAdornment: <InputAdornment position="start">
                        <Typography color="textSecondary" className={commonStyles.marginTopS3}>$</Typography>
                    </InputAdornment>,
                }}
                FormHelperTextProps={{
                    style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
                }}
                InputLabelProps={{
                    classes: {
                        root: classes.label,
                        // focused: classes.focusedLabel,
                        error: classes.error
                    },
                }}
                validators={['required', 'valueGreaterThanZero', 'accepts8DigitsAnd2Decimals']}
                errorMessages={['This field is required', 'Value must be greater than zero', `Maximum number of ${validationContent} is 99,999,999.99`]}
                {...TextFieldProps}
            />
        </>
    );
};

export const InlineCalendarIconRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesForDateField();
    // const commonStyles = makeCommonStyles();
    const { id } = TextFieldProps;
    return <TextValidator
        id={id}
        onFocus={e => e.target.select()}
        inputProps={{ 'data-testid': id, min: new Date().toISOString().split('T')[0], max: "9999-12-31" }}
        type="date" placeholder="mm/dd/yyyy" InputProps={{ classes, disableUnderline: true }}
        InputLabelProps={{
            shrink: true,
            classes: {
                root: classes.label,
                // focused: classes.focusedLabel,
                error: classes.error
            },
        }}
        FormHelperTextProps=
        {{
            style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
        }}

        {...TextFieldProps} />;
    // endAdornment: <InputAdornment position="end"><Typography color="textSecondary"><CalendarTodayIcon className={commonStyles.pointer}/></Typography></InputAdornment>,
};

export const LeaseDollarRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const commonStyles = makeCommonStyles();
    const { id } = TextFieldProps;
    return <TextValidator
        id={id}
        inputProps={{ 'data-testid': id }}
        onFocus={e => e.target.select()}
        InputLabelProps={{
            classes: {
                root: classes.label,
                error: classes.error
            },
        }}
        FormHelperTextProps=
        {{
            style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
        }}
        InputProps={{
            classes, disableUnderline: true,
            inputComponent: AmountInput,
            startAdornment: <InputAdornment position="start">
                <Typography color="textSecondary" className={commonStyles.marginTopS3}>$</Typography>
            </InputAdornment>,
        }}
        {...TextFieldProps} />;
};

export const DueDateRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const commonStyles = makeCommonStyles();

    return <TextField InputProps={{ classes, disableUnderline: true, endAdornment: <InputAdornment position="end"><Typography color="textSecondary" className={commonStyles.dueDateRedditTextFieldMargin}>days after due date</Typography></InputAdornment>, }} {...TextFieldProps} />;
};

export const StartDateRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesForDateField();
    const { id } = TextFieldProps;
    return <TextValidator
        id={id}
        onFocus={e => e.target.select()}
        inputProps={{ 'data-testid': id, max: "9999-12-31" }}
        type="date"
        InputProps={{ classes, disableUnderline: true }}
        InputLabelProps={{
            shrink: true,
            classes: {
                root: classes.label,
                error: classes.error
            },
        }}
        FormHelperTextProps=
        {{
            style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
        }}{...TextFieldProps} />;
};

export const CollectedOnTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesForDateField();
    const { id } = TextFieldProps;
    return <TextValidator
        id={id}
        onFocus={e => e.target.select()}
        inputProps={{ 'data-testid': id, max: new Date().toISOString().split('T')[0] }}
        type="date"
        InputProps={{ classes, disableUnderline: true }}
        InputLabelProps={{
            shrink: true,
            classes: {
                root: classes.label,
                error: classes.error
            },
        }}
        FormHelperTextProps=
        {{
            style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
        }}{...TextFieldProps} />;
};

export const BeforeDueDateRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const commonStyles = makeCommonStyles();
    const { id } = TextFieldProps;

    return <TextField inputProps={{ 'data-testid': id }} InputProps={{ classes, disableUnderline: true, endAdornment: <InputAdornment position="end"><Typography color="textSecondary" className={commonStyles.dueDateRedditTextFieldMargin}>days before due date</Typography></InputAdornment>, }} {...TextFieldProps} />;
};

export const MemoRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const { id, areaLabel, maxLength } = TextFieldProps;
    const classes = useStylesRedditdisabled();

    return <TextField onFocus={e => e.target.select()} id={id} inputProps={{ 'data-testid': id, maxLength: maxLength, 'aria-label': areaLabel }} InputProps={{ classes, disableUnderline: true }} {...TextFieldProps} />;
};

export const MovingOutRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const iconStyles = makeIconStyles();

    return <TextValidator
        InputLabelProps={{
            shrink: true,
            classes: {
                root: classes.label,
                // focused: classes.focusedLabel,
                error: classes.error
            },
        }}
        FormHelperTextProps=
        {{
            style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
        }}
        InputProps={{ classes, disableUnderline: true, endAdornment: <InputAdornment position="end"><Typography color="textSecondary" className={iconStyles.textFieldAlign}>days after move out</Typography></InputAdornment>, }} {...TextFieldProps} />;
};

export const InsertInvitationRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesForDateField();
    // const commonStyles = makeCommonStyles();

    return <TextValidator type="date"
        onFocus={e => e.target.select()}
        inputProps={{ min: new Date().toISOString().split('T')[0], max: "9999-12-29" }}
        InputProps={{ classes, disableUnderline: true }}
        InputLabelProps={{
            shrink: true,
            classes: {
                root: classes.label,
                // focused: classes.focusedLabel,
                error: classes.error
            },
        }}
        FormHelperTextProps={{
            style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
        }}
        {...TextFieldProps} />;
    //  endAdornment: <InputAdornment position="end"><Typography color="textSecondary"><InsertInvitationIcon className={commonStyles.pointer}/></Typography></InputAdornment>,
};

export const RedditTextFieldDisabled: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const colorStyles = makeColorStyles();
    const commonStyles = makeCommonStyles();
    return <TextField onFocus={e => e.target.select()} InputProps={{ classes, disableUnderline: true, endAdornment: <InputAdornment position="end"><AttachFileIcon className={colorStyles.gray + " " + commonStyles.pointer} />&nbsp;&nbsp;&nbsp;&nbsp;<EmojiEmotionsIcon className={colorStyles.gray + " " + commonStyles.pointer} />&nbsp;&nbsp;&nbsp;<  SendIcon color="primary" className={commonStyles.pointer} /></InputAdornment>, }} {...TextFieldProps} />;
};

export const ChatRedditTextField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesRedditdisabled1();
    const handleDelete = () => {
        console.info('You clicked the delete icon.');
    };
    return <TextField onFocus={e => e.target.select()} InputProps={{
        classes, disableUnderline: true, startAdornment: <InputAdornment position="start">  <Chip label="Charles,Luther" disabled onDelete={handleDelete} color="default" /> &nbsp;&nbsp;
            <Chip label=" Howlett,Logan" onDelete={handleDelete} color="default" />
        </InputAdornment>,
    }} {...TextFieldProps} />;
};

export const TenantNameField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const { id, areaLabel } = TextFieldProps;
    return (
        <div>
            <TextValidator
                id={id}
                onFocus={e => e.target.select()}
                InputLabelProps={{
                    classes: {
                        root: classes.label,
                        error: classes.error
                    },
                }}
                FormHelperTextProps=
                {{
                    style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
                }}
                InputProps={{ classes, id, disableUnderline: true, } as Partial<OutlinedInputProps>} {...TextFieldProps}
                inputProps={{ 'data-testid': id, 'aria-label': areaLabel, maxLength: 30 }} />
        </div >
    );
};

export const EmailField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const { id, areaLabel } = TextFieldProps;
    return (
        <div>
            <TextValidator
                id={id}
                onFocus={e => e.target.select()}
                InputLabelProps={{

                    classes: {
                        root: classes.label,
                        error: classes.error
                    },
                }}
                FormHelperTextProps=
                {{
                    style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
                }}
                InputProps={{ classes, id, disableUnderline: true, } as Partial<OutlinedInputProps>} {...TextFieldProps}
                inputProps={{ 'data-testid': id, 'aria-label': areaLabel }} />
        </div >
    );
};

export const DollarRedditTextFieldForProcessing: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesReddit();
    const commonStyles = makeCommonStyles();
    const { id } = TextFieldProps;

    var accepts5DigitsAnd2Decimals = /^\d{0,5}(\.\d{1,2})?$/;

    ValidatorForm.addValidationRule('accepts5DigitsAnd2Decimals', (value) => {
        if ((accepts5DigitsAnd2Decimals.test(value) == false) && (value.toString().length > 0)) {
            return false;
        }
        else if (value > 0) {
            return true;
        };

    });

    ValidatorForm.addValidationRule('valueGreaterThanZero', (value) => {

        if (value) {

            if (value <= 0) {
                return false;
            }
            else if (value > 0) {
                return true;
            }
        }

    });

    return (
        <>
            <TextValidator
                id={id}
                inputProps={{ "data-testid": id }}
                onFocus={e => e.target.select()}
                InputProps={{
                    classes, disableUnderline: true,
                    inputComponent: AmountInput,
                    startAdornment: <InputAdornment position="start">
                        <Typography color="textSecondary" className={commonStyles.marginTopS3}>$</Typography>
                    </InputAdornment>,
                }}
                FormHelperTextProps={{
                    style: { marginRight: "0px", color: "#BA482E", fontSize: "12px", leterSpacing: "-0.2px", lineHeight: "14px" }
                }}
                InputLabelProps={{
                    classes: {
                        root: classes.label,
                        // focused: classes.focusedLabel,
                        error: classes.error
                    },
                }}
                validators={['required', 'valueGreaterThanZero', 'accepts5DigitsAnd2Decimals']}
                errorMessages={['This field is required', 'Value must be greater than zero', 'Only 5 digits and 2 decimals are allowed']}
                {...TextFieldProps}
            />
        </>
    );
};

export const DateField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesForDateField();
    const { id } = TextFieldProps;

    let minDate = TextFieldProps.min ? TextFieldProps.min : '1974-01-01';
    let maxDate = TextFieldProps.max ? TextFieldProps.max : "9999-12-29";

    return (
        <TextValidator
            onFocus={e => e.target.select()}
            inputProps={{ 'data-testid': id, min: minDate, max: maxDate }}
            type="date"
            InputProps={{ classes, disableUnderline: true }}
            InputLabelProps={{ shrink: true }}
            {...TextFieldProps}
        />
    );
};

export const EndDateField: React.FC<Props> = (TextFieldProps) => {
    const classes = useStylesForDateField();
    const { id } = TextFieldProps;

    // let minDate = TextFieldProps.min ? TextFieldProps.min : '1974-01-01';
    let maxDate = TextFieldProps.max ? TextFieldProps.max : "9999-12-29";

    return (
        <TextValidator
            onFocus={e => e.target.select()}
            inputProps={{ 'data-testid': id, max: maxDate }}
            type="date"
            InputProps={{ classes, disableUnderline: true }}
            InputLabelProps={{ shrink: true }}
            {...TextFieldProps}
        />
    );
};