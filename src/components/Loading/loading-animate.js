import * as React from "react";
import styled, { keyframes } from "styled-components";
import Icon, { InlineSvg, Glyph } from "./icon";
import useTheme from "@material-ui/core/styles/useTheme";

export const firstFade = keyframes`
    0% {
        opacity:0
    }
    20% {
        opacity:.5
    }
    40% {
        opacity:1
    }
    60% {
        opacity:.5
    }80% {
        opacity:.5
    }
    100% {
        opacity:0
    }
`;

export const secondFade = keyframes`
    0% {
        opacity:0
    }
    20% {
        opacity:.5
    }
    40% {
        opacity:1
    }
    60% {
        opacity:.5
    }80% {
        opacity:.5
    }
    100% {
        opacity:0
    }
`;

export const thirdFade = keyframes`
    0% {
        opacity:0
    }
    20% {
        opacity:.5
    }
    40% {
        opacity:1
    }
    60% {
        opacity:.5
    }80% {
        opacity:0
    }
    100% {
        opacity:0
    }
`;

export const fourthFade = keyframes`
    0% {
        opacity:0
    }
    25% {
        opacity:.5
    }
    50% {
        opacity:.8
    }
    75% {
        opacity:0
    }
    100% {
        opacity:.0
    }
`;


export const fifthFade = keyframes`
    0% {
        opacity:0
    }
    25% {
        opacity:.5
    }
    50% {
        opacity:.8
    }
    75% {
        opacity:0
    }
    100% {
        opacity:0
    }
`;


export const sixthFade = keyframes`
    0% {
        opacity:0
    }
    25% {
        opacity:1
    }
    50% {
        opacity:.2
    }
    75% {
        opacity:0
    }
    100% {
        opacity:.0
    }
`;


export const FirstIcon = styled(({ fill, ...rest }) => (
    <InlineSvg>
        <Glyph glyph="rectangleSecondary" fill={fill} />
    </InlineSvg>
))`
    animation : ${firstFade} 3000ms linear infinite;
    animation-delay: 0ms;
    transaction : ${props => props.theme.transitions.create("opacity")};
`;

export const SecondIcon = styled(({ fill, ...rest }) => (
    <InlineSvg {...rest}>
        <Glyph glyph="quarter" fill={fill} />
    </InlineSvg>
))`
    animation: ${secondFade} 3000ms linear infinite;
    animation-delay: 500ms;
    transition: ${props => props.theme.transitions.create("opacity")};
`;

export const ThirdIcon = styled(({ fill, ...rest }) => (
    <InlineSvg {...rest}>
        <Glyph glyph="polygonPrimary" fill={fill} />
    </InlineSvg>
))`
    animation: ${thirdFade} 3000ms linear infinite;
    animation-delay: 1000ms;
    transition: ${props => props.theme.transitions.create("opacity")};
`;

export const FourthIcon = styled(({ fill, ...rest }) => (
    <InlineSvg {...rest}>
        <Glyph glyph="circle" fill={fill} />
    </InlineSvg>
))`
    animation: ${fourthFade} 3000ms linear infinite;
    animation-delay: 1500ms;
    transition: ${props => props.theme.transitions.create("opacity")};
`;

export const FifthIcon = styled(({ fill, ...rest }) => (
    <InlineSvg {...rest}>
        <Glyph glyph="polygonPrimary" fill={fill} />
    </InlineSvg>
))`
    animation: ${fifthFade} 3000ms linear infinite;
    animation-delay: 2000ms;
    transition: ${props => props.theme.transitions.create("opacity")};
`;

export const SixthIcon = styled(({ fill, ...rest }) => (
    <InlineSvg {...rest}>
        <Glyph glyph="polygonSecondary" fill={fill} />
    </InlineSvg>
))`
    animation: ${sixthFade} 3000ms linear infinite;
    animation-delay: 2500ms;
    transition: ${props => props.theme.transitions.create("opacity")};
`;

export const LoadingIcons = styled.div`
    margin-left: 0px;
`;

const LoadingAnimation = () =>
{
    const theme = useTheme();

    return (
        <LoadingIcons>
            <Icon fill={theme.palette.tertiary.warmGray}>
                <FirstIcon theme={theme} />
            </Icon>

            <Icon fill={theme.palette.tertiary.glacireBlue}>
                <SecondIcon theme={theme} />
            </Icon>

            <Icon fill={theme.palette.primary.dark}>
                <ThirdIcon theme={theme} />
            </Icon>

            <Icon fill={theme.palette.tertiary.warmGray}>
                <FourthIcon theme={theme} />
            </Icon>

            <Icon fill={theme.palette.primary.main}>
                <FifthIcon theme={theme} />
            </Icon>

            <Icon fill={theme.palette.primary.main}>
                <SixthIcon theme={theme} />
            </Icon>
        </LoadingIcons>
    );
};

export default LoadingAnimation;