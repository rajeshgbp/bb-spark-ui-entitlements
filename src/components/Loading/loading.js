import * as React from "react";
import styled from "styled-components";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import useTheme from "@material-ui/core/styles/useTheme";
import LoadingAnimation from "./loading-animate";

export const LoadingTitle = styled(({ ...otherProps }) => (
    <Typography variant="h5" component="h2" role="status" {...otherProps} />
))`
    color: ${props => props.theme.palette.primary.main};
    padding: ${props => props.theme.spacing(2)}px;
    text-align: center;
    .loadiingHeading {
    margin-left: -60px;
    position: relative;
    }
    .paper {
    margin: 0px;
    float: left;
}
`;


export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 100%;
    width: 100%;
    min-height: 320px;
    flex: 1 0 auto;
`;

const Loading = (props) =>
{
    const theme = useTheme();

    let loadingContent = props.message || "Your content is loading.";

    return (
        <Card>
            <LoadingTitle theme={theme}>
                <Typography> {loadingContent} </Typography>
            </LoadingTitle>
            <Grid container direction="column" justify="center" alignItems="center" >
                <Grid item xs={12} sm={12} md={12} >
                    <LoadingAnimation />
                </Grid>
            </Grid>
        </Card>
    );
};

export default Loading;