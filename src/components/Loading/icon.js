import React from 'react';
import styled from "styled-components";

export const InlineSvg = styled.svg`
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    height: 100%;
    width: 100%;
    color: inherit;
` ;

export const SvgWrapper = styled.div`
    display: inline-block;
    flex: 0 0 ${props => props.size}px;
    width: ${props => props.size}px;
    height: ${props => props.size}px;
    min-width: ${props => props.size}px;
    min-height: ${props => props.size}px;
    position: relative;
    color: inherit;
` ;

export const Glyph = (props) =>
{
    const { glyph, stroke, fill } = props;

    switch (glyph)
    {
        case "map":
            return (
                <g>
                    <path
                        fill={fill}
                        d="M12,11.5A2.5,2.5 0  0,1 9.5,9A2.5,2.5 0 0,1 12,6.5A2.5 0 0,1 14.5,9A2.5,2.5 0 0,1 12,11.5M12,2A7,7 0 0,0 5,9C5,14.25 12,22 12,22C12,22 19,14.25 19,9A7,7 0 0,0 12,2Z"
                    />
                </g>
            );
        case "linkedIn":
            return (
                <g>
                    <path
                        fill={fill}
                        strokeLinecap="square"
                        d="M21,21H17V14.25C17,13.19 15.81,12.31 14.75,12.31C13.69,12.31 13,13.19 13,14.25V21H9V9H13V11C13.66,9.93 15.36,9.24 16.5,9.24C19,9.24 21,11.28 21,13.75V21M7,21H3V9H7V21M5,3A2,2 0 0,1 7,5A2,2 0 0,1 5,7A2,2 0 0,1 3,5A2,2 0 0,1 5,3Z"
                    />
                </g>
            );
        case "web":
            return (
                <g>
                    <path
                        fill={fill}
                        stroke={stroke}
                        d="M16.36,14C16.44,13.34 16.5,12.68 16.5,12C16.5,11.32 16.44,10.66 16.36,10H19.74C19.9,10.64 20,12C20,12.69 19.9,13.36 19.74,14M14.59,19.56C15.19,18.45 15.65,17.25 15.97,16H18.92C17.96,17.65 16.43,18.93 14.59,19.56M14.34,14H9.66C9.56,13.34 9.5,12.68 9.5,12C9.5,11.32 9.56,10.65 9.66,10H14.34C14.43,10.65 14.5,11.32 14.5,12C14.5,12.68 14.43,13.34 14.34,14M12,19.96C11.17,18.76 10.5,17.43 10.09,16H13.91C13.5,17.43 12.83,18.76 12,19.96M8,8H5.08C6.03,6.34 7.57,5.06 9.4,4.44C8.8,5.55 8.35,6.75 8,8M5.08,16H8C8.35,17.25 8.8,18.45 9.4,19.56C7.57,18.93 6.03,17.65 5.08,16M4.26,14C4.1,13.36 4,12.69 4,12.C4,11.31 4.1,10.64 4.26,10H7.64C7.56,10.66 7.5,11.32 7.5,12C7.5,12.68 7.56,13.34 7.64,14M12,4.03C12.83,5.23 13.5,6.57 13.91,8H10.09C10.5,6.57 11.17,5.23 12,4.03M18.92,8H15.97C15.65,6.75 15.19,5.55 14.59,4.44C16.43,5.07 17.96,6.34 18.92,8M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z"
                    />
                </g>
            );
        case "delete":
            return (
                <g>
                    <path
                        fill={fill}
                        d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z"
                    />
                </g>
            );
        case "edit":
            return (
                <g>
                    <path
                        fill={fill}
                        d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,16.18L3,17.25Z"
                    />
                </g>
            );
        case "sort":
            return (
                <g>
                    <path fill={fill} d="M19, 10H5V8H19V10M19, 16H5V14H19V16Z" />
                </g>
            );
        case "quarter":
            return (
                <g>
                    <path fill={fill} d="M0, 0c13.3,0,24,10.7,24,24H0V0z" />
                </g>
            );
        case "rectanglePrimary":
            return (
                <g>
                    <react fill={fill} x="88" width="24" height="24" />
                </g>
            );
       
        case "polygonPrimary":
            return (
                <g>
                    <rect fill={fill} width="24" height="24" />
                </g>
            );
        case "polygonSecondary":
            return (
                <g>
                    <polygon fill={fill} points="24,24 0,24 0,0" />
                </g>
            );

        case "rectangleSecondary":
            return (
                <g>
                    <polygon fill={fill} points="0,24 0, 0 24,0" />
                </g>
            );
        case "circle":
            return (
                <g>
                    <circle fill={fill} cx="12" cy="12" r="12" />
                </g>
            );
        default:
            return 'null';
    }
};

const NewIcons = ({ glyph, fill, stroke, size = 24, onClick, children, ...other }) =>
{
    return (
        <SvgWrapper size={size} onClick={onClick} {...other}>
            {children ? (
                React.Children.map(children, child =>
                {
                    if (!React.isValidElement(child))
                    {
                        return null;
                    }

                    return React.cloneElement(child, {
                        glyph,
                        fill,
                        stroke,
                        size,
                    });
                })
            )
                :
                (
                    <InlineSvg>
                        <title id={glyph}>{glyph}</title>
                        <Glyph glyph={glyph} fill={fill} stroke={stroke} />
                    </InlineSvg>
                )
            }
        </SvgWrapper>
    );
};

export default NewIcons;