import React from 'react';
import styled from 'styled-components';
import Typography, { TypographyProps } from '@material-ui/core/Typography';

interface TypoProps extends TypographyProps
{
    component?: string;
    padding?: string;
    transform?: "uppercase" | "lowercase" | "capitalize";
    weight?: "bold" | "light" | "normal" | "italic";
    textColor?: "eggPlant" | "seal" | "glacireBlue" | "sand" | "darkBlue" | "black" | "white" | "darkPurple" | "lightGray" | "inherit " | "black" | "red";
}

const StyledTypography: React.FC<TypoProps> = styled(
    ({color, padding, textColor, transform, weight, children, ...rest}: TypoProps) => {
        if(textColor && color)
        {
            throw Error("Typograpghy component only takes either color or textColor in props.");
        }

        return (
            <Typography color={color} {...rest}>{children}</Typography>
        );
    }
)`
    &.MuiTypography-root {
        padding: ${props => props.padding};
        ${props => (props.weight === "bold" ? `font-weight : 700;` : null)}
        ${props => (props.weight === "light" ? `font-weight : 400;` : null)}
        ${props => (props.weight === "normal" ? `font-style : normal;` : null)}
        ${props => (props.weight === "italic" ? `font-style : italic;` : null)}
        ${props => props.transform && `text-transform: ${props.transform};` }
        ${props => props.textColor === "darkBlue" && `color: ${props.theme.palette.tertiary.darkBlue};` }
        ${props => props.textColor === "darkPurple" && `color: ${props.theme.palette.tertiary.darkPurple};` }
        ${props => props.textColor === "eggPlant" && `color: ${props.theme.palette.common.black};` }
        ${props => props.textColor === "eggPlant" && `color: ${props.theme.palette.primary.dark};` }
        ${props => props.textColor === "glacireBlue" && `color: ${props.theme.palette.tertiary.glacireBlue};` }
        ${props => props.textColor === "lightGray" && `color: ${props.theme.palette.tertiary.lightGray};` }
        ${props => props.textColor === "sand" && `color: ${props.theme.palette.tertiary.sand};` }
        ${props => props.textColor === "seal" && `color: ${props.theme.palette.tertiary.seal};` }
        ${props => props.textColor === "white" && `color: ${props.theme.palette.common.white};` }
        ${props => props.textColor === "black" && `color: ${props.theme.palette.common.black};` }
        ${props => props.textColor === "red" && `color: ${props.theme.palette.common.red};` }
    }
`;

export default StyledTypography;