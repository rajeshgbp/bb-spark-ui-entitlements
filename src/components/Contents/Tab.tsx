import React from 'react';
import { Grid } from '@material-ui/core';
import Typography from '../Typography/index';
import WarningIcon from '@material-ui/icons/Warning';
import WatchLaterIcon from '@material-ui/icons/WatchLater';
import PauseCircleFilledIcon from '@material-ui/icons/PauseCircleFilled';
import EmailIcon from '@material-ui/icons/Email';
import { makeCommonStyles } from 'styles/common';

interface Props{
    title?: any;
    count?: any;
}

const Kpi: React.FC<Props> = (props) => {
    // const { data } = props;
    const commonStyles = makeCommonStyles();
    const title = props.title;
    const count = props.count;
    return (
            <div>
                <Grid className={commonStyles.marginTopLeftRightBottom1}>
                    <Grid >
                        <Typography variant="body2" weight="bold"><small>{title}</small></Typography>
                    </Grid>
                    <Grid  className={commonStyles.marginTopXXS}>
                        <Typography variant="h5" component="h2">
                            <Grid container spacing={1}>
                                <Grid item lg={8} md= {8} sm= {8}>
                                    <b>{count} </b>
                                </Grid>
                                <Grid item lg={4} md= {4} sm= {4}>
                                    {title === "VACANT"
                                        ? (
                                            <div className={commonStyles.marginTopS3}>
                                            <WarningIcon aria-hidden="false" className={commonStyles.waringColor} />
                                            </div>
                                        ) : (<div></div>)}
                                    {title === "EXPIRING"
                                        ? (
                                            <div className={commonStyles.marginTopS3}>
                                                <WarningIcon aria-hidden="false" className={commonStyles.waringColor} />
                                            </div>
                                        ) : (<div></div>)}
                                    {title === "ISSUES"
                                        ? (
                                            <div className={commonStyles.marginTopS3}>
                                                <WarningIcon aria-hidden="false" className={commonStyles.waringColor} />
                                            </div>
                                        ) : (<div></div>)}
                                    {title === "EXPIRE LEASE"
                                        ? (
                                            <div className={commonStyles.marginTopS3}>
                                                <WatchLaterIcon aria-hidden="false"/>
                                            </div>
                                        ) : (<div></div>)}
                                    {title === "PAUSED COLLECTION"
                                        ? (
                                            <div className={commonStyles.marginTopS3}>
                                                <PauseCircleFilledIcon aria-hidden="false"/>
                                            </div>
                                        ) : (<div></div>)}
                                    {title === "NEW MESSAGES"
                                        ? (
                                            <div className={commonStyles.marginTopS3}>
                                                <EmailIcon aria-hidden="false"/>
                                            </div>
                                        ) : (<div></div>)}
                                    {title === "DELINQUENT"
                                        ? (
                                            <div className={commonStyles.marginTopS3}>
                                                <WarningIcon aria-hidden="false"/>
                                            </div>
                                        ) : (<div></div>)}
                                    {title === "1-30 DAYS LATE"
                                        ? (
                                            <div className={commonStyles.marginTopS3}>
                                                <WarningIcon aria-hidden="false"/>
                                            </div>
                                        ) : (<div></div>)}

                                </Grid>
                            </Grid>

                        </Typography>
                    </Grid>
                </Grid>
            </div>
    );
};



export default Kpi;
