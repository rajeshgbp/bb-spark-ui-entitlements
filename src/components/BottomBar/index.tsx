import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';

interface Props {
    children?:any,
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      top: 'auto',
      bottom: 0,
      padding:'1.5rem'
    },
  })
);

const BottomBar: React.FC<Props> = (props) => {
    const { children } = props;
  const classes = useStyles();
  return (
    <AppBar position="fixed" color="default" className={classes.appBar}>
      {children}
    </AppBar>
  );
};

export default BottomBar;
