import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import InfoIcon from '@material-ui/icons/Info';

interface Props{
  open ?: boolean;
  setOpen ?: any;
  handleSubmit ?: any;
  cancelButtonText ?: string;
  agreeButtonText ?: string;
  modalHeader ?: string;
  content ?: any;
}

export default function DeleteModal(props : Props) {
  const { open, setOpen, handleSubmit, cancelButtonText, agreeButtonText, modalHeader, content } = props;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const onClickAgree = () => {
    handleSubmit();
    handleClose();

  }

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title"><strong>{modalHeader}</strong></DialogTitle>
        <DialogContent style={{minWidth:'350px'}}>
            {content}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {cancelButtonText}
          </Button>
          <Button onClick={onClickAgree} variant='outlined' style={{backgroundColor:'#fc4517',color:'white'}}>
            {agreeButtonText}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}