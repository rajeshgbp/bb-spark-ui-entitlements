import { getDateString } from  '../common';

export const UniversalDateFormat = (value) =>
{
    var dateValue = new Date(getDateString(value));
        var dd = String(dateValue.getDate()).padStart(2, '0');
        var mm = String(dateValue.getMonth() + 1).padStart(2, '0');
        var yyyy = dateValue.getFullYear();

        let dateValueFormat = mm + '/' + dd + '/' +  yyyy ;

        return dateValueFormat;
};

export const changeDateFormat = (value) =>
{
    if(value)
    {
        let oldDateFormat  = value;
        let seperateDateAndTime = oldDateFormat.split('T');
        let oldDate = seperateDateAndTime[0].split('-');
        let newDateFormat  = oldDate[1] + '/' + oldDate[2] + '/' + oldDate [0];
        return newDateFormat;
    }
    else
    {
        return null;
    }    
}

