export const getDateString = (dt) =>
{
    if (dt && typeof dt == 'object')
    {
        dt = dt.toString();
    }
    else if (!dt)
    {
        dt = (new Date()).toString();
    }

    if (dt && (dt[dt.length - 1] == 'z' || dt[dt.length - 1] == 'Z'))
    {
        return dt.substring(0, dt.length - 1);
    }

    return dt;
}

export const getEstTimezone = () =>
{
    return new Date(new Date().toLocaleString('en-US', { timeZone: 'America/New_York' }));
}

export const getNoticePeriod = (value) =>
{
    if(value)
    {
        let noticeperiod = [15,30,60,90]
        if(noticeperiod.indexOf(Number(value)) === -1)
        {
            if((Number(value) < 15) || (Number(value) > 15 && Number(value) <= 20))
            {
                return 15;
            }
            else if((Number(value) > 20 && Number(value) <= 50)) 
            {
                return 30;
            }
            else if((Number(value) > 50 && Number(value) <= 70)) 
            {
                return 60;
            }
            else if(Number(value) > 70) 
            {
                return 90;
            }
        }
        else
        {
            return value;
        }
    }
    else
    {
        return 15;
    }
}