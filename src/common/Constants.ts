const UserType = {
    LANDLORD: 'RHL,',
    TENANT: 'RHT,',
    ADMIN: 'RHA,',
    TENANT_OBS: 'RHTOBS,',
    LANDLORD_OBS: 'RHLOBS,'
};

const NoticePeriods = {
    StartDay: 10,
    EndDay: 60,
    values: "15,30,60,90",
};

const ReturnDepositDays = {
    StartDay: 10,
    EndDay: 60
};

const InvoiceDeliveryDays = {
    StartDay: 15,
    EndDay: 20,
};

const DueByDays = {
    StartDay: 1,
    EndDay: 31,
};

const LateInvoiceDeliveryDays = {
    StartDay: 1,
    EndDay: 31,
};


const DIALOG_USERS_ACTIONS={
    deactivate:{
        content:'Revoke permissions temporarily until the user is activated.',
        agreeButtonText:'DEACTIVATE',
        cancelButtonText:'CANCEL'
    },
    remove:{
        content:'This action is permanant. Restoring permissions require a new invitation to join your team.',
        agreeButtonText:'REMOVE',
        cancelButtonText:'CANCEL'
    }
}

const EDIT_USER_ACTIONS={
    cancel:{
        content:'All unsaved changes will be lost.',
        agreeButtonText:'DISCARD',
        cancelButtonText:'GO BACK'
    },
    reset:{
        content:'All unsaved changes will be lost.',
        agreeButtonText:'DISCARD',
        cancelButtonText:'GO BACK'
    },
    deactivate:{
        content:'Revoke permissions temporarily until the user is activated.',
        agreeButtonText:'DEACTIVATE',
        cancelButtonText:'CANCEL'
    },
    remove:{
        content:'This action is permanant. Restoring permissions require a new invitation to join your team.',
        agreeButtonText:'REMOVE',
        cancelButtonText:'CANCEL'
    }
}

const EDIT_USER_KEYS={
    CANCEL:'cancel',
    RESET:'reset',
    DEACTIVATE:'deactivate',
    REMOVE:'remove'
}

export {
    UserType,
    NoticePeriods,
    ReturnDepositDays,
    InvoiceDeliveryDays,
    DueByDays,
    LateInvoiceDeliveryDays,
    DIALOG_USERS_ACTIONS,
    EDIT_USER_ACTIONS,
    EDIT_USER_KEYS
};