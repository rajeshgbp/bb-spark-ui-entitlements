import React from 'react';
import NumberFormat from 'react-number-format';

const AmountFormat = props => 
{
    let value = props.amount ? Number(props.amount) : 0;

    const shortForm = props.shortForm;

    let prefix = props.isNumber ? '' : '$';
    let suffix = '';
    let decimalScale = 2;
    let currencyDecimalScale = props.currencyDecimalScale ? 2 : props.bedsDecimalScale ? 1 : 0;
    let currencyFixedDecimalScale = props.currencyFixedDecimalScale ? true : true;
    let fixedDecimalScale = props.isFixedDecimal ? false : true;

    // if (shortForm)
    // {
    //     value = value / 1000000;
    //     suffix = 'M';
    // }
    
    if (shortForm && Math.abs(value) >= 1000000)
    {
        value = value / 1000;
        suffix = 'K';
    }

    return (
        <span>
            {shortForm  ?
            (
                <NumberFormat
                    prefix={prefix}
                    value={value}
                    suffix={suffix}
                    displayType={'text'}
                    thousandSeparator={true}
                    decimalScale={decimalScale}
                    fixedDecimalScale={fixedDecimalScale}
                />
            ):
            (
                    <NumberFormat
                        prefix={prefix}
                        value={value}
                        displayType={'text'}
                        thousandSeparator={true}
                        decimalScale={currencyDecimalScale}
                        fixedDecimalScale={currencyFixedDecimalScale}
                    />
            )}
        </span>
    );
};


export default AmountFormat;