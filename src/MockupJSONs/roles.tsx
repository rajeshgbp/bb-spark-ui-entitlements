export const roles = [
    {
        roleId: 1,
        roleName: 'custom role 1'
    },
    {
        roleId: 2,
        roleName: 'custom role 2'
    },
    {
        roleId: 3,
        roleName: 'custom role 3'
    },
]

export const accessLevels = [
    {
        label: 'View Only', value: 'viewOnly',
    },
    {
        label: 'View and Transact', value: 'viewAndTransact'
    }
];