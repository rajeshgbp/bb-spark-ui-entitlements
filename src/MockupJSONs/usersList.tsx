
    export const users = [
        {
            "id": 1, "isSelected": false, "firstName": "Rajesh", "lastName": "Gorijavolu",
            "email": "rajesh.gorijavolu@bluepal.com", "phoneNumber": "9876543210",
            "role": "Admin", "status": "Active", "lastSignedIn": "06/11/2021 9:00:00"
        },
        {
            "id": 2, "isSelected": false, "firstName": "Ravi", "lastName": "Reddy",
            "email": "ravi.gajareddy@bluepal.com", "phoneNumber": "9876543211",
            "role": "Delegate", "status": "Expired", "lastSignedIn": "07/11/2021 6:12:00"
        },
        {
            "id": 3, "isSelected": false, "firstName": "Abhishek", "lastName": "Kumar",
            "email": "abhishek.kumar@bluepal.com", "phoneNumber": "9876543212",
            "role": "Delegate", "status": "Deactivated", "lastSignedIn": "07/11/2021 8:20:00"
        }
    ]

    export const status=[
        {
            id:'Active',value:'Active'
        },
        {
            id:'Expired',value:'Invite expired'
        },
        {
            id:'Pending',value:'Pending'
        },
        {
            id:'Inactive',value:'Inactive'
        },
        {
            id:'Block',value:'Block'
        },
    ]

    export const role=[
        {
            id:'Admin',value:'Admin'
        },
        {
            id:'Delegate',value:'Delegate'
        }
    ]
    
    export const lastSignIn=[
       'Last 7 days','Last 30 days','Last 6 months','Last 12 months'
    ]