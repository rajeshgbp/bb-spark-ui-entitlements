export const usersActivityList = [
    {
      date: '5',
      month: 'NOV',
      year: '2020',
      firstName: 'Rajesh',
      lastName: 'Gorijavolu',
      email: 'rajesh.gorijavolu@bluepal.com',
      phoneNumber: '9876543210',
      role: 'Admin'
    },
    {
      date: '4',
      month: 'OCT',
      year: '2019',
      firstName: 'Ravi',
      lastName: 'Reddy',
      email: 'ravi.gajareddy@bluepal.com',
      phoneNumber: '9876543211',
      role: 'Delegate'
    },
    {
      date: '5',
      month: 'SEP',
      year: '2019',
      firstName: 'Abhishek',
      lastName: 'Kumar',
      email: 'abhishek.kumar@bluepal.com',
      phoneNumber: '9876543212',
      role: 'Delegate'
    }
  ];