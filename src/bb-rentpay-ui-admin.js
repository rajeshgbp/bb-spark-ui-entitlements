import React from 'react';
import ReactDOM from 'react-dom';
import singleSpaReact from 'single-spa-react';
import App from './App.tsx';
import *as serviceWorker from './serviceWorker';

function domElementGetter() 
{
	let el = document.getElementById("services-container");

	if (!el) 
	{
		el = document.createElement('div');
		el.id = 'services-container';
		// document.body.appendChild(el);
	}

	return el;
}

const reactLifecycles = singleSpaReact({
	React,
	ReactDOM,
	rootComponent: App,
	domElementGetter,
});

export const bootstrap = reactLifecycles.bootstrap;
export const mount = reactLifecycles.mount;
export const unmount = reactLifecycles.unmount;
export const unload = reactLifecycles.unload;

serviceWorker.register();