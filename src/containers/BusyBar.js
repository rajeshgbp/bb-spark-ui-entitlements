import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import Card from '@material-ui/core/Card';
import Loading from '../components/Loading/loading';

const BusyBar = (props) =>
{
    return (
        <Card open={true}>
            {/* <LinearProgress color="primary" /> */}

            <div style={{ position: "fixed", top: "50%", left: "50%", transform: "translate(-50%, -50%)", boxShadow:"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)", zIndex: 999}}>
                <Loading />
            </div>

        </Card>
    );
};

export default BusyBar;