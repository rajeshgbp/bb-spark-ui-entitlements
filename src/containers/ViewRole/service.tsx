import React, { useState } from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Header from '../SparkComponents/UserHeader';
import Typography from 'components/Typography';
import { Checkbox, Grid, Paper, Button } from '@material-ui/core';
import { makeCommonStyles, makeIconStyles, makeColorStyles, makeButtonStyles } from "../../styles";
import CreateIcon from '@material-ui/icons/Create';
import MailIcon from '@material-ui/icons/Mail';
import { NavLink } from 'react-router-dom';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import Entitlements from './Entitlements';
import {rolesData} from '../../utils/constants';
import UsersView from '../Users/UsersView';

interface Props{
    match ?: any;
    history ?: any;
}

const tabData = [
    {
        key: 'users',
        value: <Typography>ENTITLEMENTS</Typography>
    },
    {
        key: 'activity',
        value: <Typography>USERS</Typography>
    }
]

const ViewRole :React.FC<Props> = (props) => {

    const { match, history } = props;

    const commonStyles = makeCommonStyles();
    const buttonStyles = makeButtonStyles();
    const colorStyles = makeColorStyles();
    const iconStyles = makeIconStyles();

    const [value, setValue] = useState(0);
    const [roleData, setRoleData] = useState(rolesData)
    
    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    const redirectToRoles = () => {
        props.history.push('/entitlements/roles');
    }

    return (
        <div>
            <div>
                <Header
                    profilePicture=" "
                    headerTitle={
                    <div style={{marginBottom:'1.5rem'}}>   
                        <Typography variant="body1" style={{cursor:'pointer'}} onClick={redirectToRoles}> <ArrowBackIosIcon style={{fontSize:"16px"}} className={iconStyles.primary} /> ROLES</Typography>
                        <Typography variant="h4">{roleData.name}</Typography>
                        <Typography variant="body2">Access level : {roleData.accessLevel}</Typography>
                    </div>
                    }
                    headerData={
                        <Tabs
                            value={value}
                            indicatorColor="secondary"
                            style={{ position: 'absolute', marginTop: '-1rem' }}
                            onChange={handleChange}
                            aria-label="users activity tabs"
                        >
                            {
                                tabData.map((items) =>
                                    <Tab label={items.value} key={items.key} style={{ minWidth: '3rem' }} />
                                )
                            }
                        </Tabs>
                    }
                />
                <Grid container className={commonStyles.contentRoot} style={{minHeight:'0px', paddingBottom:'0px'}}>
                    <Grid item lg={12} xs={12} md={12} style={{textAlign:'end', marginTop:'-3rem'}}>
                        {
                            value ?
                            <Button variant="contained" color="secondary"><MailIcon/> Invite</Button>
                            :
                            <Button variant="contained" color="secondary"><CreateIcon/> Edit</Button>
                        }
                    </Grid>
                </Grid>
                <div className={commonStyles.contentRoot}>
                    {
                        value ?
                        <UsersView
                            match={match}
                            history={history}
                        />
                        :
                        <Entitlements
                            match={match}
                            history={history}
                            role={roleData}
                        />
                    }
                </div>
            </div>
        </div>
    )
}

export default ViewRole
