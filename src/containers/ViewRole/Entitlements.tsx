import { Grid } from '@material-ui/core';
import React from 'react';
import GeneralProperties from '../Roles/components/GeneralProperties';
import UserAndRoles from '../Roles/components/UserAndRoles';
import Property from '../Roles/components/Property';
import { makeCommonStyles, makeIconStyles, makeColorStyles, makeButtonStyles } from "../../styles";

interface Props{
    match ?: any;
    history ?: any;
    role ?: any;
}

const Entitlements: React.FC<Props> = (props) => {

    const { match, history, role } = props;

    const commonStyles = makeCommonStyles();
    const buttonStyles = makeButtonStyles();
    const colorStyles = makeColorStyles();
    const iconStyles = makeIconStyles();

    return (
        <div>
            <Grid container>
                <Grid item lg={12} xs={12}>
                    <GeneralProperties
                        addProperties={role.general.addProperties}
                        exportData={role.general.exportData}
                    />
                </Grid>
            </Grid>
            <Grid container className={commonStyles.marginTop3}>
                <Grid item lg={12} xs={12}>
                    <UserAndRoles
                        role={role}
                        match={match}
                        history={history}
                    />
                </Grid>
            </Grid>
            <Grid container className={commonStyles.marginTop2}>
                <Grid item lg={12} xs={12}>
                    <Property
                        data={role.properties}
                    />
                </Grid>
            </Grid>
        </div>
    )
}

export default Entitlements
