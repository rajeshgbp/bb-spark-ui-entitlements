import React from 'react';
import Typography from '../../../components/Typography';
import Grid from '@material-ui/core/Grid';
import { IconButton, Button, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import TablePagination from '@material-ui/core/TablePagination';
import WarningIcon from '@material-ui/icons/Warning';
import { Link, Paper } from '@material-ui/core';
import Moment from 'react-moment';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Avatar from '@material-ui/core/Avatar';
import DoneIcon from '@material-ui/icons/Done';
import { CheckCircle, MoreHoriz, RemoveCircle, Cancel, ArrowUpward, ArrowDownward } from '@material-ui/icons';
import { makeCommonStyles } from "../../../styles/common";
import PopupState, { bindTrigger, bindPopover } from 'material-ui-popup-state';
import Popover from '@material-ui/core/Popover';
import Actions from '../../../components/Table/actions';
import { DocumentService } from '../../../services';
import { SelectTextField } from 'components/TextField/textfield';


const useStyles = makeStyles((theme) => ({
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
  root: {
    '&::-webkit-scrollbar':
    {
      display: 'none',
      // flexWrap: 'nowrap',
    }
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },



  iconCellWidth: {
    width: '1%'
  },

  avatarCellWidth: {
    width: '2%'
  },

  avatar: {
    marginLeft: 2,
    width: 64,
    height: 64,
    backgroundColor: '#673ab7'
  },
  LinkColor: {

    color: '#766ea8'
  },
  rotate: {
    transform: 'rotate(90deg)'
  },
  fontStyle: {
    fontFamily: [
      'TN web use only',
    ].join(','),
  },
}));


interface Props {
  rows?: any;
  onDeleteunit?: any;
  history?: any;
  list?: any;
  setList?: any;
  onSelectAll?: any;
  onSelectItem?: any;
  actions?: any;
  source?: any;
  allCheckboxState?: Boolean;
  sortingDetails?:any;
  handleSorting?:any;
}

const UsersCardView: React.FC<Props> = props => {
  const { history, list, setList, onSelectAll, onSelectItem, onDeleteunit, actions, source, allCheckboxState,sortingDetails,handleSorting } = props;

  const classes = useStyles();
  const commonStyles = makeCommonStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [noInvoiceActionList, setNoInvoiceActionList] = React.useState([])

  React.useEffect(() => {
    window.scrollTo(0, 0);
    if (source === 'units') {
      let DuplicateActions = [...actions]
      DuplicateActions.splice(1, 1)
      setNoInvoiceActionList([...DuplicateActions])
    }

  }, []);


  const handleChangePage = (event, newPage) => {
    setPage(newPage);

  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const viewDetails = (e, id) => {
    e.preventDefault();
    history.push(`/rplandlord/unit/${id}`);
  };

  function actionMenuWithNoSendInvoiceOption(type, details) {
    if (type && details) {
      if (type === 'units') {
        if (details.lease) {
          if (details.lease.status === "Active" && (details.lease.leaseTenants.length > 0)) {
            return false;
          }
          else {
            return true;
          }
        }
        else {
          return true;
        }
      }
      else {
        return false;
      }
    }

  }

  function displayNumberOfDecimals(number, digits) {
    if (!number) {
      return '';
    }
    else {
      return number = Math.trunc(number * Math.pow(10, digits)) / Math.pow(10, digits)
    }
  };

  const getStatusIcon = (value) => {
    switch (value) {
      case 'Active': return <CheckCircle aria-hidden="false" fontSize="small" style={{ color: '#3D7AF4' }} />
      case 'Expired': return <Cancel aria-hidden="false" fontSize="small" style={{ color: 'gray' }} />
      case 'Pending': return <MoreHoriz aria-hidden="false" fontSize="small" style={{ color: 'gray' }} />
      case 'Suspended': return <RemoveCircle aria-hidden="false" fontSize="small" style={{ color: '#F4593D' }} />
    }
  }

  return (
    <Paper>
      <Grid container xs={12} >
        <Grid container item xs={12} justify='space-between' alignItems='center' style={{ backgroundColor: "#e6e5e9" }}>
          <Grid item xs={6} className={commonStyles.marginTopLeftRightBottom2}>
            <Button variant="outlined" style={{ backgroundColor: 'white' }} onClick={onSelectAll}>{allCheckboxState ? 'UnSelect all' : 'Select all'}</Button>
          </Grid>
          <Grid item container xs={3} alignItems='center'>
            <Grid item xs={9}>
              <SelectTextField
                label="Sort by"
                select
                id="sortBy"
                value={sortingDetails.columnId}
                className={commonStyles.autoWidth}
                variant="filled"
                onChange={(e)=>handleSorting(e.target.value)}
              >
                <MenuItem key='firstName' value='firstName'>
                  Name
                </MenuItem >
                <MenuItem key='role' value='role'>
                  Role
                </MenuItem >
                <MenuItem key='lastSignedIn' value='lastSignedIn'>
                  Last Sign-in
                </MenuItem >
              </SelectTextField>
            </Grid>
            <Grid xs={3}>
              <Button onClick={()=>handleSorting(sortingDetails.columnId)} disabled={!sortingDetails.columnId}>
                {
                  sortingDetails.orderBy === 'ASC' ? <ArrowUpward /> : <ArrowDownward />
                }
                </Button>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Paper square elevation={0} className={commonStyles.overFlowauto}>

            {list.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => {
                return (
                  <TableRow
                    style={row.isSelected ? { backgroundColor: "#e6e5e9", cursor: 'pointer' } : { backgroundColor: 'white', cursor: 'pointer' }}
                    hover
                    aria-checked={row.isSelected}
                    key={index}
                    selected={row.isSelected}
                  >
                    <TableCell align="left" className={commonStyles.tablewidth39} >
                      <Grid container spacing={2}>
                        <Grid item xs={2} data-testid={'selectunit' + index}>
                          <Avatar className={classes.avatar}  >
                            {row.isSelected === false ? (<img alt={"unit picture" + "  " + row.name + " " + index} src={row.picture ? (DocumentService.GetDocument(row.picture)) : "/app/assets/rp/images/property_default_image.jpg"} width="100%" height="100%" />) : (<DoneIcon className={commonStyles.DoneIconColor} />)}
                          </Avatar>
                        </Grid>
                        <Grid item xs={10}>
                          <div>
                            <Grid container spacing={2} wrap="nowrap" className={commonStyles.tenantNameWidth}>
                              <Grid item zeroMinWidth>
                                <Typography variant="h5" component="h5" textColor="darkBlue" className={commonStyles.pointer} noWrap
                                  onClick={() => onSelectItem(row.id)}>
                                  {row.firstName} {row.lastName}</Typography>
                                <Typography color="textSecondary" noWrap><small>{row.email}</small></Typography>
                                <Typography color="textSecondary" noWrap><small>{row.phoneNumber}</small></Typography>

                                <Grid container alignItems='center' justify='flex-start'>
                                  <Typography color="textSecondary" noWrap>
                                    {getStatusIcon(row.status)}
                                  </Typography>
                                  &nbsp;
                                  <Typography color="textSecondary" noWrap>
                                    {row.status}
                                  </Typography>
                                </Grid>
                              </Grid>
                            </Grid>
                          </div>
                        </Grid>
                      </Grid>
                    </TableCell>
                    <TableCell align="left" >
                      <Grid container spacing={2} wrap="nowrap" className={commonStyles.tenantNameWidth}>
                        <Grid item zeroMinWidth>
                          <Typography variant="h5" component="h5" textColor="darkBlue" className={commonStyles.pointer} noWrap>
                            {row.role}</Typography>
                          <Typography color="textSecondary" noWrap><small>Role</small></Typography>
                        </Grid>
                      </Grid>
                    </TableCell>
                    <TableCell className={commonStyles.tablewidth26}>
                      <Grid container spacing={2} wrap="nowrap" className={commonStyles.tenantNameWidth}>
                        <Grid item zeroMinWidth>
                          <Typography variant="h5" component="h5" textColor="darkBlue" className={commonStyles.pointer} noWrap>
                            {row.lastSignedIn}</Typography>
                          <Typography color="textSecondary" noWrap><small>Last Sign-in</small></Typography>
                        </Grid>
                      </Grid>
                    </TableCell>
                    <TableCell align="left" className={commonStyles.occupiedTableCellWidth}>
                      <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'row', gap: "7px" }}  >
                        <div>
                          {row.status === "Late" || row.status === "Vacant" ?
                            (
                              <div>
                                <WarningIcon aria-hidden="false" className={commonStyles.waringColor} />
                              </div>)
                            : (<div></div>)
                          }
                        </div>
                        <div>
                          <Typography noWrap>{row.status}</Typography>
                        </div>
                      </div>
                    </TableCell>

                    <TableCell align="center" className={classes.iconCellWidth + " " + commonStyles.paddingrightXlg}>
                      <Typography>

                        <PopupState variant="popover" popupId="demo-popup-popover">
                          {(popupState) => (
                            <div>
                              <IconButton aria-label={"actions for " + row.name} data-testid={"actions" + index} {...bindTrigger(popupState)} >
                                <MoreVertIcon aria-hidden="false" />
                              </IconButton>
                              <Popover
                                {...bindPopover(popupState)}
                                anchorOrigin={{
                                  vertical: 'bottom',
                                  horizontal: 'left',
                                }}
                                transformOrigin={{
                                  vertical: 'top',
                                  horizontal: 'right',
                                }}
                              >
                                {actionMenuWithNoSendInvoiceOption(source, row) ?
                                  (
                                    <Actions actions={noInvoiceActionList} row={row} closePopUp={popupState.close} />
                                  ) :
                                  (
                                    <Actions actions={actions} row={row} closePopUp={popupState.close} />
                                  )
                                }
                              </Popover>
                            </div>
                          )}
                        </PopupState>
                      </Typography>
                    </TableCell>

                  </TableRow>

                );
              })}

            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              count={list.length}
              rowsPerPage={rowsPerPage}
              page={page}
              labelRowsPerPage={"Results per page:"}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              labelDisplayedRows={({ from, to, count }) => `${from} - ${to} of ${count}`}
            />
          </Paper>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default (UsersCardView);