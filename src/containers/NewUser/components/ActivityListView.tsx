import React from 'react';
import Typography from '../../../components/Typography';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import TablePagination from '@material-ui/core/TablePagination';
import WarningIcon from '@material-ui/icons/Warning';
import { Link, Paper, TableBody, Divider } from '@material-ui/core';
import Moment from 'react-moment';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Avatar from '@material-ui/core/Avatar';
import DoneIcon from '@material-ui/icons/Done';
import {
  CheckCircle,
  Cancel,
  MoreHoriz,
  RemoveCircle,
  Edit,
  Delete
} from '@material-ui/icons';
import { makeCommonStyles } from '../../../styles/common';
import PopupState, { bindTrigger, bindPopover } from 'material-ui-popup-state';
import Popover from '@material-ui/core/Popover';
import Actions from '../../../components/Table/actions';
import { DocumentService } from '../../../services';

const useStyles = makeStyles((theme) => ({
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)'
  },
  root: {
    '&::-webkit-scrollbar': {
      display: 'none'
      // flexWrap: 'nowrap',
    }
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2)
  },
  table: {
    minWidth: 750
  },

  iconCellWidth: {
    width: '1%'
  },

  avatarCellWidth: {
    width: '2%'
  },

  avatar: {
    marginLeft: 2,
    width: 64,
    height: 64,
    backgroundColor: '#673ab7'
  },
  LinkColor: {
    color: '#766ea8'
  },
  rotate: {
    transform: 'rotate(90deg)'
  },
  fontStyle: {
    fontFamily: ['TN web use only'].join(',')
  }
}));

interface Props {
  rows?: any;
  onDeleteunit?: any;
  history?: any;
  list?: any;
  setList?: any;
  onSelectAll?: any;
  onSelectItem?: any;
  actions?: any;
  source?: any;
  years?: any;
}

const ActivityListView: React.FC<Props> = (props) => {
  const {
    history,
    list,
    setList,
    onSelectAll,
    onSelectItem,
    onDeleteunit,
    actions,
    source,
    years
  } = props;

  const classes = useStyles();
  const commonStyles = makeCommonStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [noInvoiceActionList, setNoInvoiceActionList] = React.useState([]);

  React.useEffect(() => {
    window.scrollTo(0, 0);
    if (source === 'units') {
      let DuplicateActions = [...actions];
      DuplicateActions.splice(1, 1);
      setNoInvoiceActionList([...DuplicateActions]);
    }
  }, []);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const viewDetails = (e, id) => {
    e.preventDefault();
    history.push(`/rplandlord/unit/${id}`);
  };

  function actionMenuWithNoSendInvoiceOption(type, details) {
    if (type && details) {
      if (type === 'units') {
        if (details.lease) {
          if (
            details.lease.status === 'Active' &&
            details.lease.leaseTenants.length > 0
          ) {
            return false;
          } else {
            return true;
          }
        } else {
          return true;
        }
      } else {
        return false;
      }
    }
  }

  function displayNumberOfDecimals(number, digits) {
    if (!number) {
      return '';
    } else {
      return (number =
        Math.trunc(number * Math.pow(10, digits)) / Math.pow(10, digits));
    }
  }

  const getStatusIcon = (value) => {
    switch (value) {
      case 'Active':
        return <CheckCircle aria-hidden="false" fontSize="small" />;
      case 'Expired':
        return <CheckCircle aria-hidden="false" fontSize="small" />;
      case 'Pending':
        return <MoreHoriz aria-hidden="false" fontSize="small" />;
      case 'Suspended':
        return <RemoveCircle aria-hidden="false" fontSize="small" />;
    }
  };

  return (
    <Paper>
      <Grid container lg={12} md={12} sm={12} xs={12} spacing={0}>
        <Grid item lg={12} md={12} sm={12} xs={12}></Grid>

        <Grid item lg={12} md={12} sm={12} xs={12}>
          <Paper square elevation={0} className={commonStyles.overFlowauto}>
            <TableBody>
            {
                years.map((items)=>
                <TableRow>
                  <TableCell className={commonStyles.tablewidth15 } style={{padding:'0'}}>
                    <Grid container>
                      <Grid item lg={1} md={1} xs={12} style={{padding:'2rem', borderRight:'1px solid #bdbdbd'}}>
                        <Typography variant="subtitle1">
                          {items}
                        </Typography>
                        <Divider orientation="vertical" flexItem />
                      </Grid>
                      <Grid item lg={11} md={11} xs={12}>
                        {list
                          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                          .map((row, index) => {
                            return (
                              <>
                                {row.year==items ? (
                                  <TableRow
                                    style={
                                      row.isSelected
                                        ? { backgroundColor: '#e6e5e9' }
                                        : { backgroundColor: 'white' }
                                    }
                                    hover
                                    aria-checked={row.isSelected}
                                    key={index}
                                    selected={row.isSelected}>
                                    <TableCell
                                      align="left"
                                      className={commonStyles.tablewidth15 }
                                      >
                                      <Grid container spacing={4}>
                                        <Grid item lg={1} md={1} xs={1} sm={1} style={{textAlign:'center'}}>
                                          <Typography variant="subtitle2">{row.month}</Typography>
                                          <Typography variant="subtitle2">{row.date}</Typography>
                                        </Grid>
                                        <Grid
                                          item
                                          lg={1}
                                          md={1}
                                          sm={1}
                                          xs={1}
                                          data-testid={'selectunit' + index}>
                                          <Avatar
                                            className={classes.avatar}
                                            style={{ backgroundColor: '#bdbdbd' }}>
                                            <img
                                              alt={
                                                'unit picture' +
                                                '  ' +
                                                row.name +
                                                ' ' +
                                                index
                                              }
                                              src={
                                                row.picture
                                                  ? DocumentService.GetDocument(
                                                      row.picture
                                                    )
                                                  : '/app/assets/rp/images/property_default_image.jpg'
                                              }
                                              width="100%"
                                              height="100%"
                                            />
                                            )
                                          </Avatar>
                                        </Grid>
                                        <Grid item lg={10} md={10} sm={10} xs={10}>
                                          <div>
                                            <Grid
                                              container
                                              spacing={2}
                                              wrap="nowrap"
                                              className={commonStyles.tenantNameWidth}>
                                              <Grid item lg={12}>
                                                <Typography
                                                  variant="subtitle1"
                                                  component="h5"
                                                  textColor="darkBlue"
                                                  className={commonStyles.pointer}>
                                                  {row.firstName} {row.lastName}{' '}
                                                  {row.email}
                                                </Typography>
                                                <Typography color="textSecondary">
                                                  <small>[{row.role}]</small>
                                                </Typography>
                                              </Grid>
                                            </Grid>
                                          </div>
                                        </Grid>
                                      </Grid>
                                    </TableCell>
                                  </TableRow>
                                ) : (
                                  ''
                                )}
                              </>
                            );
                          })}
                      </Grid>
                    </Grid>
                  </TableCell>
                </TableRow>
                )
              }
            </TableBody>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              count={list.length}
              rowsPerPage={rowsPerPage}
              page={page}
              labelRowsPerPage={'Results per page:'}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true
              }}
              labelDisplayedRows={({ from, to, count }) =>
                `${from} - ${to} of ${count}`
              }
            />
          </Paper>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default ActivityListView;
