import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeCommonStyles, makeColorStyles, makeButtonStyles, makeIconStyles } from 'styles';
import { makeStyles, withStyles, Theme, createStyles } from '@material-ui/core/styles';
import CheckCircleSharpIcon from '@material-ui/icons/CheckCircleSharp';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { Box, Grid, Typography, Button, Avatar, FormControlLabel, Checkbox, FormControl } from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import GeneralProperties from 'containers/Roles/components/GeneralProperties';
import UserAndRoles from 'containers/Roles/components/UserAndRoles';
import Property from 'containers/Roles/components/Property';
import BottomBar from 'components/BottomBar';
import { rolesData } from 'utils/constants';
import CustomTypography from 'components/Typography';
import { Edit, RemoveCircle, Cancel } from '@material-ui/icons'
import Switch from '@material-ui/core/Switch';
import DeleteModal from 'components/Modal/DeleteModal';
import { EDIT_USER_ACTIONS, EDIT_USER_KEYS } from 'common/Constants'
import userService from '../../services/user-service';
import ReplayIcon from '@material-ui/icons/Replay';

const AntTabs = withStyles({

    indicator: {
        backgroundColor: '#fd8500',
        height: 4,
    },
})(Tabs);

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`tenantDetails-${index}`}
            aria-labelledby={`tenantDetails-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={0}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

const AntTab = withStyles((theme) => ({
    root: {
        textTransform: 'none',

        fontWeight: theme.typography.fontWeightRegular,
        minWidth: 83,
        marginRight: theme.spacing(3),

        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),

        '&:hover': {
            color: 'white',
            opacity: 1,
        },
        '&$selected': {
            color: 'white',
            fontWeight: theme.typography.fontWeightMedium,
        },
        '&:focus': {
            color: 'white',
        },
    },
    selected: {},
}))((props: StyledTabProps) => <Tab {...props} />);

interface StyledTabProps {
    label: any;
}

interface Props {
    history?: any,
    match?: any
}

const ViewUserDetails: React.FC<Props> = (props) => {

    const { history, match } = props;
    const commonStyles = makeCommonStyles();
    const buttonStyles = makeButtonStyles();
    const colorStyles = makeColorStyles();
    const iconStyles = makeIconStyles();

    const [isLoading, setIsLoading] = useState(false);
    const [value, setValue] = useState(0);
    const [isEdit, setIsEdit] = useState(false);
    const [isSuspend, setIsSuspend] = useState(false);
    const [isRemove, setIsRemove] = useState(false);
    const [userData, setUserData] = useState<any>(null);
    const [modalDetails, setModalDetails] = useState({ state: false, actionType: '', title: '' });

    useEffect(() => {
        if (window.location.href.includes('edit')) {
            setIsEdit(true);
        }
    }, [])

    useEffect(() => {
        setUserData(rolesData);
    }, [])

    const gotoUsers = () => {
        history.push('/entitlements');
    }

    const handleChange = (event, newValue) => {
        setValue(newValue);
    }

    const onCancelClick = () => {
        setModalDetails({ ...modalDetails, state: true, actionType: EDIT_USER_KEYS.CANCEL, title: 'Discard changes?' });
    }

    const onResetClick = () => {
        setModalDetails({ ...modalDetails, state: true, actionType: EDIT_USER_KEYS.RESET, title: 'Discard changes?' });
    }

    const onSave = () => {
        if (isRemove) {
            setModalDetails({ ...modalDetails, state: true, actionType: EDIT_USER_KEYS.REMOVE, title: `Remove ${userData.userInfo.firstName + ' ' + userData.userInfo.lastName}?` });
        }
        else if (isSuspend) {
            setModalDetails({ ...modalDetails, state: true, actionType: EDIT_USER_KEYS.DEACTIVATE, title: `Deactivate ${userData.userInfo.firstName + ' ' + userData.userInfo.lastName}?` });
        }
    }

    const closeModal = () => {
        setModalDetails({ ...modalDetails, state: false })
    }
    useEffect(() => {
        setIsLoading(true);
        userService.getTeamMemberById((userRolesAndInfoData: any) => {
            setUserData(userRolesAndInfoData);
            setIsLoading(false);
        }, match.params.id)
    }, [])

    return (
        <div>{
                !isLoading &&
                <div className={commonStyles.headerColor + " " + commonStyles.tabspadding0}>
                    <Grid container className={commonStyles.marginTopLeftRightBottom2} justify="space-around">
                        <Grid item lg={12} className={commonStyles.paddingleftrightXlg} style={{ paddingLeft: '1.5rem', paddingBottom: '1rem' }}>
                            <Button data-testid="goToTeamManagement" onClick={() => gotoUsers()} aria-label={"Back to Team management"} className={colorStyles.white} startIcon={<ArrowBackIosIcon style={{ fontSize: '15px' }} className={iconStyles.primary} />}>
                                <div className={commonStyles.marginTopXXS}>
                                    <Typography variant="body2">TEAM MANAGEMENT</Typography>
                                </div>
                            </Button>
                        </Grid>
                        <Grid item lg={12} container>
                            <Grid item lg={1} className={commonStyles.centerAlign} justify="center">
                                <Avatar style={{ backgroundColor: '#5B5397', height: '60px', width: '60px' }} alt={userData && userData.userInfo.firstName + 'profile picture'} src={userData && userData.userInfo.image} />
                            </Grid>
                            <Grid item lg={10}>
                                <Typography variant="h4">{userData && userData.userInfo.firstName + ' ' + userData.userInfo.lastName}</Typography>
                                <Typography variant="body2">{userData && userData.userInfo.email} &nbsp;{userData && userData.userInfo.phoneNumber}</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container spacing={4}  >
                        <Grid item  >
                        </Grid>
                        <Grid item lg={9} >
                            <AntTabs value={value} onChange={handleChange} aria-label="Tenant information" >
                                <AntTab label="ACCESS" data-testid="ACCESS" />
                                {/* <AntTab label="ACTIVITY" data-testid="ACTIVITY" /> */}
                            </AntTabs>
                        </Grid>
                    </Grid>
                    {
                        !isEdit && (
                            <Grid container justify="flex-end" style={{ height: '0', position: 'relative' }}>
                                <Grid item lg={12} style={{ position: 'absolute', marginTop: '-1.5rem', paddingRight: '2rem' }} className={commonStyles.textAlignRight}>
                                    <Button variant='contained' className={buttonStyles.PropertieslistviewFabButton} onClick={() => setIsEdit(true)}>
                                        <Grid container spacing={2} direction='row' wrap="nowrap" alignItems="center" justify="center">
                                            <Grid item lg={2} sm={2} md={2} xs={2} zeroMinWidth className={commonStyles.marginTopXXS}>
                                                <Edit aria-hidden="false" />
                                            </Grid>
                                            <Grid item lg={10} sm={10} md={10} xs={10} zeroMinWidth>
                                                <CustomTypography display='inline' variant="body2" weight="bold">
                                                    Edit
                                                </CustomTypography>
                                            </Grid>
                                        </Grid>
                                    </Button>
                                </Grid>
                            </Grid>
                        )
                    }
                </div>
            }
            {
                value === 0 ?
                    <div className={commonStyles.contentRoot}>
                        {
                            userData &&
                            <Grid container spacing={1}>
                                {
                                    isEdit ? (
                                        <Grid item xs={12} container justify='space-between' alignItems='center'>
                                            <Grid item >
                                                <Grid container alignItems='center' spacing={2}>
                                                    <Grid item>
                                                        <Typography variant="h6"><strong>Roles and permissions</strong></Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Button role="button" color='primary' aria-label="Reset" aria-roledescription="Reset Button" disabled={!(isRemove || isSuspend)} onClick={() => {
                                                            setIsRemove(false)
                                                            setIsSuspend(false)
                                                            onResetClick()
                                                        }}>
                                                            <ReplayIcon /> &nbsp;
                                                            Reset Permissions</Button>
                                                    </Grid>
                                                </Grid>
                                                {
                                                    isRemove ? (
                                                        <Typography variant="body2" style={{ marginTop: '0.5rem' }} className={commonStyles.centerAlign}>
                                                            <Cancel aria-hidden="false" fontSize="small" style={{ color: '#F4593D' }} />
                                                            &nbsp;Removed
                                                        </Typography>
                                                    )
                                                        : (
                                                            <>
                                                                {
                                                                    isSuspend ? (
                                                                        <Typography variant="body2" style={{ marginTop: '0.5rem' }} className={commonStyles.centerAlign}>
                                                                            <RemoveCircle aria-hidden="false" fontSize="small" style={{ color: '#F4593D' }} />
                                                                            &nbsp;Inactive
                                                                        </Typography>

                                                                    ) : (
                                                                        <Typography variant="body2" style={{ textTransform: 'capitalize', marginTop: '0.5rem' }} className={commonStyles.centerAlign}>
                                                                            {
                                                                                userData && userData.userInfo.status === 'Active' ?
                                                                                    <span><CheckCircleSharpIcon style={{ fill: 'dodgerblue', marginRight: '0.5rem' }} /></span> :
                                                                                    null
                                                                            }
                                                                            {userData && userData.userInfo.status}
                                                                        </Typography>
                                                                    )
                                                                }
                                                            </>
                                                        )
                                                }
                                            </Grid>
                                            {
                                                !isRemove && (
                                                    <Grid item justify='flex-start' alignItems='center'>
                                                        <Grid container justify='flex-start' alignItems='center'>
                                                            <Grid item>
                                                                {/* <Typography color='primary'>Suspend</Typography> */}
                                                            </Grid>
                                                            <Grid item>
                                                                <FormControl>
                                                                    <FormControlLabel
                                                                        label="Deactivate"
                                                                        labelPlacement="start"
                                                                        control={<Switch checked={isSuspend} onChange={() => setIsSuspend(!isSuspend)} color='primary' />}
                                                                        aria-label="Suspend"
                                                                        data-testid={"suspend-user"}
                                                                    />
                                                                </FormControl>
                                                            </Grid>
                                                            <Grid item>
                                                                <Button aria-label="Remove" variant='outlined' style={{ backgroundColor: '#fc4517', marginLeft: '1rem', color: 'white' }} onClick={() => setIsRemove(true)}>Remove</Button>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                )
                                            }
                                        </Grid>
                                    ) : (
                                        <Grid item lg={12}>
                                            <Typography variant="h6"><strong>{userData && userData.userInfo.role}</strong></Typography>
                                            <Typography variant="body2" style={{ textTransform: 'capitalize', marginTop: '0.5rem' }} className={commonStyles.centerAlign}>
                                                {
                                                    userData && userData.userInfo.status === 'Active' ?
                                                        <span><CheckCircleSharpIcon style={{ fill: 'dodgerblue', marginRight: '0.5rem' }} /></span> :
                                                        null
                                                }
                                                {userData && userData.userInfo.status}
                                            </Typography>
                                        </Grid>
                                    )
                                }
                                {
                                    !isRemove && (
                                        <>
                                            <Grid item lg={12} >
                                                <GeneralProperties
                                                    addProperties={userData.general.addProperties}
                                                    exportData={userData.general.exportData}
                                                    isNonEditable={!isEdit}
                                                />
                                            </Grid>
                                            <Grid item lg={12} className={commonStyles.marginTop3}>
                                                <UserAndRoles
                                                    role={userData}
                                                    isNonEditable={!isEdit}
                                                />
                                            </Grid>
                                            <Grid item lg={12}>
                                                <Property
                                                    data={userData && userData.properties}
                                                    isNonEditable={!isEdit}
                                                />
                                            </Grid>
                                        </>
                                    )
                                }
                            </Grid>
                        }
                    {  
                    isEdit &&
                        <BottomBar>
                            <Grid container justify="flex-end" className={commonStyles.textAlignRight}>
                                <Grid item lg={2} md={12} xs={12} className={commonStyles.textAlignRight}>
                                    <Button color="primary">Cancel</Button>
                                    <Button color="secondary" variant="contained" style={{marginLeft:'1rem', paddingRight:'1.5rem', paddingLeft:'1.5rem'}}>Save</Button>
                                </Grid>
                            </Grid>
                        </BottomBar>
                        }
                    </div>
                    :
                    <div className={commonStyles.contentRoot}>
                        This is User activity component.
                    </div>
            }
            {
                modalDetails.state &&
                <DeleteModal
                    open={modalDetails.state}
                    setOpen={closeModal}
                    handleSubmit={() => { }}
                    modalHeader={modalDetails.title}
                    agreeButtonText={EDIT_USER_ACTIONS[modalDetails.actionType].agreeButtonText}
                    cancelButtonText={EDIT_USER_ACTIONS[modalDetails.actionType].cancelButtonText}
                    content={
                        <Typography variant="subtitle1">{EDIT_USER_ACTIONS[modalDetails.actionType].content}</Typography>
                    }
                />
            }
        </div>
    )
}

export default ViewUserDetails
