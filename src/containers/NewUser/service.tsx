import React, { useState } from 'react';
import { Checkbox, Grid, Paper, Button } from '@material-ui/core'
import Typography from 'components/Typography';
import { makeCommonStyles, makeIconStyles, makeColorStyles, makeButtonStyles } from "../../styles";
import UsersCardView from './components/UsersCardView';
import DynamicTable from '../../components/Table/DynamicTable';
import FilterView from './components/filter';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { Email } from '@material-ui/icons';

interface Props {
    history: any;
    match?: any;
}

const NewUser: React.FC<Props> = (props) => {
    const { history, match } = props;

    const commonStyles = makeCommonStyles();
    const buttonStyles = makeButtonStyles();

    return (
        <div >

        </div>
    );

};

export default NewUser;
