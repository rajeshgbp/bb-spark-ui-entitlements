import React, { useState, useEffect } from 'react';
import { EmailField, SelectTextField, TenantNameField, DollarRedditTextFieldForProcessing } from 'components/TextField/textfield';
import { Avatar, Divider, Grid, Paper, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { makeButtonStyles } from "styles/buttons";
import { makeCommonStyles } from "styles/common";
import { makeColorStyles } from "styles/colors";
import MenuItem from '@material-ui/core/MenuItem';
import { ValidatorForm } from 'react-material-ui-form-validator';
import Typography from 'components/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { FormControl, FormGroup } from '@material-ui/core';

interface Props{
    match?:any;
    history?:any;
    role?:any;
    isNonEditable?:boolean;
}

const accessLevels = [
    {
        label:'View Only',value:'viewOnly',
    },
    {
        label:'View and Transact',value:'viewAndTransact'
    }
];

const userAndRolesData = [
    {
        key : 'add-edit-user',
        value : 'Add/Edit users',
        checked : true,
        permissions : ['admin', 'view-only'],
        currentPermission : 'view-only',
        editable : true
    },
    {
        key : 'delete-existing-users',
        value : 'Delete existing users',
        checked : true,
        permissions : ['admin', 'view-only'],
        currentPermission : 'view-only',
        editable : true
    },
    {
        key : 'add-edit-custom-roles',
        value : 'Add/Edit custom roles',
        checked : true,
        permissions : ['admin', 'view-only'],
        currentPermission : 'view-only',
        editable : true
    },
    {
        key : 'delete-existing-custom-roles',
        value : 'Delete existing custom roles',
        checked : true,
        permissions : ['admin', 'view-only'],
        currentPermission : 'view-only',
        editable : true
    },
]

const UserAndRoles:React.FC<Props> = (props) => {

    const { role } = props;

    const commonStyles = makeCommonStyles();
    const colorStyles = makeColorStyles();
    const buttonStyles = makeButtonStyles();

    const [userAndRolesProperties, setuserAndRolesProperties] = useState<Array<object>|any>([]);

    
    useEffect(() => {
        if(props.isNonEditable){
            setuserAndRolesProperties([...userAndRolesData.map((item)=>{
                item.editable = false;
                return item
            })])
        }else{
            setuserAndRolesProperties(userAndRolesData);
        }
    }, [props])

    const handleClickCheckbox = (key:string, e:any) => {
        const findIndex = userAndRolesProperties.findIndex(item=>item.key===key);
        if(findIndex>-1){
            userAndRolesProperties[findIndex].checked = e.target.checked;
            setuserAndRolesProperties([...userAndRolesProperties]);
        }
    }

    return (
        <Paper >
        <Grid container justify="space-between" alignItems='center' style={{ padding: '1rem 2rem' }}>
            <Grid item xs={6}>
                <Typography variant="h5">User and roles</Typography>
            </Grid>
            <Grid item xs={6} md={4} lg={3}>
                <SelectTextField
                    label="Access level"
                    aria-label="Access level select box"
                    select
                    id="selectPayService"
                    className={commonStyles.autoWidth}
                    variant="filled"
                    value={role.usersAndRoles.accessLevel}
                >
                    {accessLevels.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                            {option.label}
                        </MenuItem >
                    ))}
                </SelectTextField>
            </Grid>
        </Grid>
        <Divider />
        <Grid item container style={{ paddingLeft: '2rem', paddingBottom: '1rem' }}>
            {
                userAndRolesProperties.map(items=>                    
                    <Grid item lg={12} >
                        <FormControl>
                            <FormGroup>
                                <FormControlLabel control={
                                <Checkbox 
                                disabled={!items.editable}
                                checked={items.checked} 
                                onClick={(e)=>handleClickCheckbox(items.key, e)}
                                />}
                                label={items.value}
                                aria-label={items.value}
                                data-testid={items.key}
                                />
                            </FormGroup>
                        </FormControl>
                    </Grid >
                    )
            }
            {/* <Grid item lg={12} >
                <FormControl>
                    <FormGroup>
                        <FormControlLabel control={<Checkbox 
                        disabled={props.isNonEditable}
                        checked={true} 
                        // checked={role.usersAndRoles.deleteUsers} 
                        />}
                            label="Delete existing users"
                            aria-label="Delete existing users"
                            data-testid={"delete-existing-users"}
                        />
                    </FormGroup>
                </FormControl>

            </Grid >
            <Grid item lg={12} >
                <FormControl>
                    <FormGroup>
                        <FormControlLabel control={<Checkbox 
                        disabled={props.isNonEditable}
                        checked={true} 
                        // checked={role.usersAndRoles.addEditRoles} 
                        />}
                            label="Add/edit custom roles"
                            aria-label="Add/edit custom roles"
                            data-testid={"add-edit-custom-roles"}
                        />
                    </FormGroup>
                </FormControl>

            </Grid >
            <Grid item lg={12} >
                <FormControl>
                    <FormGroup>
                        <FormControlLabel control={<Checkbox 
                        disabled={props.isNonEditable}
                        checked={true} 
                        // checked={role.usersAndRoles.deleteRoles} 
                        />}
                            label="Delete existing custom roles"
                            aria-label="Delete existing custom roles"
                            data-testid={"delete-existing-custom-roles"}
                        />
                    </FormGroup>
                </FormControl>

            </Grid > */}

        </Grid>
    </Paper>
    )
}

export default UserAndRoles
