import React, { useEffect, useState } from 'react';
import { EmailField, SelectTextField, TenantNameField, DollarRedditTextFieldForProcessing } from 'components/TextField/textfield';
import { Avatar, Divider, Grid, Paper, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { makeButtonStyles } from "styles/buttons";
import { makeCommonStyles } from "styles/common";
import { makeColorStyles } from "styles/colors";
import Typography from 'components/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { FormControl, FormGroup } from '@material-ui/core';

interface Props{
    match?:any;
    history?:any;
    addProperties ?: any;
    exportData ?: any;
    isNonEditable?:boolean;
}

const generalEntitlements = [
    {
        key : 'add-properties',
        value : 'Add Properties',
        checked : true,
        permissions : ['admin', 'view-only'],
        currentPermission : 'view-only',
        editable : true
    },
    {
        key : 'exports-data-and-reports',
        value : 'Export data and reports',
        checked : true,
        permissions : ['admin', 'view-only'],
        currentPermission : 'view-only',
        editable : true
    },
]

const GeneralProperties: React.FC<Props> = (props) => {

    const commonStyles = makeCommonStyles();
    const colorStyles = makeColorStyles();
    const buttonStyles = makeButtonStyles();

    const [generalProperties, setgeneralProperties] = useState<Array<object>|any>([]);

    useEffect(() => {
        if(props.isNonEditable){
            setgeneralProperties([...generalEntitlements.map((item)=>{
                item.editable = false;
                return item
            })])
        }else{
            setgeneralProperties(generalEntitlements);
        }
    }, [props])

    const handleClickCheckbox = (key:string, e:any) => {
        const findIndex = generalProperties.findIndex(item=>item.key===key);
        if(findIndex>-1){
            generalProperties[findIndex].checked = e.target.checked;
            setgeneralProperties([...generalProperties]);
        }
    }

    return (
        <Paper style={{ padding: '2rem' }}>
        <Typography variant="h5" style={{ marginBottom: '1rem' }}>General</Typography>

        <Grid container spacing={3} >
            {
                generalProperties.map((items)=>                
                <Grid item xs={12} style={{ padding: '0px 12px' }}>
                    <FormControl>
                        <FormGroup>
                            <FormControlLabel control={
                                <Checkbox 
                                disabled={!items.editable}
                                checked={items.checked} 
                                onClick={(e)=>handleClickCheckbox(items.key, e)}
                                />}
                                label={items.value}
                                aria-label={items.value}
                                data-testid={items.key}
                            />
                        </FormGroup>
                    </FormControl>
                </Grid >
                )
            }
            {/* <Grid item xs={12} style={{ padding: '0px 12px' }}>
                <FormControl>
                    <FormGroup>
                        <FormControlLabel control={<Checkbox 
                        checked={props.exportData} 
                        disabled={props.isNonEditable}
                        />}
                            label="Export data and reports"
                            aria-label="Export data and reports"
                            data-testid={"exports-data-and-reports"}
                        />
                    </FormGroup>
                </FormControl>
            </Grid> */}
        </Grid>
    </Paper>
    )
}

export default GeneralProperties
