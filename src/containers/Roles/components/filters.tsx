import React, { useState } from 'react';
import { makeCommonStyles, makeIconStyles, makeColorStyles, makeButtonStyles } from '../../../styles';
import { Grid, Select, MenuItem, FormControl, InputLabel } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { SelectTextField } from 'components/TextField/textfield';

interface Props
{
    filterValues?:any;
    setFilterValues?:any;
}

const array = Array.from(new Array(100))
const userNumber = array.map((item, index)=>({
    key : 1*index,
    value : 1*index,
}));

const accessLevel =[
    {
        key :'',
        value : 'All',
        text : 'All',
    },
    {
        key :'view_only',
        value : 'view only',
        text : 'view only',
    },
    {
        key : 'view_and_transact',
        value : 'view and transact',
        text : 'view and transact',
    }
]

const RolesFilters: React.FC<Props> = (props) => {

    const { filterValues, setFilterValues } = props;
    // const [filterValues, setFilterValues] = useState({
    //     noOfUsers:'All',
    //     accessLevel :'',
    // })

    const commonStyles = makeCommonStyles();
    const buttonStyles = makeButtonStyles();
    const colorStyles = makeColorStyles();
    const iconStyles = makeIconStyles();

    const handleChangeFilterValues = (key, value) => {
        setFilterValues({
            ...filterValues,
            [key]:value
        })
    }

    return (
        <div>
            <Grid container justify="space-between" >
                <Grid lg={6}>
                    <Grid container className={commonStyles.marginTopLeftRightBottom2}>
                        <Grid item lg={1} className={commonStyles.centerAlign}> <SearchIcon fontSize="large"/> </Grid>
                        <Grid item lg={5} className={commonStyles.marginRight2}>
                            <SelectTextField
                                label="Access level"
                                select
                                aria-label="Access level select box"
                                id="selectPayService"
                                className={commonStyles.autoWidth}
                                variant="filled"
                                value={filterValues.accessLevel}
                                onChange={(e)=>handleChangeFilterValues('accessLevel', e.target.value)}
                            >
                                {accessLevel.map((option) => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.text}
                                    </MenuItem >
                                ))}
                            </SelectTextField>
                        </Grid>
                        <Grid item lg={5}>
                            
                            <SelectTextField
                                label="No. of users"
                                select
                                aria-label="Number of users select box"
                                id="selectPayService"
                                className={commonStyles.autoWidth}
                                variant="filled"
                                value={filterValues.noOfUsers}
                                onChange={(e)=>handleChangeFilterValues('noOfUsers', e.target.value)}
                            >
                                <MenuItem key={'All'} value={'All'}>
                                    All
                                </MenuItem >
                                {userNumber.map((option) => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.value}
                                    </MenuItem >
                                ))}
                            </SelectTextField>
                            
                        </Grid>
                    </Grid> 
                </Grid>
                <Grid item lg={3}></Grid>
                <Grid lg={2} className={commonStyles.rightAlign} style={{marginRight:'2rem'}}>
                <FormControl>
                    <InputLabel htmlFor="default-native-simple">Default</InputLabel>
                    <Select
                    native
                    aria-label="Default select field"
                    inputProps={{
                        name: 'default',
                        id: 'default-native-simple',
                    }}
                    >
                    <option aria-label="None" value="" />
                    <option value={10}>Ten</option>
                    <option value={20}>Twenty</option>
                    <option value={30}>Thirty</option>
                    </Select>
                </FormControl>
                </Grid>
            </Grid>
        </div>
    )
}

export default (RolesFilters)
