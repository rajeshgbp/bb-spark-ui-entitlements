import React, { useState, useEffect } from 'react';
import { EmailField, SelectTextField, TenantNameField, DollarRedditTextFieldForProcessing } from 'components/TextField/textfield';
import { Avatar, Divider, Grid, Paper, Button, Typography, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { makeButtonStyles } from "styles/buttons";
import { makeCommonStyles } from "styles/common";
import { makeColorStyles } from "styles/colors";
import MenuItem from '@material-ui/core/MenuItem';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { FormControl, FormGroup } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { element } from 'prop-types';

interface Props{
    data?:any;
    isNonEditable?:boolean;
}

const options = [
    {
        key :'view_only',
        value : 'view only',
        text : 'view only',
    },
    {
        key : 'view_and_transact',
        value : 'view and transact',
        text : 'view and transact',
    }
];

interface selectedEntitlementsObject{
    key ?: string;
    actions ?: any;
}

const Property : React.FC<Props> = (props) => {

    const { data } = props;

    const commonStyles = makeCommonStyles();
    const colorStyles = makeColorStyles();
    const buttonStyles = makeButtonStyles(); 

    const [propertyData, setPropertyData] = useState([])

    const [collapse, setCollapse] = useState(false);
    const [selectedEntitlement, setSelectedEntitlement] = useState<selectedEntitlementsObject|undefined>({
        key:'General',
        actions:[
            {
                key :'Edit property details',
                isSelected:false
            },
            { 
                key :'Delete/remove property from profile',
                isSelected:false
            },
        ]
    });

    useEffect(() => {
        setPropertyData(
            data.properties && data.properties.length ?
            data.properties.map((items)=>{
                items.collapse = false;
                items.entitlements = items.entitlements.map((element, index) => {
                    if(index==0){
                        element.isSelected = true;
                    }
                    return element   
                });
                return items
            }):[]
            );
    }, [data])

    const collapseAllTab = () => {
        setPropertyData(
            propertyData.length ?
            propertyData.map((items)=>{
                items.collapse = !items.collapse;
                return items
            }):[]
        )
    }

    const collapseSingleTab = (id) => {
        const index = propertyData.findIndex(items=>items.id == id);
        if(index>-1){
            propertyData[index].collapse = !propertyData[index].collapse;
            setPropertyData([...propertyData]);
        }
    }

    const selectEntitlement = (id, item) => {

        setPropertyData([...propertyData.map((property)=>{
            if(property.id===id){
                const { entitlements } = property;
                property.entitlements = entitlements.map((entitles)=>{
                    if(entitles.key===item.key){
                        entitles.isSelected = true;
                    }else{
                        entitles.isSelected = false;
                    }
                    return entitles
                })
            }
            return property
        })]);

    }

    const selectEntitlementProperties = (propertyId, entitlement, selectedKey) => {

        setPropertyData([...propertyData.map((property)=>{
            if(property.id===propertyId){
                const { entitlements } = property;
                property.entitlements = entitlements.map((entitles)=>{
                    if(entitles.key===entitlement.key){
                        const { actions } = entitles;
                        entitles.actions = actions.map((action)=>{
                            if(action.key===selectedKey){
                                action.isSelected=!action.isSelected;
                            }
                            return action
                        })
                    }
                    return entitles
                })
            }
            return property
        })]);

    }


    return (
        <>
            <Grid className={commonStyles.marginTop4}>
                <Grid item lg={12}>
                    <Grid container className={commonStyles.marginLeftXLg} style={{ marginLeft: '0'}} justify="space-between">
                        <Grid item lg={11} md={10} xs={12}>
                            <Typography variant="h5" style={{ marginBottom: '0rem' }}>{data.title}</Typography>
                        </Grid>
                        <Grid item lg={1} md={2} xs={12} style={{ textAlign: 'right' }}>
                            <Typography variant="subtitle1" className={colorStyles.purple} style={{ marginBottom: '0rem', cursor:'pointer' }} onClick={collapseAllTab} aria-lebel="collapse all"><a>COLLAPSE ALL</a></Typography>
                        </Grid>
                    </Grid>
                    { 
                        propertyData.length ? propertyData.map((property)=>                        
                        <Paper className={commonStyles.marginTop3}>
                            <Grid container justify="space-between" style={{padding:'1rem'}}>
                                <Grid item lg={6} xs={12} className={commonStyles.centerAlign}>
                                    <div className={commonStyles.centerAlign}>
                                        <Avatar style={{ backgroundColor: '#5B5397', height: '60px', width: '60px' }} alt="property image" src={data && data.image}/>
                                        <Typography variant="subtitle1" style={{ marginLeft: '1rem' }}>{property.name}</Typography>
                                    </div>
                                </Grid>
                                <Grid item lg={3}></Grid> 
                                <Grid item lg={3} xs={12}  className={commonStyles.centerAlign} justify="center">
                                    <SelectTextField
                                        label="Access level"
                                        select
                                        aria-label="Access level select box"
                                        id="selectPayService"
                                        className={commonStyles.autoWidth}
                                        variant="filled"
                                    >
                                        {options.map((option) => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.value}
                                            </MenuItem >
                                        ))}
                                    </SelectTextField>
                                    <IconButton onClick={()=>collapseSingleTab(property.id)} role="button" aria-label={!property.collapse ?"close property":"open property"}> 
                                        {
                                            !property.collapse ?
                                            <ExpandLessIcon fontSize="large"/>
                                            :
                                            <ExpandMoreIcon fontSize="large"/>
                                        }
                                    </IconButton>
                                </Grid>
                            </Grid>
                            {
                                !property.collapse &&
                                <>
                                    <Divider />
                                    <Grid container justify="space-between">
                                        <Grid item lg={3} style={{ borderRight: '1px solid #b3b3b3' }}>
                                            <List className={commonStyles.noPadding}>
                                                {
                                                    property.entitlements.length ? property.entitlements.map((entitlement, index)=>  {
                                                        return (
                                                            <>                                          
                                                            <ListItem button style={{padding:'1rem'}} onClick={()=>{selectEntitlement(property.id, entitlement)}}>
                                                                <Grid container>
                                                                    <Grid item lg={6} md={12} xs={12}>
                                                                        <Typography variant="subtitle2">{entitlement.key}</Typography>
                                                                    </Grid>
                                                                    <Grid item lg={6} md={12} xs={12}>
                                                                        <Typography>{entitlement.selected} of {entitlement.total} entitlements</Typography>
                                                                    </Grid>
                                                                </Grid>
                                                            </ListItem> 
                                                            <Divider/>
                                                            </>
                                                        )
                                                    }
                                                    ):''
                                                }
                                            </List>
                                        </Grid>
                                        {
                                            property.entitlements.length ? property.entitlements.map((entitlement, index)=>  {
                                            if(entitlement.isSelected){
                                                return (
                                                <Grid item lg={9}>
                                                    <Grid container>
                                                        <Grid item lg={12}>
                                                            <Grid item lg={12} container justify="space-between" style={{ padding: '1rem 2rem' }}>
                                                                <Grid item lg={2}>
                                                                    <Typography variant="h5" style={{ margin: '0.5rem 0' }}>{entitlement.key}</Typography>
                                                                </Grid>
                                                                <Grid item lg={2}>
                                                                    <SelectTextField
                                                                        label="Access level"
                                                                        select
                                                                        aria-label="Access level select box"
                                                                        id="selectPayService"
                                                                        className={commonStyles.autoWidth+ " " + commonStyles.height3}
                                                                        variant="filled"
                                                                    >
                                                                        {options.map((option) => (
                                                                            <MenuItem key={option.value} value={option.value}>
                                                                                {option.value}
                                                                            </MenuItem >
                                                                        ))}
                                                                    </SelectTextField>
                                                                </Grid>
                                                            </Grid>
                                                            <Divider style={{marginBottom:'1.5rem'}}/>
                                                            <Grid item lg={12} style={{ padding: '1rem 2rem' }}>
                                                                <Grid container spacing={3} >
                                                                    {
                                                                        entitlement.actions.map((elements)=>
                                                                        <Grid item lg={12} style={{ padding: '0px 12px' }}>
                                                                            <div>
                                                                                <FormControl>
                                                                                    <FormGroup>
                                                                                        <FormControlLabel control={<Checkbox 
                                                                                        checked={elements.isSelected}
                                                                                        onClick={()=>selectEntitlementProperties(property.id, entitlement, elements.key)}
                                                                                        />}
                                                                                            label={elements.key}
                                                                                            aria-label={elements.key}
                                                                                            data-testid={elements.key}
                                                                                        />
                                                                                    </FormGroup>
                                                                                </FormControl>
                                                                            </div>
                                                                        </Grid >
                                                                        ) 
                                                                    }
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                )
                                            }else{
                                                return null
                                            }
                                            }):""
                                        }
                                    </Grid>
                                </>
                            }
                        </Paper>
                        ):''
                    }
                </Grid>
            </Grid> 
        </>
    )
}

export default Property
