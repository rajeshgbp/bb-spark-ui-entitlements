import React, { useState, useEffect } from 'react';
import RolesFilters from './components/filters';
import Grid from '@material-ui/core/Grid';
import Typography from 'components/Typography';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types'; 
import { makeStyles } from '@material-ui/core/styles';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import { makeButtonStyles } from "styles/buttons";
import { makeCommonStyles } from "styles/common";
import Paper from '@material-ui/core/Paper';
import { makeColorStyles } from "styles/colors";
import AddIcon from '@material-ui/icons/Add';
import { makeIconStyles } from "styles/icons";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { Divider } from '@material-ui/core';
import SparkList from '../Users/components/UsersCardView';
import DynamicTable from 'components/Table/DynamicTable';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles((theme) => ({
    box: {
        backgroundColor: '#f7f6f5', boxShadow: "none",
    },
    inviteLandlordFab: {
        position: 'absolute',
        top: "184px", 
        right: "56px", 
        backgroundColor: '#ed8b00', 
        color: '#000000', 
        borderRadius: "26px", 
        paddingLeft: "20px",
        paddingRight: "20px", 
        height: "48px", 
        width: "175px", 
    },
    table: {
        minWidth: 650,
      },
}));

interface Props 
{
    setRoleFlag?:any;
    actions?:any;
    rolesList?:any;
    history?:any;
    match?:any;
    columns?:any;
    rows?:any;
    showDeleteOption?:boolean;
    handleDeleteRoles?:any;
    selectedItems?:number;
}

const Roles: React.FC<Props> = (props) => 
{
    const { setRoleFlag, history, match, columns, rows, actions, showDeleteOption, handleDeleteRoles, selectedItems } = props;

    const commonStyles = makeCommonStyles();
    const buttonStyles = makeButtonStyles();
    const colorStyles = makeColorStyles();
    const iconStyles = makeIconStyles();

    const classes = useStyles();

    const [list, setList] = React.useState([]);
    const [rolesListData, setRolesListData] = useState(rows);
    const [filterValues, setFilterValues] = useState({
        noOfUsers:'All',
        accessLevel :'All',
    });

    const addInfoIcon = (rows) => {
        const newRolesData = JSON.parse(JSON.stringify(rows))
        setRolesListData(newRolesData.map((items:any)=>{
            items.role = <Typography variant="subtitle1" style={{textTransform:'capitalize', cursor:'pointer'}}>{items.role}</Typography>;
            items['access-level'] = <>{items['access-level']} <InfoOutlinedIcon className={commonStyles.marginLeft1}/></>;
            return items;
        })); 
    }

    const filterWithNoOfUsersAndAccessLevel = () => {
        let filteredData = rows;
        if(filterValues.noOfUsers!='All' && filterValues.accessLevel!='All'){
            filteredData = rows.filter((items)=>{
                if(items['no-of-users']==filterValues.noOfUsers && items['access-level']==filterValues.accessLevel) return items;
            });
            addInfoIcon(filteredData);
        }
        else if(filterValues.noOfUsers!='All' && filterValues.accessLevel=='All'){
            filteredData = rows.filter((items)=>{
                if(items['no-of-users']==filterValues.noOfUsers) return items;
            });
            addInfoIcon(filteredData);
        }else if(filterValues.noOfUsers=='All' && filterValues.accessLevel!='All'){
            filteredData = rows.filter((items)=>{
                if(items['access-level']==filterValues.accessLevel) return items;
            });
            addInfoIcon(filteredData);
        }
        else{
            addInfoIcon(rows);
        }
    }


    useEffect(() => {
        if(rows.length){
            filterWithNoOfUsersAndAccessLevel();
        }
    }, [filterValues, rows])

    const compRef = React.useRef<HTMLDivElement>(null);
    React.useEffect(() => 
    {
        if (compRef.current) 
        {
            // compRef.current.scrollIntoView();
        }
    }, []);

    return (
        
        <div ref={compRef}>
            <div className={commonStyles.headerColor}>
                <Grid container justify="space-between" className={commonStyles.marginTopBottom2}>
                    <Grid item lg={10} sm={10} md={10} xs={10} className={commonStyles.marginLeftXLg}>
                        <Button className={colorStyles.white} startIcon={ <ArrowBackIosIcon style={{fontSize:"16px"}} className={iconStyles.primary} />} aria-label={"Back to dashboard"} data-testid="dashboard">
                                    <div className={commonStyles.marginTopXXS}>
                                        <Typography variant="body2" textColor="white">DASHBOARD</Typography>
                                        </div>
                            </Button>
                    </Grid>
                </Grid>
                <Grid className={commonStyles.marginTopBottom2}>
                    <div className={commonStyles.marginLeftXLg}>
                        <Typography variant="h5" component="h1" weight="light"> Roles </Typography>
                    </div>
                </Grid>
            </div>
            
            <Grid container className={commonStyles.contentRoot}>
                <Grid item lg={12} md={12} sm={12} xs={12} >
                    <div >
                        <Typography align='right' className={commonStyles.fabButtonMargin}>
                            <Button variant='contained' onClick={()=>setRoleFlag(true)} className={buttonStyles.PropertieslistviewFabButton} data-testid={'invitelandlorddialog'}>
                                <Typography
                                    display='inline'
                                    className={commonStyles.marginTopXXXS}>
                                    <AddIcon aria-hidden="false" />
                                        &nbsp;&nbsp;
                                </Typography>
                                <Typography display='inline' variant="body2" weight="bold">
                                    New Role
                                </Typography>
                            </Button>
                        </Typography>
                    </div>                   
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12} className={commonStyles.marginTop4}>                    
                    <div>
                        <Paper>
                            {
                                showDeleteOption ?
                                <Grid container justify="space-between" alignItems="center" style={{padding:'1.5rem'}}>
                                    <Grid item lg={6} md={6} xs={12}>
                                        <Typography>{selectedItems} Selected</Typography>
                                    </Grid>
                                    <Grid item lg={6} md={6} xs={12} style={{textAlign:'right'}}>
                                        <Button variant="text" onClick={handleDeleteRoles}><DeleteIcon/> Delete</Button>
                                    </Grid>
                                </Grid>
                                :
                                <RolesFilters
                                filterValues={filterValues}
                                setFilterValues={setFilterValues}
                                />
                            }
                            <Divider/>
                            <Grid>
                                <DynamicTable columns={columns} rows={rolesListData} actions={actions} source={'unitsList'} handleSorting={()=>{}}/>
                            </Grid>
                        </Paper>
                        
                    </div>
                    
                </Grid>
            </Grid>
       
        </div>
    );
};

export default Roles;