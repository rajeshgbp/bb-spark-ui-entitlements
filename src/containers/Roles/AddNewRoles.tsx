import React, { useEffect, useState } from 'react';
import { EmailField, SelectTextField, TenantNameField } from 'components/TextField/textfield';
import { Avatar, Divider, Grid, Paper, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { makeButtonStyles } from "styles/buttons";
import { makeCommonStyles } from "styles/common";
import { makeColorStyles } from "styles/colors";
import MenuItem from '@material-ui/core/MenuItem';
import { ValidatorForm } from 'react-material-ui-form-validator';
import Typography from 'components/Typography';
import Property from './components/Property';
import { Scanner } from '@material-ui/icons';
import GeneralProperties from './components/GeneralProperties';
import UserAndRoles from './components/UserAndRoles';
import { RolesService } from '../../services';

interface Props {
    fromInviteUser?: Boolean,
    role?: any,
}

const AddNewRoles = (props: Props) => {

    const commonStyles = makeCommonStyles();
    const colorStyles = makeColorStyles();
    const buttonStyles = makeButtonStyles();
    const [accessLevels, setAccessLevels] = useState([])
    const [roles, setRoles] = useState([])
    const [selectedRole, setSelectedRole] = useState('');

    useEffect(() => {
        RolesService.GetRoles((roles) => {
            setRoles(roles)
        })
        RolesService.GetAccessLevels((data) => {
            setAccessLevels(data);
        })
    }, [])

    const { fromInviteUser, role } = props;

    const onSelectedRoleChanged = (e) => {
        setSelectedRole(e.target.value)
    }

    const isRoleNotSelected = !(fromInviteUser && !selectedRole);

    return (
        <div>
            {
                !fromInviteUser && (
                    <div className={commonStyles.headerColor}>
                        <Grid container justify="space-between" className={commonStyles.marginTopBottom2}>
                            <Grid item lg={10} sm={10} md={10} xs={10} className={commonStyles.marginLeftXLg}>

                            </Grid>
                        </Grid>
                        <Grid className={commonStyles.marginTopBottom2}>
                            <div className={commonStyles.marginLeftXLg}>
                                <Typography variant="h5" component="h1" weight="light">New Role </Typography>
                            </div>
                        </Grid>
                    </div>
                )
            }
            <div className={fromInviteUser ? '' : commonStyles.contentRoot}>
                <ValidatorForm onSubmit={() => { }} >
                    <Grid container justify="space-between" spacing={4}>
                        <Grid item container spacing={2} xs={8}>
                            {
                                fromInviteUser && (
                                    <Grid item xs={4}>
                                        <SelectTextField
                                            label="Role"
                                            select
                                            value={selectedRole}
                                            onChange={onSelectedRoleChanged}
                                            id="role"
                                            className={commonStyles.autoWidth}
                                            variant="filled"
                                        >
                                            {roles.map((option) => (
                                                <MenuItem key={option.roleId} value={option.roleId}>
                                                    {option.roleName}
                                                </MenuItem >
                                            ))}
                                            <MenuItem key='newRole' value='newRole'>
                                                New Role
                                            </MenuItem >
                                        </SelectTextField>
                                    </Grid>
                                )
                            }
                            {
                                isRoleNotSelected && (
                                    <>
                                        {
                                            (!fromInviteUser || selectedRole === 'newRole') && (
                                                <Grid item xs={fromInviteUser ? 4 : 6}>
                                                    <TenantNameField
                                                        label="Name"
                                                        className={commonStyles.autoWidth}
                                                        variant="filled"
                                                        id="roleName"
                                                    />
                                                </Grid>
                                            )
                                        }
                                        <Grid item xs={fromInviteUser ? 4 : 6}>
                                            <SelectTextField
                                                label="Access level"
                                                select
                                                // aria-label="Select"
                                                id="selectPayService"
                                                className={commonStyles.autoWidth}
                                                variant="filled"
                                            >
                                                {accessLevels.map((option) => (
                                                    <MenuItem key={option.value} value={option.value}>
                                                        {option.label}
                                                    </MenuItem >
                                                ))}
                                            </SelectTextField>
                                        </Grid>
                                    </>
                                )
                            }
                        </Grid>
                        {
                            isRoleNotSelected && (
                                <Grid item container alignItems='center' justify='flex-end' xs={4}>
                                    <Typography className={colorStyles.linkColor}>
                                        <Scanner /> USE ROLE AS A TEMPLATE
                                    </Typography>
                                </Grid>
                            )
                        }
                    </Grid>
                </ValidatorForm>
                <br />
                {
                    isRoleNotSelected && (
                        <GeneralProperties
                            addProperties={role.general.addProperties}
                            exportData={role.general.exportData}
                        />
                    )
                }
                <br />
                {
                    isRoleNotSelected && (
                        <UserAndRoles
                            role={role}
                        />
                    )
                }
                {
                    isRoleNotSelected && (
                        <Property
                            data={role.properties}
                        />
                    )
                }
                {
                    !props.fromInviteUser &&
                    <Grid container justify="flex-end" className={commonStyles.marginTop4}>
                        <Grid item className={commonStyles.marginRight2}>
                            <Button data-testid={"cancel"} className={buttonStyles.tertiary}> Cancel </Button>
                        </Grid>
                        <Grid item className={commonStyles.marginRight2}>
                            <Button data-testid={"submit"} type='submit' value={"submit"} color="primary" className={buttonStyles.primary}> Save </Button>
                        </Grid>
                    </Grid>
                }
            </div>
        </div>
    )
}

export default AddNewRoles
