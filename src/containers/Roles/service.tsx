import React, { useState, useEffect } from 'react';
import { API, UserService, PaymentService } from '../../services';
import { makeCommonStyles, makeIconStyles, makeColorStyles, makeButtonStyles } from "../../styles";
import { CheckCircle, Cancel, MoreHoriz, RemoveCircle, Edit, Delete } from '@material-ui/icons';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import { Checkbox, Grid, Typography, Paper } from '@material-ui/core';
import DeleteModal from 'components/Modal/DeleteModal';
import RolesList from './rolesList';
import AddNewRoles from './AddNewRoles';
import { NavLink } from 'react-router-dom';
import { rolesData as customRoleData } from 'utils/constants';

const DELTE_MODAL_HEADER = 'Reassign or remove the following users to continue deleting role.'

interface Props
{
    history: any;
    match?: any;
}

const rolesData = [
    {
        code            : 1,
        isSelected      : false,
        role            : 'custom role 1',
        'access-level'  : 'view and transact',
        'no-of-users'   : '4',
        'action'        : 'none'
    },
    {
        code            : 2,
        isSelected      : false,
        role            : 'custom role 2',
        'access-level'  : 'view and transact',
        'no-of-users'   : '2',
        'action'        : 'none'
    },
    {
        code            : 3,
        isSelected      : false,
        role            : 'custom role 3',
        'access-level'  : 'view only',
        'no-of-users'   : '0',
        'action'        : 'none'
    },
    {
        code            : 4,
        isSelected      : false,
        role            : 'Delegate',
        'access-level'  : 'view and transact',
        'no-of-users'   : '4',
        'action'        : 'none'
    }
]

const RolesService: React.FC<Props> = (props) => 
{
    const { history, match } = props;

    const commonStyles = makeCommonStyles();
    const buttonStyles = makeButtonStyles();
    const colorStyles = makeColorStyles();
    const iconStyles = makeIconStyles();

    const [rolesList, setRolesList] = React.useState([]);
    const [isApiResponse, setIsApiResponse] = React.useState(false);
    const [roleFlag, setRoleFlag] = useState(false);
    const [showDeleteOption, setShowDeleteOption] = useState(false);
    const [selectedItems, setSelectedItems] = useState([]);
    const [openDeleteModal, setOpenDeleteModal] = useState(false);
    const [role,setRole] = useState({});

    function getLandlords()
    {
        UserService.GetLandlords(function (result: any) 
        {
            if (result.status === 1) 
            {
                let data = result.data.landlords;

                data.forEach(landlord =>
                {
                    landlord.isOpenServiceFee = false;
                    landlord.wepayServiceFeeUpdated = landlord.wepayServiceFee;
                });

                setIsApiResponse(true);
            }
            else 
            {
                API.showError(result.message);
            }
        });
    };


    const actions = [

        {
            text: 'Edit',
            icon: <Edit aria-hidden="false" className={iconStyles.darkgrey} />,
            isDivider: false,
        },
        {
            text: 'Delete',
            icon: < Delete aria-hidden="false" className={iconStyles.darkgrey} />,
            isDivider: false,
        }

    ];

    const handleClickSelectAllCheckBox = (e) => {
        setRolesList([
            ...rolesList.map((items)=>{
                items.isSelected=e.target.checked?true:false;
                return items;
            })
        ])
    }

    const checkIfAnyRolesIsSelected = () => {
        const anySelectedRole = rolesList.find(items=>items.isSelected);
        if(anySelectedRole){
            setShowDeleteOption(true);
        }else{
            setShowDeleteOption(false);
        }
        setSelectedItems((rolesList.filter(items=>items.isSelected)));
    }

    useEffect(() => {
        checkIfAnyRolesIsSelected();
    }, [rolesList])

    const handleClickSingleCheckbox = (e, code) => {
        const index = rolesList.findIndex((items)=>items.code===code);
        if(index>-1){
            rolesList[index].isSelected = !rolesList[index].isSelected;
            setRolesList([...rolesList]);
        }
    }

    const handleDeleteRoles = () => {  //Delete Api integration need to be added here
        const newRolesList = rolesList;
        newRolesList.forEach((items, index)=>{
            if(items.isSelected){
                newRolesList.splice(index,1)
            }
        })
        setRolesList([...newRolesList]); 
    }

    const redirectToViewRolePage = (code) => {
        props.history.push(`/entitlements/role/${code}`)
    }

    React.useEffect(() =>
    {
        setRolesList(rolesData); 
        setRole(customRoleData)
    }, []);

    const columns = [
        { 
            id: 'isSelected',
            label: <Checkbox onClick={handleClickSelectAllCheckBox}/> ,
            type: 'checkbox',
            ignoreSorting:true,
            isLink: true,
            callbackArguments: ['code','isSelected'],
            customColumns: ['code', 'isSelected'],
            customCell: (code,isSelected) => <Checkbox  onClick={(e)=>handleClickSingleCheckbox(e,code)} checked={isSelected}/>,
        },
        {
            id: 'role',
            label: 'Role',
            type: 'text',
        },
        {
            id: 'access-level',
            label: 'Access Level',
          
        },
        {
            id: 'no-of-users',
            label: 'No. of users',
            align: 'center',
            alignText:'center'
        },
    ];

    return (
            <div>
                {
                    openDeleteModal &&
                    <DeleteModal
                        open = {openDeleteModal}
                        setOpen = {setOpenDeleteModal}
                        handleSubmit = {handleDeleteRoles}
                        modalHeader={DELTE_MODAL_HEADER}
                        agreeButtonText="View Users"
                        cancelButtonText="Cancel"
                        content={
                            <ul>
                                {
                                    selectedItems.map((items)=>
                                    <li><Typography variant="subtitle1">{items.role}</Typography></li>
                                    )
                                }
                            </ul>
                        } 
                    />
                }
                {
                    roleFlag ?
                    <AddNewRoles fromInviteUser={false} role={role}/>
                    :
                    <RolesList 
                        setRoleFlag={setRoleFlag}
                        actions={actions}
                        rolesList={rolesList}
                        history={history}
                        match={match}
                        columns={columns}
                        rows={rolesList}
                        showDeleteOption={showDeleteOption}
                        handleDeleteRoles={()=>setOpenDeleteModal(true)}
                        selectedItems={selectedItems.length}
                    />
                }
            </div>
    );
       
};

export default RolesService;
