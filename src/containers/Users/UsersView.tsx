import React, { useState, useEffect } from 'react';
import { Checkbox, Grid, Paper, Button, Link } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import Typography from 'components/Typography';
import { makeCommonStyles, makeIconStyles, makeColorStyles, makeButtonStyles } from "../../styles";
import UsersCardView from './components/UsersCardView';
import DynamicTable from '../../components/Table/DynamicTable';
import FilterView from './components/filter';
import Info from '@material-ui/icons/InfoOutlined'
import { UserService } from '../../services';
import DeleteModal from 'components/Modal/DeleteModal';
import { DIALOG_USERS_ACTIONS, EDIT_USER_KEYS } from 'common/Constants'

import { CheckCircle, Cancel, MoreHoriz, RemoveCircle, Edit, Delete, Email } from '@material-ui/icons';

interface Props {
    history: any;
    match?: any;
}

const UsersView: React.FC<Props> = (props) => {
    const { history, match } = props;

    const commonStyles = makeCommonStyles();
    const colorStyles = makeColorStyles();
    const iconStyles = makeIconStyles();

    const [usersList, setUsersList] = useState([]);
    const [filteredUsers, setFilteredUsers] = useState([])
    const [allCheckboxState, setAllCheckboxState] = useState(false);
    const [sortingDetails, setSortingDetails] = useState({ columnId: '', orderBy: 'ASC' })
    const [filterValues, setFilterValues] = useState({ role: [], status: [], lastSignIn: [] })
    const [selectedFilter, setSelectedFilter] = useState({ role: ['All'], status: ['All'], lastSignIn: 'All' })

    const [modalDetails, setModalDetails] = useState({ state: false, actionType: '', title: '', users: [] });

    useEffect(() => {
        UserService.GetUsers((users:any) => {
            console.log(users);    
            const newUsers:Array<Object> = users.map((items)=>{
                items.isSelected = false;
                return items;
            })        
            setUsersList(newUsers)
            setFilteredUsers(newUsers)
        });
        UserService.GetFilterList((filterList) => {
            console.log(filterList); 
            
            setFilterValues(filterList);
            setSelectedFilter({ role: ['All', ...filterList.role.map(i => i.id)], status: ['All', ...filterList.status.map(i => i.id)], lastSignIn: 'All' })
        })
    }, [])

    const onDeleteSelectedUsers = () => {
        setModalDetails({
            ...modalDetails, state: true, actionType: EDIT_USER_KEYS.REMOVE,
            title: 'Remove the following users from your team?',
            users: filteredUsers.filter(i => i.isSelected)
        })
    }

    const onDeactivateSelectedUsers = () => {
        setModalDetails({
            ...modalDetails, state: true, actionType: EDIT_USER_KEYS.DEACTIVATE,
            title: 'Deactivate the following user(s)?',
            users: filteredUsers.filter(i => i.isSelected)
        })
    }

    const onDeleteUser = (id) => {
        setModalDetails({
            ...modalDetails, state: true, actionType: EDIT_USER_KEYS.REMOVE,
            title: 'Remove the following user from your team?',
            users: [filteredUsers.find(i => i.id === id)]
        })
    }

    const onDeactivateUser = (id) => {
        setModalDetails({
            ...modalDetails, state: true, actionType: EDIT_USER_KEYS.DEACTIVATE,
            title: 'Deactivate the following user?',
            users: [filteredUsers.find(i => i.id === id)]
        })
    }

    const toggleAllCheckboxState = () => {
        setAllCheckboxState(!allCheckboxState);
        const users = [...filteredUsers];
        setFilteredUsers(users.map(i => ({ ...i, isSelected: !allCheckboxState })))
    }

    const toggleUserSelection = (id) => {
        const users = [...filteredUsers];
        const selectedUser = users.find(i => i.id === id);
        selectedUser.isSelected = !selectedUser.isSelected;
        setFilteredUsers(users)
        if (selectedUser.isSelected) {
            const unSelectedUser = users.find(i => !i.isSelected)
            if (!unSelectedUser) {
                setAllCheckboxState(true)
            }
        }
        else {
            setAllCheckboxState(false)
        }
    }

    const onChangeFilter = (id, value) => {
        let newFilter = { ...selectedFilter, [id]: value };
       if(value.includes('clear')){
        newFilter = { ...selectedFilter, [id]: []}
       }
       else{

            if (!selectedFilter[id].includes('All') && value.includes('All')) { // All selected
                newFilter = { ...selectedFilter, [id]: ['All', ...filterValues[id].map(i => i.id)] }
            }
            else if (selectedFilter[id].includes('All') && !value.includes('All')) { // All deselected
                newFilter = { ...selectedFilter, [id]: [] }
            }
            else {
                let valuesWithoutAll = value.filter(i => i !== 'All');
                if (valuesWithoutAll.length === filterValues[id].length) {
                    newFilter = { ...selectedFilter, [id]: [...value, 'All'] };
                }
                else {
                    newFilter = { ...selectedFilter, [id]: valuesWithoutAll };
                }
            }
        }        

        let users = usersList.map(i => ({ ...i, isSelected: false }))
        if(id === 'status'){
            users = users.filter(i => newFilter.status.includes(i.status));
        }
        if (id === 'role') {
            users = users.filter(i => newFilter.role.includes(i.role));
        }
        if (id === 'lastSignIn') {
            users = users.filter(i => i.lastSignedIn.includes(newFilter.lastSignIn));
        }
        setSelectedFilter(newFilter)
        setFilteredUsers(users)
        setAllCheckboxState(false);
        setSortingDetails({ columnId: '', orderBy: 'ASC' })
    }

    const onDateChange = (id, value) => {
        const newFilter = { ...selectedFilter, [id]: value };
        let users = usersList.map(i => ({ ...i, isSelected: false }))
        
        if (newFilter.lastSignIn !== 'All') {
            users = users.filter(i => i.lastSignedIn.includes(newFilter.lastSignIn));
        }
        setSelectedFilter(newFilter)
        setFilteredUsers(users)
        setAllCheckboxState(false);
        setSortingDetails({ columnId: '', orderBy: 'ASC' })
    }

    const selectedUsersCount = filteredUsers.filter(i => i.isSelected).length;

    const columns = [
        {
            id: 'isSelected',
            label: <Checkbox checked={allCheckboxState} indeterminate={!allCheckboxState && selectedUsersCount > 0} onChange={toggleAllCheckboxState} />,
            type: 'checkbox',
            isLink: true,
            ignoreSorting: true,
            callbackArguments: ['id'],
            customColumns: ['id', 'isSelected'],
            customCell: (id, isSelected) => <Checkbox checked={isSelected} onChange={() => toggleUserSelection(id)} />,
        },
        {
            id: 'firstName',
            label: 'Name',
            type: 'text',
            isLink: true,
            callbackArguments: ['id'],
            customColumns: ['firstName', 'lastName', 'id'],
            customCell: (firstName, lastName, id) => { return <Typography noWrap variant="body2" className={commonStyles.pointer + ' ' + colorStyles.purple} onClick={() => gotoViewUser(id)} ><u>{firstName} {lastName}</u></Typography>; },
        },
        {
            id: 'email',
            label: 'Email',

        },
        {
            id: 'phone',
            label: 'Phone Number',
        },
        {
            id: 'role',
            label: 'Role',
            callbackArguments: ['id'],
            customColumns: ['role'],
            customCell: (role) => {
                return (
                    <Grid container alignItems='center' justify='space-between'>
                        <Grid item>
                            <Typography noWrap variant="body2"> {role}
                            </Typography>
                        </Grid>
                        <Grid item>
                            &nbsp; <Info style={{ color: '#908887', marginTop: '5px' }} aria-hidden="false" fontSize="small" />
                        </Grid>
                    </Grid>
                )
            }
        },
        {
            id: 'status',
            label: 'Status',
            iconArguments: ['status'],
            showIcon: (value) => {
                switch (value) {
                    case 'Active': return <><CheckCircle aria-hidden="false" fontSize="small" style={{ color: '#3D7AF4', marginTop: '5px' }} />&nbsp;&nbsp;</>
                    case 'Expired': return <><Cancel aria-hidden="false" fontSize="small" style={{ color: 'gray', marginTop: '5px' }} />&nbsp;&nbsp;</>
                    case 'Pending': return <><MoreHoriz aria-hidden="false" fontSize="small" style={{ color: 'gray', marginTop: '5px' }} />&nbsp;&nbsp;</>
                    case 'Deactivated': return <><RemoveCircle aria-hidden="false" fontSize="small" style={{ color: '#F4593D', marginTop: '5px' }} />&nbsp;&nbsp;</>
                }
            },
            format: (value) => value.toLocaleString('en-US'),
        },
        {
            id: 'lastSignedIn',
            label: 'Last Sign-In',
        }
    ];

    const [view, setView] = useState('list')

    const [list, setList] = React.useState([]);

    const handleSorting = (columnId) => {
        if (sortingDetails.columnId === columnId) {
            if (sortingDetails.orderBy === 'DSC') {
                sortRows(columnId, 'ASC');
                setSortingDetails({ columnId, orderBy: 'ASC' })
            }
            else {
                sortRows(columnId, 'DSC');
                setSortingDetails({ columnId, orderBy: 'DSC' })
            }
        }
        else {
            sortRows(columnId, 'ASC');
            setSortingDetails({ columnId, orderBy: 'ASC' })
        }
    }

    const sortRows = (columnId, sortType) => {
        if (sortType === 'DSC') {
            setFilteredUsers(filteredUsers.sort((a, b) => {
                const fa = a[columnId];
                const fb = b[columnId]
                if (fa < fb) {
                    return -1;
                }
                if (fa > fb) {
                    return 1;
                }
                return 0;
            })
            )
        }
        else {
            setFilteredUsers(filteredUsers.sort((a, b) => {
                const fa = a[columnId];
                const fb = b[columnId]
                if (fa < fb) {
                    return 1;
                }
                if (fa > fb) {
                    return -1;
                }
                return 0;
            })
            )
        }
    }

    const selectAll = () => {
    };

    const onEditUser = (id) => {
        history.push(`entitlements/user/edit/${id}`);
        unSelectUsers();
    }

    const actions = [
        {
            text: 'Edit',
            icon: <Edit aria-hidden="false" className={iconStyles.darkgrey} />,
            isDivider: false,
            callback: onEditUser,
            callbackArguments: ['id'],
        },
        {
            text: 'Deactivate',
            icon: <RemoveCircle aria-hidden="false" fontSize="small" style={{ color: '#F4593D' }} />,
            isDivider: false,
            callback: onDeactivateUser,
            callbackArguments: ['id']
        },
        {
            text: 'Remove',
            icon: <Cancel aria-hidden="false" fontSize="small" className={iconStyles.red} />,
            isDivider: false,
            callback: onDeleteUser,
            callbackArguments: ['id']
        },

    ];

    const tabData = [
        {
            key: 'users',
            value: <Typography>Users</Typography>
        },
        {
            key: 'activity',
            value: <Typography>Activity</Typography>
        }
    ]

    const gotoViewUser = (id) => {
        history.push('/entitlements/user/' + id);
        unSelectUsers();
    }

    const unSelectUsers = () => {
        setUsersList(usersList.map(i => ({ ...i, isSelected: false })))
    }

    const closeModal = () => {
        setModalDetails({ ...modalDetails, state: false })
    }

    return (
        <div >
            {
                modalDetails.state &&
                <DeleteModal
                    open={modalDetails.state}
                    setOpen={closeModal}
                    handleSubmit={() => { }}

                    modalHeader={modalDetails.title}
                    agreeButtonText={DIALOG_USERS_ACTIONS[modalDetails.actionType].agreeButtonText}
                    cancelButtonText={DIALOG_USERS_ACTIONS[modalDetails.actionType].cancelButtonText}
                    content={
                        <>
                            <Typography variant="subtitle1">{DIALOG_USERS_ACTIONS[modalDetails.actionType].content}</Typography>
                            <ul>
                                {
                                    modalDetails.users.map(user => (<li key={user.id}>{user.firstName} {user.lastName} ({user.role})</li>))
                                }
                            </ul>
                        </>
                    }
                />
            }
            {
                usersList.length > 0 ? (
                    <>
                        <Grid item>
                            {
                                selectedUsersCount ? (
                                    <Paper square={true}>
                                        <Grid container justify="space-between" alignItems="center" style={{ padding: '1.5rem' }}>
                                            <Grid item lg={6} md={6} xs={12}>
                                                <Typography color='primary'>{selectedUsersCount} Selected</Typography>
                                            </Grid>
                                            <Grid item lg={6} md={6} xs={12} style={{ textAlign: 'right' }}>
                                                <Button variant="text" color='primary' role="button" aria-label="Deactivate" onClick={onDeactivateSelectedUsers}><RemoveCircle />&nbsp; Deactivate</Button>
                                                <Button variant="text" color='primary' role="button" aria-label="Cancel" onClick={onDeleteSelectedUsers}><Cancel /> &nbsp;Remove</Button>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                ) : (
                                    <FilterView view={view} setView={setView} filters={filterValues} selectedFilter={selectedFilter} onChangeFilter={onChangeFilter} onDateChange={onDateChange} />
                                )
                            }
                            {
                                view === 'list' ?
                                    <UsersCardView history={history} list={filteredUsers} setList={setList} onSelectAll={toggleAllCheckboxState} allCheckboxState={allCheckboxState} onSelectItem={toggleUserSelection} sortingDetails={sortingDetails} handleSorting={handleSorting} actions={actions} gotoViewUser={gotoViewUser} source='users' />
                                    :
                                    <Paper square >
                                        <DynamicTable columns={columns} rows={filteredUsers} onSelectAll={selectAll} sortingDetails={sortingDetails} handleSorting={handleSorting} actions={actions} source='users' />
                                    </Paper>
                            }
                        </Grid>
                    </>
                )
                    :
                    <div className={commonStyles.marginTopLeftRightBottom2}>
                        <Typography>No data added</Typography>
                    </div>
            }
        </div>
    );

};

export default UsersView;
