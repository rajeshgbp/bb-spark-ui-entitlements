import React from 'react'
import UsersCardView from '../UsersCardView';
import { render } from '@testing-library/react';
import ThemeProvider from '../../../../utils/ThemeProvider'
import { createMemoryHistory } from 'history';

const history = createMemoryHistory();

it('Users CardView', () => {
    const { container } = render(
        <ThemeProvider>
            <UsersCardView history={history} list={[]} sortingDetails={{}} actions={[]}/>
        </ThemeProvider>
    );

    expect(container).toMatchSnapshot();
})