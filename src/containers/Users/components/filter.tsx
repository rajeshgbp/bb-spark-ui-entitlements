import React from 'react';
import Typography from '../../../components/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Toolbar from '@material-ui/core/Toolbar';
import { Box, MenuItem, Divider, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import IconButton from '@material-ui/core/IconButton';
// import SearchBar from '../../../components/Contents/SearchBar';
// import Filters from '../../../components/Contents/Filter';
import ListIcon from '@material-ui/icons/List';
import SearchIcon from '@material-ui/icons/Search';
// import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ViewListIcon from '@material-ui/icons/ViewList';
import { makeCommonStyles } from "../../../styles/common";
import { makeColorStyles } from "../../../styles/colors";
import { EmailField, SelectTextField, TenantNameField, MultiSelectTextField } from 'components/TextField/textfield';
import {
    MobileDateRangePicker,
    LocalizationProvider
} from "@material-ui/pickers";
import DateFnsUtils from "@material-ui/pickers/adapter/date-fns";

const useStyles = makeStyles((theme) => ({
    root: {
        '&::-webkit-scrollbar':
        {
            display: 'none',
            // flexWrap: 'nowrap',
        }
    },
    ViewList: {
        marginLeft: 4,
        marginTop: 4,

    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    indicator: {
        backgroundColor: '#ed8b00',
        height: 4,
        top: "0px",
    },
    marginTop1: {
        marginTop: '10%'
    }

}));

const useToolbarStyles = makeStyles((theme) => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
    }
}));

interface Props {
    history?: any;
    data?: any;
    view?: any;
    setView?: any;
    filters?: any;
    component?: any;
    selectedFilter?: any;
    onChangeFilter?: any;
    onDateChange?: any;
}


const FilterView: React.FC<Props> = (props) => {
    const { view, setView, filters, component, selectedFilter, onChangeFilter, onDateChange } = props;
    const [selectedDate, handleDateChange] = React.useState<any>([null, null]);
    const [customDateDialogState, setCustomDateDialogState] = React.useState(false)
    const classes = useStyles();
    const classes1 = useToolbarStyles();
    const commonStyles = makeCommonStyles();
    const colorStyles = makeColorStyles();

    function selectView(type) {
        setView(type);
    }

    return (
        <Paper square={true}>
            <Toolbar
                className={clsx(classes1.root)}
            >
                <Grid container justify='space-between' className={commonStyles.marginTopLeftRightBottom2}>

                    <Grid item md={10} sm={12} spacing={2} container>

                        <Grid item xs={2} >
                            <MultiSelectTextField
                                label="Status"
                                select
                                aria-label="Select Status"
                                aria-value={selectedFilter.status}
                                id="status"
                                selectedValue={selectedFilter.status}
                                className={commonStyles.autoWidth + " " + commonStyles.height3}
                                variant="outlined"
                                onChange={(e) => onChangeFilter("status", e.target.value, e)}
                                values={filters.status ? filters.status : []}
                            />
                        </Grid>
                        <Grid item xs={2} >
                            <MultiSelectTextField
                                label="Roles"
                                select
                                aria-label="Select Roles"
                                aria-value={selectedFilter.role}
                                id="role"
                                selectedValue={selectedFilter.role}
                                className={commonStyles.autoWidth + " " + commonStyles.height3}
                                variant="outlined"
                                onChange={(e) => onChangeFilter("role", e.target.value, e)}
                                values={filters.role ? filters.role : []}
                            />

                        </Grid>
                        <Grid item xs={2} >
                            <SelectTextField
                                label="Last Signin"
                                select
                                aria-label="Select Last Signin"
                                aria-value={selectedFilter.lastSignIn}
                                value={selectedFilter.lastSignIn}
                                id="lastSignIn"
                                className={commonStyles.autoWidth + " " + commonStyles.height3}
                                variant="filled"
                                onChange={(e) => {
                                    onDateChange("lastSignIn", e.target.value)
                                }}
                            >
                                <MenuItem key='All' value='All' style={{ margin: '8px 0' }}>
                                    All
                                </MenuItem >
                                {
                                    filters.lastSignIn && filters.lastSignIn.map(i => {
                                        return (
                                            <MenuItem key={i} value={i} style={{ margin: '8px 0' }}>
                                                {i}
                                            </MenuItem >
                                        )
                                    })
                                }
                                <Divider />
                                <LocalizationProvider dateAdapter={DateFnsUtils}>
                                    <MobileDateRangePicker
                                        startText="Start"
                                        endText="End"
                                        toolbarTitle='Custom date range'
                                        okLabel='done'
                                        value={selectedDate}
                                        onChange={date => {
                                            handleDateChange(date)
                                        }}
                                        open={customDateDialogState}
                                        onClose={() => {
                                            setCustomDateDialogState(false)
                                        }}
                                        className='dateDialog'
                                        renderInput={(startProps, endProps) => (
                                            <>
                                                {
                                                    customDateDialogState ? (
                                                        <>
                                                            <TextField {...startProps} variant="outlined" label=''/> &nbsp;&nbsp;
                                                            <TextField {...endProps} />
                                                        </>
                                                    ) : (
                                                        <Typography style={{ margin: '5px 0 0 15px',cursor:'pointer' }} onClick={()=>{
                                                            setTimeout(()=>{
                                                                setCustomDateDialogState(true)
                                                            },200)
                                                        }}> Custom date range &nbsp;&nbsp;&nbsp;&nbsp;</Typography>
                                                    )
                                                }
                                            </>
                                        )
                                        }
                                    />
                                </LocalizationProvider>
                            </SelectTextField>
                        </Grid>

                    </Grid>
                    {
                        component !== 'user-activity' &&
                        <Grid item lg={2} md={2} sm={12} xs={12} className={commonStyles.centerAlign} justify="flex-end">
                            <Grid container spacing={2} alignItems="center" justify="flex-end">
                                <Grid item >
                                    <Typography align='right' variant="body2" color="textSecondary" >View</Typography>
                                </Grid>
                                <Grid item >
                                    <Grid container>
                                        <Grid item >
                                            <Box border={1} aria-label="List View" className={view === 'list' ? commonStyles.selecteditem : commonStyles.notselecteditem} >
                                                <IconButton aria-label="List View" data-testid={'listView'} onClick={() => selectView('list')} style={{ paddingTop: '0px', paddingRight: '0px', paddingLeft: '0px', paddingBottom: '0px' }}>
                                                    <Typography color="textSecondary" variant="body2" className={commonStyles.pointer}><ViewListIcon style={view === 'list' ? { color: '#6b63a1' } : { color: '' }} className={commonStyles.ViewList} /></Typography>
                                                </IconButton>
                                            </Box>
                                        </Grid>
                                        <Grid item>
                                            <Box border={1} aria-label="Table View" className={view === 'table' ? commonStyles.selecteditem : commonStyles.notselecteditem} >
                                                <IconButton aria-label="Table View" data-testid={'tableView'} onClick={() => selectView('table')} style={{ paddingTop: '0px', paddingRight: '0px', paddingLeft: '0px', paddingBottom: '0px' }}>
                                                    <Typography color="textSecondary" variant="body2" className={commonStyles.pointer}><ListIcon style={view === 'table' ? { color: '#6b63a1' } : { color: '' }} className={commonStyles.ViewList} /></Typography>
                                                </IconButton>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    }
                </Grid>
            </Toolbar>
        </Paper>
    );
};

export default FilterView;