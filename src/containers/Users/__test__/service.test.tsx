import React from 'react'
import Service from '../service';
import { render } from '@testing-library/react';
import ThemeProvider from '../../../utils/ThemeProvider'
import { createMemoryHistory } from 'history';

const history = createMemoryHistory();

it('Users service', () => {
    const { container } = render(
        <ThemeProvider>
            <Service history={history} />
        </ThemeProvider>
    );
    expect(container).toMatchSnapshot();
})