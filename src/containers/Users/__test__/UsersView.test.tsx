import React from 'react'
import UsersView from '../UsersView';
import { render } from '@testing-library/react';
import ThemeProvider from '../../../utils/ThemeProvider'
import { createMemoryHistory } from 'history';

const history = createMemoryHistory();

it('Users View', () => {
    const { container } = render(
        <ThemeProvider>
            <UsersView history={history} />
        </ThemeProvider>
    );

    expect(container).toMatchSnapshot();
})