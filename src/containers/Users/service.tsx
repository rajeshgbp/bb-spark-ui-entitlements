import React, { useState } from 'react';
import { Checkbox, Grid, Paper, Button } from '@material-ui/core'
import Header from '../SparkComponents/UserHeader';
import Typography from 'components/Typography';
import { makeCommonStyles, makeIconStyles, makeColorStyles, makeButtonStyles } from "../../styles";
import UsersCardView from './components/UsersCardView';
import DynamicTable from '../../components/Table/DynamicTable';
import FilterView from './components/filter';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import UsersView from './UsersView';
import UsersActivity from './UsersActivity';

import { Email } from '@material-ui/icons';

interface Props {
    history: any;
    match?: any;
}

const LandlordsService: React.FC<Props> = (props) => {
    const { history, match } = props;

    const commonStyles = makeCommonStyles();
    const buttonStyles = makeButtonStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    const tabData = [
        {
            key: 'users',
            value: <Typography>Users</Typography>
        },
        // {
        //     key: 'activity',
        //     value: <Typography>Activity</Typography>
        // }
    ]

    return (
        <div >
            <div>
                <Header
                    profilePicture=" "
                    headerTitle={value ? "Activity" : "Team Management"}
                    headerData={

                        <Tabs
                            value={value}
                            indicatorColor="secondary"
                            style={{ position: 'absolute', marginTop: '-1rem' }}
                            onChange={handleChange}
                            aria-label="users activity tabs"
                        >
                            {
                                tabData.map((items) =>
                                    <Tab label={items.value} key={items.key} style={{ minWidth: '3rem' }} />
                                )
                            }
                        </Tabs>
                    }
                />
            </div>
            <Grid container className={commonStyles.contentRoot} direction='column'>
                <div className={commonStyles.propertiesFabButtonMargin}>
                    <Typography align='right' >
                        <Button variant='contained' className={buttonStyles.PropertieslistviewFabButton} onClick={() => history.push('/entitlements/inviteUser')}>
                            <Grid container spacing={2} direction='row' wrap="nowrap" alignItems="center" justify="center">
                                <Grid item lg={2} sm={2} md={2} xs={2} zeroMinWidth className={commonStyles.marginTopXXS}>
                                    <Email aria-hidden="false" />
                                </Grid>
                                <Grid item lg={10} sm={10} md={10} xs={10} zeroMinWidth>
                                    <Typography display='inline' variant="body2" weight="bold">
                                        Invite User
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Button>
                    </Typography>
                </div>
                <br />
                {
                    value ?
                        <UsersActivity
                            history={history}
                            match={match}
                        />
                        :
                        (
                            <UsersView
                                history={history}
                                match={match} />
                        )
                }
            </Grid>
        </div>
    );

};

export default LandlordsService;
