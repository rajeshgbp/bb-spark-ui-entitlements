import React, { useState, useEffect } from 'react';
import { UserService } from '../../services';
import { Grid, Typography } from '@material-ui/core';
import {
  makeCommonStyles,
  makeIconStyles,
  makeColorStyles,
  makeButtonStyles
} from '../../styles';
import ActivityListView from './components/ActivityListView';
import FilterView from './components/filter';

interface Props {
  history: any;
  match?: any;
}

const years = ['2020', '2019'];

const UsersActivity: React.FC<Props> = (props) => {
  const { history } = props;

  const commonStyles = makeCommonStyles();
  const buttonStyles = makeButtonStyles();
  const colorStyles = makeColorStyles();
  const iconStyles = makeIconStyles();

  const [usersList, setUsersList] = useState([]);
  const [filteredUsers,setFilteredUsers] = useState([])
  const [view, setView] = useState();
  const [filterValues, setFilterValues] = useState({ roles: [], status: [], lastSignIn: [] })
  const [selectedFilter, setSelectedFilter] = useState({ role: 'All', status: 'All', lastSignIn: 'All' })

  useEffect(() => {
    UserService.GetUserActivities((activities) => {
      setUsersList(activities)
      setFilteredUsers(activities)
    })
    UserService.GetFilterList((filterList) => {
      setFilterValues(filterList);
    })
  }, [])

  const onChangeFilter = (id, value) => {    
    const newFilter = {...selectedFilter,[id]:value};
    let users=usersList.map(i=>({...i,isSelected:false}))
    if(newFilter.status !== 'All'){
        users = users.filter(i=>i.status === newFilter.status);
    }
    if(newFilter.role !== 'All'){
        users = users.filter(i=>i.role === newFilter.role);
    }
    if(newFilter.lastSignIn !== 'All'){
        users = users.filter(i=>i.lastSignedIn.includes(newFilter.lastSignIn));
    }
    setSelectedFilter(newFilter)
    setFilteredUsers(users)
  }

  return (
    <div>
      <Grid container className={commonStyles.paddingXXXS} direction="column">
        {usersList.length > 0 ? (
          <>
            <Grid item>
              <FilterView
                view={view}
                setView={setView}
                component="user-activity"
                filters={filterValues} selectedFilter={selectedFilter} onChangeFilter={onChangeFilter}
              />
              {
                <ActivityListView
                  history={history}
                  list={usersList}
                  years={years}
                />
              }
            </Grid>
          </>
        ) : (
          <div className={commonStyles.marginTopLeftRightBottom2}>
            <Typography>No data added</Typography>
          </div>
        )}
      </Grid>
    </div>
  );
};

export default UsersActivity;
