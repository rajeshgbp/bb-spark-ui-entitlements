import React from 'react';
import Grid from '@material-ui/core/Grid';
// import PublishIcon from '@material-ui/icons/Publish';
import Link from '@material-ui/core/Link';
import IconButton from '@material-ui/core/IconButton';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import LinearProgress from '@material-ui/core/LinearProgress';
import DeleteIcon from '@material-ui/icons/Delete';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Typography from '../../components/Typography';
import Card from '@material-ui/core/Card';
import { makeCommonStyles } from "../../styles/common";
import {useDropzone} from 'react-dropzone';
import Dropzone from 'react-dropzone';
import { API, PortfolioService } from '../../services';


const useStyles = makeStyles(() => ({
    fileHeight: {
        border: "1px solid gray",
        width: "100%",
        borderStyle: "dashed",
        height: "130px",
    },
    marginTop: {
        marginTop: "20px",
    },
    dottedBorder: {
        borderTop: '0.2px solid lightGrey',
    },
    progressBar: {
        width: 285
    }
}));

interface Props {
    files?: any;
    onLoad?: any;
    source?: any;
    type?: any;
}

const UploadDocuments: React.FC<Props> = (props) => {
    const { files = [], onLoad, source,type } = props;

    function onDeleteDocument(id) 
    {
        PortfolioService.DeleteDocument(id, function (result) {
            if (result.status === 1) 
            {
                 API.showSuccess(result.message);
            }
            else 
            {
                API.showError(result.message);
            }
        });
    };

    const commonStyles = makeCommonStyles();
    const classes = useStyles();

    const newFile = { url: null, file: null, name: null, type: '' };

    // const onFileUpload = event => {
    //     let modifiedFile = { ...newFile };


    //     modifiedFile = {
    //         url: URL.createObjectURL(event.target.files[0]),
    //         file: event.target.files[0],
    //         name: event.target.files[0].name,
    //         type: '',
    //     };

    //     let updatedFiles = [...files];

    //     updatedFiles.push(modifiedFile);

    //     onLoad(updatedFiles);
    // };

    const onFileRemove = (index, file) => 
    {
        let modifiedFiles = [...files];

        if (file.id)
        {
            onDeleteDocument(file.id);
        }
        modifiedFiles.splice(index, 1);

        onLoad(modifiedFiles);
    };

    // const onSelectFileType = (index, value) => {
    //     let modifiedFiles = [...files];

    //     modifiedFiles[index].type = value;

    //     onLoad(modifiedFiles);
    // };

    const onDragAndDrop = (acceptedFiles) => {

        let modifiedFile = { ...newFile };


        modifiedFile = {
            url: URL.createObjectURL(acceptedFiles[0]),
            file: acceptedFiles[0],
            name: acceptedFiles[0].name,
            type: '',
        };

        let updatedFiles = [...files];

        updatedFiles.push(modifiedFile);

        for (var previousUnitIndex = 0; previousUnitIndex < updatedFiles.length; previousUnitIndex++) 
        {
            for (var currentUnitIndex = 0; currentUnitIndex < updatedFiles.length; currentUnitIndex++) 
            {
                if (currentUnitIndex === previousUnitIndex) 
                {
                    continue;
                }

                if (updatedFiles[currentUnitIndex].name === updatedFiles[previousUnitIndex].name) 
                {
                    API.showError("File must be unique.");
                    return;
                }
            }
        }
        
        onLoad(updatedFiles);
    };

    const onClickHandlerForLabel = (e) => {
        // if(e.key === "Enter") {
        //     e.preventDefault();
        //     e.currentTarget.click();
        // }
        if(e.target === e.currentTarget) {
            e.preventDefault();
            e.currentTarget.click();  
        }
    };


    return (
        <div>
            <Dropzone noClick={true} noKeyboard={true}  onDrop={acceptedFiles => onDragAndDrop(acceptedFiles)}>
                {({ getRootProps, getInputProps }) => (
                    <section>
                        <div {...getRootProps()}>
                            <div className={commonStyles.marginTop3 + " " + classes.fileHeight}>
                                <span className={commonStyles.marginTopLeftRightBottom1 + " " + classes.marginTop}>
                                    <input id="icon-button-file" data-testid="handlechangeImage" type="file" value={""} className={commonStyles.hide} {...getInputProps()} />
                                    <div>
                                        <Typography color="textSecondary" align='center'>
                                            {type === "white" ? (<img alt="upload Icon" src="/app/assets/rp/icons/upload-document-icon.PNG" height="35px" width="30px"/>)
                                                : (<img alt="upload Icon" src="/app/assets/rp/icons/upload-document-icon.PNG" height="35px" width="30px"/>)}

                                        </Typography>
                                        <Grid container justify="center" spacing={1}  >
                                            <Grid item>
                                                <Typography align='center'> Drag and drop files here </Typography>
                                            </Grid>
                                        </Grid>
                                        
                                            <Typography align='center' >or<b> <label htmlFor="icon-button-file" tabIndex={0} data-testid={'uploadFile'} onKeyPress={(e) => onClickHandlerForLabel(e)}><Link  className={commonStyles.pointer}>
                                            <span aria-label='link browse files.'></span><span aria-hidden="true">
                                                 browse files.</span></Link> </label></b></Typography>
                                       
                                    </div>

                                </span>
                            </div>
                        </div>
                    </section>
                )}
            </Dropzone>

            <div className={commonStyles.marginTopXS} >

                {files.map((file, index) => (
                    <>
                        <Card square className={classes.dottedBorder}>
                            <div className={commonStyles.marginTopLeftRightBottom2}>
                                <Grid container justify="space-between">
                                    <Grid item >
                                        <div style={{display:'flex', flexDirection:'row', gap:'10px'}}>
                                            <div>
                                                <Typography color="textSecondary" >
                                                    <InsertDriveFileIcon />
                                                </Typography>
                                            </div>

                                            <div>
                                                <Typography>{file ? file.name : ''}</Typography>
                                            </div>
                                        </div>
                                    </Grid>

                                    <Grid item >
                                        <Typography color="textSecondary" >
                                            <IconButton aria-label="Delete" onClick={() => onFileRemove(index, file)}  data-testid={"delete"+ index}className={commonStyles.noPadding}>
                                                < DeleteIcon className={commonStyles.pointer} />
                                            </IconButton>
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </div>
                        </Card>
                        <Grid item >
                          
                        </Grid>
                    </>
                ))}
            </div>

        </div >
    );
};

export default UploadDocuments;