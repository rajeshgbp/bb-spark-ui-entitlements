import React from 'react';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Link from '@material-ui/core/Link';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import LinearProgress from '@material-ui/core/LinearProgress';
import DeleteIcon from '@material-ui/icons/Delete';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Typography from 'components/Typography';
import Card from '@material-ui/core/Card';
import { makeCommonStyles } from "styles/common";
import Dropzone from 'react-dropzone';

const useStyles = makeStyles((theme) => ({
    fileHeight: {
        border: "1px solid gray",
        width: "100%",
        borderStyle: "dashed",
        height: "130px",
    },
    marginTop: {
        marginTop: "20px",
    },
    dottedBorder: {
        borderTop: '0.2px solid lightGrey',
    },
    progressBar: {
        width: 285
    }
}));

interface Props
{
    uploadedDocumentUrl?: any;
    setDocumentUrl?: any;
    file?: any;
    onLoad?: any;
    setuploadFile?: any;
    uploadFile?: any;
    type?:any;
}

const FileUploadDoc: React.FC<Props> = (props) =>
{
    const { type,file, onLoad, uploadedDocumentUrl, setuploadFile, uploadFile, setDocumentUrl } = props;

    const commonStyles = makeCommonStyles();

    const classes = useStyles();

    const uploadFileNew = props.uploadFile || { url: null, file: null, name: null };

    // const handleImageChange = event => 
    // {
    //     let modifiedFile = {...uploadFileNew};

    //     modifiedFile = {
    //         url: URL.createObjectURL(event.target.files[0]),
    //         file: event.target.files[0],
    //         name: event.target.files[0].name,
    //         isSaved: false
    //     };

    //     setuploadFile(modifiedFile);
    //     onLoad(event.target.files[0]);
    // };

    const onDragAndDrop = (acceptedFiles) => 
    {
        let modifiedFile = {...uploadFileNew};

        modifiedFile = {
            url: URL.createObjectURL(acceptedFiles[0]),
            file: acceptedFiles[0],
            name: acceptedFiles[0].name,
            isSaved: false
        };

        setuploadFile(modifiedFile);
        onLoad(acceptedFiles[0]);
    };

    const onClickHandlerForLabel = (e) => {
        // if(e.key === "Enter") {
        //     e.preventDefault();
        //     e.currentTarget.click();
        // }
        if(e.target === e.currentTarget) {
            e.preventDefault();
            e.currentTarget.click();  
        }
    };
    
    // const handleClickRemovePhoto = () => 
    // {
    //     let modifiedFile = {...uploadFile};

    //     modifiedFile = 
    //     {
    //         url: null,
    //         file: null,
    //         name: null,
    //         isSaved: false
    //     };

    //     setuploadFile(modifiedFile);
    //     onLoad({});
    //     setDocumentUrl(null);
    // };

    return (

        <div>

            <Dropzone noClick={true} noKeyboard={true} onDrop={acceptedFiles => onDragAndDrop(acceptedFiles)}>
                {({ getRootProps, getInputProps }) => (
                    <section>
                        <div {...getRootProps()}>
                            <div className={commonStyles.marginTop3 + " " + classes.fileHeight}>
                                <span className={commonStyles.marginTopLeftRightBottom1 + " " + classes.marginTop}>
                                    <input id="icon-button-file" data-testid="handlechangeImage" type="file" value={""} className={commonStyles.hide} {...getInputProps()}/>
                                    <div>
                                        <Typography color="textSecondary" align='center'>
                                            {type === "multidoc" ? (<img alt="upload Icon" src="/app/assets/rp/icons/upload-document-icon.PNG" height="35px" width="30px"/>) :
                                                (<img alt="upload Icon" src="/app/assets/rp/icons/upload-document-icon.PNG" height="35px" width="30px"/>)}
                                        </Typography>
                                        <Grid container justify="center" spacing={1}  >
                                            <Grid item>
                                                <Typography align='center'> Drag and drop files here </Typography>
                                            </Grid>
                                        </Grid>
                                        
                                            <Typography align='center' >or<b> <Link  className={commonStyles.pointer}> 
                                            <label data-testid="fileUpload" htmlFor="icon-button-file" tabIndex={0} onKeyPress={(e) => onClickHandlerForLabel(e)} >
                                            <span aria-label='link browse files.'></span><span aria-hidden="true">browse files.</span></label></Link></b></Typography>
                                        
                                    </div>
                                </span>
                            </div>
                        </div>
                    </section>
                )}
            </Dropzone>


            {file || uploadFile.url  ?
                <div className={commonStyles.marginTopXS} >
                    <Card square className={classes.dottedBorder}>
                        <div className={commonStyles.marginTopLeftRightBottom2}>
                            <Grid container justify="space-between">
                                <Grid item>
                                    <div style={{display:'flex', flexDirection:'row', gap:'10px'}}>
                                        <div>
                                            <Typography color="textSecondary" >
                                                <InsertDriveFileIcon />
                                            </Typography>
                                        </div>

                                        <div>
                                            {/* {uploadedDocumentUrl} */}
                                            <Typography>{file ? file.name : uploadFile.name}</Typography>
                                        </div>
                                    </div>
                                </Grid>
                                <Grid item>
                                </Grid>
                            </Grid>
                        </div>
                    </Card>

                </div>
                :
                ''
            }
        </div >
    );
};

export default FileUploadDoc;