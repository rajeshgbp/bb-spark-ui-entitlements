import React from 'react';
// import PropTypes from 'prop-types';
// import { Typography } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
// import Fab from '@material-ui/core/Fab';
// import AddIcon from '@material-ui/icons/Add';
// import { layoutStyles } from "layouts/styles";
import Grid from '@material-ui/core/Grid';
import Typography from 'components/Typography';
// import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { makeIconStyles } from "styles/icons";
import { makeCommonStyles } from "styles/common";
import useMediaQuery from '@material-ui/core/useMediaQuery';
// import SaveIcon from '@material-ui/icons/Save';
import { makeColorStyles } from 'styles';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';


interface Props {
    headerData: any;
    headerTitle: any;
    // headerUnit: any,
    profilePicture: string;
}

const Header: React.FC<Props> = ({ headerData, headerTitle, profilePicture, ...rest }: Props) => {

    const iconStyles = makeIconStyles();
    const commonStyles = makeCommonStyles();
    const colorStyles = makeColorStyles();

    const theme = useTheme();
    const isLg = useMediaQuery(theme.breakpoints.up('lg'));
    const isSm = useMediaQuery(theme.breakpoints.up('sm'));
    const isMd = useMediaQuery(theme.breakpoints.up('md'));;

    return (

        // <div {...rest} >
        //     {isLg || isMd || isSm ?
        //         (

        <div className={commonStyles.headerColor}>

            <div className={commonStyles.marginTopXS + " " + commonStyles.marginLeft2}>
                <Grid container justify="space-between" direction='column' className={commonStyles.marginTopBottom2}>
                    <Grid item xs={10} className={commonStyles.marginLeftXLg + " " + commonStyles.marginBottom1}>
                        <Button className={colorStyles.white} startIcon={<ArrowBackIosIcon style={{ fontSize: "14px" }} className={iconStyles.primary} />} aria-label={"Back to dashboard"} data-testid="dashboard">
                            <div className={commonStyles.marginTopXXS}>
                                <Typography variant="body2" textColor="white">DASHBOARD</Typography>
                            </div>
                        </Button>
                    </Grid>
                    <Grid >
                        <div className={commonStyles.marginLeftXLg}>
                            <Typography variant="h5" component="h1" weight="light">  {headerTitle} </Typography>
                            <Typography className={commonStyles.marginTopXLarge }>{headerData} </Typography>

                        </div>
                    </Grid>
                </Grid>
            </div>

        </div>
        //         ) :
        //         (
        //             <div></div>
        //         )
        //     }
        // </div>
    );
};

// Header.propTypes = {
//     className: PropTypes.string,
//     headerData: PropTypes.string
// };

export default Header;
