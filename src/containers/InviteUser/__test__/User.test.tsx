import React from 'react'
import User from '../User';
import { render } from '@testing-library/react';
import ThemeProvider from '../../../utils/ThemeProvider'

it('User', () => {
    const { container } = render(
        <ThemeProvider>
            <User user={{firstName: '', lastName: '', email: '', phoneNumber: ''}} index={0} onChange={()=>{}} deleteUser={()=>{}}/>
        </ThemeProvider>
    );

    expect(container).toMatchSnapshot();
})