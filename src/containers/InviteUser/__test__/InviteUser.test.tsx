import React from 'react'
import InviteUser from '../InviteUser';
import { render } from '@testing-library/react';
import ThemeProvider from '../../../utils/ThemeProvider'
import { createMemoryHistory } from 'history';

const history = createMemoryHistory();

it('InviteUser', () => {
    const { container } = render(
        <ThemeProvider>
            <InviteUser history={history}/>
        </ThemeProvider>
    );

    expect(container).toMatchSnapshot();
})