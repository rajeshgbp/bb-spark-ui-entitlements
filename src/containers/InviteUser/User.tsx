import React from 'react';
import Header from '../SparkComponents/UserHeader';
import Grid from '@material-ui/core/Grid';
import Typography from 'components/Typography';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import MenuItem from '@material-ui/core/MenuItem';
import PhoneIcon from '@material-ui/icons/Phone';
import { EmailField, SelectTextField, TenantNameField, DollarRedditTextFieldForProcessing } from 'components/TextField/textfield';
import CancelIcon from '@material-ui/icons/Cancel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TablePagination from '@material-ui/core/TablePagination';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import Divider from '@material-ui/core/Divider';
import EditIcon from '@material-ui/icons/Edit';
import { makeStyles } from '@material-ui/core/styles';
import { makeButtonStyles } from "styles/buttons";
import { makeCommonStyles } from "styles/common";
import Card from '@material-ui/core/Card';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Avatar from '@material-ui/core/Avatar';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { makeColorStyles } from "styles/colors";
import { TextField } from '@material-ui/core';
import { ValidatorForm } from 'react-material-ui-form-validator';

const useStyles = makeStyles((theme) => ({
    box: {
        backgroundColor: '#f7f6f5', boxShadow: "none",

    },
    inviteLandlordFab: {
        position: 'absolute',
        top: "184px",
        right: "56px",
        backgroundColor: '#ed8b00',
        color: '#000000',
        borderRadius: "26px",
        paddingLeft: "20px",
        paddingRight: "20px",
        height: "48px",
        width: "175px",
    },
}));


const EnhancedTableToolbar = (props) => {
    const { onRequestSort } = props;

    const commonStyles = makeCommonStyles();
    const colorStyles = makeColorStyles();

    return (
        <div>
            <Paper square={true} elevation={0} className={colorStyles.tablecell}>
                <div >
                    <Grid container justify="space-between" >
                        <Grid item className={commonStyles.marginTopLeftRightBottom2}>
                            <Grid container spacing={2} >
                                <Grid item>
                                    <Typography variant="body2" className={commonStyles.pointer}>
                                        {"Select All"}
                                    </Typography>
                                </Grid>

                            </Grid>
                        </Grid>
                        <Grid item className={commonStyles.marginTopLeftRightBottom2} >
                            <Grid container>
                                <Grid item>
                                    <Typography variant="body2">Sort by&nbsp;</Typography>
                                </Grid>
                                <Grid item>
                                    <Typography color='textSecondary' variant="body2">
                                        <ArrowDropDownIcon aria-hidden="false" className={commonStyles.pointer} />
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography color='textSecondary' variant="body2">
                                        <ImportExportIcon aria-hidden="false" className={commonStyles.pointer} />&nbsp;
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </div>
            </Paper>
        </div>
    );
};

EnhancedTableToolbar.propTypes = {

    onRequestSort: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
};

const payeeTypes = [
    {
        value: 'Rent Hub'
    },
    {
        value: 'Property Owner'
    },
    {
        value: 'Tenant'
    }
];

interface Props {
    user: any,
    index: Number,
    onChange: Function,
    deleteUser: Function
}

const User: React.FC<Props> = (props) => {

    const { user, onChange, index, deleteUser } = props;
    const commonStyles = makeCommonStyles();

    return (

        <ValidatorForm onSubmit={() => { }}>
            <Grid container spacing={2} alignItems='center'>
                <Grid item container xs={11} spacing={2} justify='space-between'>
                    <Grid item xs={3}>
                        <TenantNameField
                            label="First Name *"
                            autoFocus
                            value={user.firstName}
                            onChange={(e) => onChange(index, 'firstName', e.target.value)}
                            className={commonStyles.autoWidth}
                            variant="filled"
                            validators={['required', 'trim']}
                            errorMessages={['This field is required', 'The field should not be empty']}
                            id="firstName"
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <TenantNameField
                            label="Last Name *"
                            value={user.lastName}
                            onChange={(e) => onChange(index, 'lastName', e.target.value)}
                            className={commonStyles.autoWidth}
                            variant="filled"
                            validators={['required', 'trim']}
                            errorMessages={['This field is required', 'The field should not be empty']}
                            id="lastName"
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <EmailField
                            label="Email *"
                            className={commonStyles.autoWidth}
                            variant="filled"
                            value={user.email}
                            onChange={(e) => onChange(index, 'email', e.target.value)}
                            id="email"
                            validators={['required', "matchRegexp:^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+\\.[A-Za-z]{2,64}]*$"]}
                            errorMessages={['Email is required', 'Please enter a valid email address']}
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <TenantNameField
                            label="Phone number *"
                            value={user.phoneNumber}
                            onChange={(e) => onChange(index, 'phoneNumber', e.target.value)}
                            className={commonStyles.autoWidth}
                            variant="filled"
                            validators={['required', 'trim']}
                            errorMessages={['This field is required', 'The field should not be empty']}
                            id="phoneNumber"
                        />
                    </Grid>
                </Grid>
                <Grid item xs={1} justify='center' alignItems='center'>
                    <Button onClick={() => deleteUser(index)}> X </Button>
                </Grid>
            </Grid>
        </ValidatorForm>

    );
};

export default User;