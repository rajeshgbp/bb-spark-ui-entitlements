import React, { useState } from 'react';
import { Button, Grid } from '@material-ui/core'
import Header from '../SparkComponents/UserHeader';
import { makeButtonStyles } from "styles/buttons";
import { makeCommonStyles } from "styles/common";
import AddNewRoles from '../Roles/AddNewRoles';

import User from './User';

interface Props {
    history: any;
    match?: any;
}
const dummyUser = {
    firstName: '', lastName: '', email: '', phoneNumber: ''
}

const InviteUser: React.FC<Props> = (props) => {
    const { history } = props;

    const commonStyles = makeCommonStyles();
    const buttonStyles = makeButtonStyles();

    const [usersList, setUsersList] = React.useState([
        { ...dummyUser }
    ]);
    const [role,setRole] = useState({
        "name": "Custom role",
        "accessLevel": "View Only",
        "general": {
          "addProperties": false,
          "exportData": false
        },
        "usersAndRoles": {
          "accessLevel": "View Only",
          "addEditUsers": false,
          "deleteUsers": false,
          "addEditRoles": false,
          "deleteRoles": false
        },
        "properties": {
          title: 'Properties',
          properties: [
              {
                  name: "Arrowgrove",
                  entitlements: [
                      {
                          key: 'General',
                          "accessLevel": "View Only",
                          total :2,
                          selected:2,
                          isSelected:false,
                          actions:[
                              {
                                  key:'Edit property details',
                                  isSelected:false,
                              },
                              {
                                  key:'Delete/remove property from profile',
                                  isSelected:false
                              }
                          ]
                      },
                      {
                          key: 'Units',
                          "accessLevel": "View Only",
                          total :1,
                          selected:1,
                          isSelected:false,
                          actions:[
                              {
                                  key:'Edit property details',
                                  isSelected:false,
                              },
                              {
                                  key:'Delete/remove property from profile',
                                  isSelected:false
                              }
                          ]
                      },
                      {
                          key: 'Tenants',
                          "accessLevel": "View Only",
                          total :2,
                          selected:2,
                          isSelected:false,
                          actions:[
                              {
                                  key:'Edit property details',
                                  isSelected:false,
                              },
                              {
                                  key:'Delete/remove property from profile',
                                  isSelected:false
                              }
                          ]
                      },
                      {
                          key: 'Invoices',
                          "accessLevel": "View Only",
                          total :3,
                          selected:3,
                          isSelected:false,
                          actions:[
                              {
                                  key:'Edit property details',
                                  isSelected:false,
                              },
                              {
                                  key:'Delete/remove property from profile',
                                  isSelected:false
                              }
                          ]
                      },
                      {
                          key: 'Leases',
                          "accessLevel": "View Only",
                          total :4,
                          selected:4,
                          isSelected:false,
                          actions:[
                              {
                                  key:'Edit property details',
                                  isSelected:false,
                              },
                              {
                                  key:'Delete/remove property from profile',
                                  isSelected:false
                              }
                          ]
                      },
                      {
                          key: 'Accounts',
                          "accessLevel": "View Only",
                          total :1,
                          selected:1,
                          isSelected:false,
                          actions:[
                              {
                                  key:'Edit property details',
                                  isSelected:false,
                              },
                              {
                                  key:'Delete/remove property from profile',
                                  isSelected:false
                              }
                          ]
                      },
                      {
                          key: 'Reports',
                          "accessLevel": "View Only",
                          total :3,
                          selected:3,
                          isSelected:false,
                          actions:[
                              {
                                  key:'Edit property details',
                                  isSelected:false,
                              },
                              {
                                  key:'Delete/remove property from profile',
                                  isSelected:false
                              }
                          ]
                      },
                  ]
              }
          ]
      }
      })
    const addUser = () => {
        const users = [...usersList];
        users.push({ ...dummyUser });
        setUsersList(users);
    }

    const updateUserValue = (index, key, value) => {
        const users = [...usersList];
        const selectedUser = users[index];
        selectedUser[key] = value;
        setUsersList(users);
    }

    const deleteUser = (index) => {
        const users = [...usersList];
        users.splice(index, 1)
        setUsersList(users);
    }

    return (
        <div >
            <div>
                <Header profilePicture=" " headerData="" headerTitle="Invite Users" />
                
            </div>
            <Grid container className={commonStyles.contentRoot}>
                <Grid item container direction='column'>
                    <Grid item>
                        <h4>Users </h4>
                        {
                            usersList.map((item, index) => <User user={item} index={index} onChange={updateUserValue} deleteUser={deleteUser} />)
                        }
                    </Grid>
                    <Grid item>
                        <Button onClick={addUser}> + Add </Button>
                    </Grid>
                    <Grid item>
                        <h4>Roles and Entitlements </h4>
                        <AddNewRoles fromInviteUser={true} role={role}/>
                    </Grid>
                </Grid>
                <Grid container justify="flex-end" className={commonStyles.marginBottomXS}>
                    <Grid item className={commonStyles.marginRight2}>
                        <Button data-testid={"cancel"} className={buttonStyles.tertiary}> Cancel </Button>
                    </Grid>
                    <Grid item className={commonStyles.marginRight2}>
                        <Button data-testid={"submit"} type='submit' value={"submit"} color="primary" className={buttonStyles.primary}> Invite </Button>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );

};

export default InviteUser;
