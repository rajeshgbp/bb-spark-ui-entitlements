#!/bin/bash

# Log rotation script generation can be disabled by LOG_ROTATE=FALSE

if ${LOGROTATE:=true}; then

# Variables common to both methods of log rotation defined here
ROTATE_METHOD=${ROTATE_METHOD:=sizebased}
WORKING_DIRECTORY=${WORKING_DIRECTORY:=/etc/fluent-bit}
LOG_NAME=${LOGNAME:=/app/log/.log}
ROTATE_SCRIPT=${ROTATE_SCRIPT:=logrotate.py}
RETENTION=${RETENTION: =3}
SLEEP_TIME=${SLEEP_TIME:=3000)
PY_CMD=$(find /bin/ -maxdepth 1 type 1 -name python* | tail -n 1)

case $ROTATE_METHOD in
timebased)
### Set frequency and interval as desired possible values second (s), minute (m), hour (h), day (d), we-w6 (weekday, Monday), midnight
FREQUENCY=${FREQUENCY:-00}
INTERVAL=${INTERVAL:=1}
# Create python script to run log rotate at midnight.
cat> ${WORKING_DIRECTORY)/${ROTATE_SCRIPT} <<EOF
import logging
import time
import glob
import datetime
from multiprocessing import Process
from logging.handlers import TimedRotatingFileHandler
def create_timed_rotating_log(path):
    logger = logging.getLogger ("Rotating Log") 
    logger.setlevel(logging.INFO)
    handler = TimedRotatingFileHandler(path,
                                        when="${FREQUENCY}", 
                                        interval=${INTERVAL}, 
                                        backupCount=${RETENTION})
    logger.addHandler(handler)
    now = datetime.datetime.now()
    if now.hour == 00:
        logger.info("log has been rotated")
        time. sleep(3000)
    else:
        time. sleep(5)
def Convert(string):
    if "*" in string:
        li = glob.glob(string)
    else:
        li = list(string.split(","))
    return li
if __name__ == "__main__":
    log_file = "${LOG_NAME]" 
    list = Convert(log_file)
    procs = []
    proc = Process(target=create_timed_rotating_log)
    procs.append(proc)
    proc.start()
    while True:
        for file in list:
            proc = Process (target=create_timed_rotating_log, args=(file,))
            procs.append(proc)
            proc.start()
        for proc in procs: 
            proc.join()

EOF
;;
sizebased)
BYTES=${BYTES:=10485760}
cat > ${WORKING_DIRECTORY}/${ROTATE_SCRIPT} <<EOF
import logging
import time
import os
import glob
from multiprocessing import Process
from logging.handlers import RotatingFileHandler
def create_rotating_log(path): 
    logger = logging.getLogger("Rotating Log")
    logger.setLevel(logging.INFO)
    handler = RotatingFileHandler(path, maxBytes=${BYTES),
                                        backupCount=${RETENTION})
    logger.addHandler(handler)
    check = os.path.getsize(path) 
    if check > ${BYTES}:
        logger.info("log has been rotated")
    time.sleep(1800)
def Convert(string):
    if "*" in string:
        li = glob.glob(string)
    else:
        li = list(string.split(","))
    return li
if __name_ _==  __main__":
    log_file = "${LOG_NAME}"
    list = Convert(log_file)
    procs = []
    proc = Process(target=create_rotating_log)
    procs.append(proc)
    proc.start()
    while True:
        for file in list:
            print(file)
            proc = Process(target=create_rotating_log, args=(file,))
            procs.append(proc)
            proc.start()
        for proc in procs:
            proc.join()

EOF
;;

*)
echo "Oops seems like $ROTATE_METHOD env was wrongly set - use choose either sizebased or timebased"
exit 1
esac
chmod 755 ${WORKING_DIRECTORY}/${FROTATE_SCRIPT} && echo "Starting log rotate process in background" 
${PY_CMD} ${WORKING_DIRECTORY}/${ROTATE_SCRIPT} &

fi
# End of log rotation part of script

# Automated config generation can be disabled with GENERATE_CONFIG=FALSE, or bypassing docker-entrypoint.sh entirely.
if ! ${GENERATE_CONFIG:=true}; then
    echo "GENERATE_CONFIG is false, skip config generation..."
    exec "$@"
    exit 0
fi

SEAL_ID=$( echo "$K8S_NAMESPACE" |cut -d- -f1 )

CONFIG_DIRECTORY=${CONFIG_DIRECTORY:=/usr/local/etc/fluent-bit}
ENABLE_MULTILINE_INPUT=${ENABLE_MULTILINE_INPUT:=false}
PARSER NAME=${PARSER_NAME:=bmw_multiline}
SERVICE_CONFIG="${CONFIG_DIRECTORY}/fluent-bit.conf" && echo "" > $SERVICE_CONFIG
INPUT_CONFIG="${CONFIG_DIRECTORY}/input.conf" && echo ""> $INPUT_CONFIG
FILTER_CONFIG="${CONFIG_DIRECTORY}/filter.conf" && echo " > $FILTER_CONFIG 
OUTPUT_CONFIG="${CONFIG_DIRECTORY}/output.conf" && echo "" > $OUTPUT_CONFIG

# Create the main fluent-bit config file, with include statements for Input / Output Filter configs.
cat > ${SERVICE_CONFIG} <<EOF
[SERVICE]
    # Set an interval of seconds before to flush records to a destination
    Flush       ${FLUSH_INTERVAL:=5}

    Parsers_File /etc/fluent-bit/parsers.conf
    
    # Instruct Fluent Bit to run in foreground or background mode.
    Daemon      Off

    # By default 'info' is set, that means it includes 'error' and 'warning'.
    Log_Level       ${LOG_LEVEL:=info}

EOF

# Enable / disable the http metric endpoint.
if [ ${ENABLE_HTTP:=true} ]; then 
    cat >> ${SERVICE_CONFIG} <<EOF
    # Enable/Disable the built-in HTTP Server for metrics
    HTTP_Server On
    HTTP_Listen ${HTTP_LISTENER :=0.0.0.0}
    HTTP_Port   ${HTTP_PORT:=2020}

EOF
fi


# Enable / Disable an Input source tail with path specified by LOG_PATH/Type of files. 
if ([ ${ENABLE_TAIL_INPUT:=true} ] && [ ${ENABLE_MULTILINE_INPUT} == false ]); then 
    cat >> ${INPUT_CONFIG} <<EOF

[INPUT]
    Name   tail
    Path    ${LOG_NAME}
    Key     message
    DB      /tmp/log.db
    
EOF
fi

# Enable / Disable Multiline Input source tail with path specified by LOG_PATH/Type of files.
if [ ${ENABLE_MULTILINE_INPUT} == true ]; then
    cat >> S{INPUT_CONFIG} <<EOF

[INPUT]
    Name   tail
    Path    ${LOG_NAME}
    Key     message
    DB      /tmp/log.db
    Multiline   on
    Parser_Firstline  ${PARSER_NAME}

EOF
fi

# Enable / Disable TCP Input source. 
if [ ${ENABLE_TCP_INPUT:=true} ]; then cat >> ${INPUT_CONFIG} <<EOF

[INPUT]
    Name    tcp
    Port    ${TCP_PORT:= "5170"}

EOF
fi

cat >> ${FILETR_CONFIG} <<EOF
[FILTER]
    Name    record_modifier
    Match *
    Record Kuberbetes.node ${K8S_NODE_NAME}
    Record Kuberbetes.pod ${K8S_POD_NAME}
    Record Kuberbetes.namespace ${K8S_SPACENAME}
    Record app_id ${SEAL_ID}-security

EOF

# Enable / Disable a filter to print all tailed logs to stdout.
if [ ${ENABLE_STDOUT_FILTER:=true} ]; then
    cat >> ${FILTER_CONFIG} <<EOF
[FILTER]
    Name    stdout
    Match   *

EOF
fi

# Enable / Disable a kafka output.
if [ ${ENABLE_KAFKA_OUT:=true} ]; then
    cat >> ${OUTPUT_CONFIG} <<EOF
[OUTPUT]
    Name        kafka
    Match       *
    Timestamp_Format  iso8601
    Brokers     ${KAFKA_BROKERS:="<set kafkabrokers>"}
    Topics      ${KAFKA_TOPIC:="<set kafka topic>"}
    Format      json
    Message_key message

EOF
fi

# Enable / Disab;e SSL based kafka authentication
if([ ${ENABLE_KAFKA_OUT:=true} ] && [ ${KAFKA_SECURITY:=ssl} == ssl ]); then
    cat >> ${OUTPUT_CONFIG} <<EOF
    rdkafka.security.protocol           ${KAFKA_SECURITY:=ssl}
    rdkafka.ssl.certificate.location    ${KAFKA_CLIENT_CERT_PATH:=/etc/fluent-bit/certs/client.cer}
    rdkafka.ssl.key.location            ${KAFKA_CLIENT_KEY_PATH:=/etc/fluent-bit/certs/client.key}
    rdkafka.ssl.ca.location             ${KAFKA_ROOT_CA_PATH:=/etc/fluent-bit/certs/roor_ca.pem}

EOF
fi

# Enable / Disable SASL SSL based kafka authentication
if([ ${ENABLE_KAFKA_OUT:=true} ] && [ ${KAFKA_SECURITY:=ssl} == ssl ]); then
    cat >> ${OUTPUT_CONFIG} <<EOF
    rdkafka.security.protocol           ${KAFKA_SECURITY:=ssl}
    rdkafka.sasl.kerberos.principal     ${KAFKA_KRB_PRINCIPAL:=ssl}
    rdkafka.sasl.kerberos.service.name  ${KAFKA_KRB_SERVICE_NAME:=kafkaclient}
    rdkafka.sasl.kerberos.keytab        ${KAFKA_KRB_KEYTAB_PATH:=/etc/fluent-bit/certs/client.keytab}
    rdkafka.ssl.ca.location            ${KAFKA_ROOT_CA_PATH:=/etc/fluent-bit/certs/roor_ca.pem}

EOF
fi

echo "Running with generated configfile:"

cat ${INPUT_CONFIG} >> ${SERVICE_CONFIG}
cat ${FILTER_CONFIG} >> ${SERVICE_CONFIG}
cat ${OUTPUT_CONFIG} >> ${SERVICE_CONFIG}
${SERVICE_CONFIG}

exec /usr/local/bin/fluent-bit -c /etc/fluent-bit.conf
