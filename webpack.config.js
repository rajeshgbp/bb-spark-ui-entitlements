/* eslint-env node */
const webpack = require('webpack')
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: path.resolve(__dirname, 'src/bb-rentpay-ui-admin.js'),
  output: {
    filename: 'bb-rentpay-ui-admin.js',
    library: 'bb-rentpay-ui-admin',
    libraryTarget: 'amd',
    path: path.resolve(__dirname, 'build/bb-rentpay-ui-admin'),
  },
  mode: 'production',
  module: {
    rules: [
      {parser: {System: false}},
      {
        test: /\.js?$/,
        exclude: [path.resolve(__dirname, 'node_modules')],
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        exclude: [path.resolve(__dirname, 'node_modules'), /\.krem.css$/],
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: '[path][name]__[local]',
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins() {
                return [
                  require('autoprefixer')
                ];
              },
            },
          },
        ],
      },
      {
        test: /\.css$/,
        include: [path.resolve(__dirname, 'node_modules')],
        exclude: [/\.krem.css$/],
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.krem.css$/,
        exclude: [path.resolve(__dirname, 'node_modules')],
        use: [
          {
            loader: 'kremling-loader',
            options: {
              namespace: 'bb-rentpay-ui-admin',
              postcss: {
                plugins: {
                  'autoprefixer': {}
                }
              }
            },
          },
        ]
      },
      {
        test: /\.(png|jp(e*)g|svg|woff|woff2|eot|ttf|gif|scss)$/,
        loader: "file-loader",
        options: {}
      }
    ]
  },
  resolve: {
    modules: [
      __dirname,
      'node_modules',
    ],
    alias: {
      public: path.join(__dirname, './public')
    }
  },
  plugins: [
    new CleanWebpackPlugin(['build/bb-rentpay-ui-admin']),
    CopyWebpackPlugin([
      {from: path.resolve(__dirname, 'src/bb-rentpay-ui-admin.js')}
    ]),
  ],
  devtool: 'source-map',
  externals: [
    /^@portal\/*/,
    /^lodash$/,
    /^single-spa$/,
    /^rxjs\/?.*$/,
    /^react$/,
    /^react\/lib.*/,
    /^react-dom$/,
    /.*react-dom.*/,
  ],
};

