const args = require('minimist')(process.argv.slice(2));

let config_file = args['RENTPAY_CONFIG'] || process.env.RENTPAY_CONFIG || './config.json';
const config = require(config_file);

const environment = args['RENTPAY_DEPLOYMENT_MODE'] || process.env.RENTPAY_DEPLOYMENT_MODE || 'development';
const environmentConfig = config[environment];

module.exports = {
    server_url: process.env.RENTPAY_SERVER_URL || environmentConfig.RENTPAY_SERVER_URL,
    port_number: args['PORT'] || process.env.PORT || process.env.port || environmentConfig.PORT || 4020,
};