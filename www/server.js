const config = require('./config');

var express = require('express');
var proxy = require('http-proxy-middleware');

var app = express();

let cookieParser = require('cookie-parser');
app.use(cookieParser()); 
var path = require('path');

// app.use('/api', proxy({ target: config.server_url, ignorePath: false, secure: false, changeOrigin: true }));

var myInit = async (req, res, next) =>
{
    if(req.headers.person_id)
    {
        res.cookie("person_id", req.headers.person_id, { httpOnly: false });
        next();
    }
    
    if (req.url.startsWith('/static/') || req.url.startsWith('/app/assets/') || req.url == '/manifest.json' || req.url == '/favicon.ico')
    {
        res.sendFile(path.join(__dirname, '/build/', req.url));
    }
    else
    {
        res.sendFile(path.join(__dirname, '/build/', 'index.html'));
    }

}

app.use(myInit);
    
app.use(express.static(__dirname + '/build'));

app.get('/healthcheck', (req, res) => 
{
    console.log('called healthcheck route');
    res.set('Content-Type', 'text/plain');
    res.send('SUCCESS').status(200);
});

app.listen(config.port_number);