const modules = require("../config/modules");
const moduleFileExtensions = [
    "web.mjs",
    "mjs",
    "web.js",
    "js",
    "web.ts",
    "ts",
    "web.tsx",
    "tsx",
    "json",
    "web.jsx",
    "jsx",
];

module.exports = (resolve, rootDir) =>
{
    const config = {
        collectCoverage: true,
        testResultsProcessor: "jest-sonar-reporter",
        collectCoverageFrom: [
            "src/**/*.{js,jsx,ts,tsx}",
            "!src/global.d.ts",
            "!src/history.ts",
            "!src/react-app-env.d.ts",
            "!src/index.tsx",
            "!src/serviceWorker.js",
            "!src/set-public-paths.js",
            "!src/index.js",
            "!src/bb-rentpay-ui-admin.js",
            "!src/common/serviceWorker.js",
            "!src/layouts/**/*.{js,jsx,ts,tsx}",
            "!www/*",
        ],
        coveragePathIgnorePatterns: ["/__tests__/", "/node_modules/", "config", "log", "public"],
        coverageReporters: ["text", "lcov"],
        reporters: ["default", "jest-junit"],
        coverageThreshold: {
            global: {
                branches: 60,
                functions: 70,
                lines: 70,
                statements: 70
            },
        },
        setupFiles: ["react-app-polyfill/jsdom"],
        setupFilesAfterEnv: [],
        snapshotSerializers: ["enzyme-to-json/serializer"],
        testMatch: [
            "<rootDir>/src/**/__tests__/**/*.test.{js,jsx,ts,tsx}",
            "<rootDir>/src/**/__tests__/**/*.spec.{js,jsx,ts,tsx}",
			"<rootDir>/src/**/*.{spec,test}.{js,jsx,ts,tsx}"
        ],
        testEnvironment: "jest-environment-jsdom-sixteen",
        transform: {
            "^.+\\.(js|jsx|ts|tsx)$": "<rootDir>/node_modules/babel-jest",
            "^.+\\.css$": resolve("config/jest/cssTransform.js"),
            "^(?!.*\\.(js|jsx|ts|tsx|css|json)$)": resolve("config/jest/fileTransform.js"),
        },
        transformIgnorePatterns: [
            "[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$",
            "^.+\\.module\\.(css|sass|scss)$",
        ],
        modulePaths: modules.additionalModulePaths || [],
        moduleNameMapper: {
            "^.+\\.module\\.(css|sass|scss)$": "identity-obj-proxy"
        },
        moduleFileExtensions: [...moduleFileExtensions, "node"].filter(ext => !ext.includes("mjs")),
        watchPlugins: ["jest-watch-typeahead/filename", "jest-watch-typeahead/testname"],
        verbose: true,
    };

    if (rootDir)
    {
        config.rootDir = rootDir;
    }

    return config;
};