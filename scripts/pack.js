const { spawn } = require('child_process');
const fx = require("fs");
const pack = require("../package.json");

function runCommand(cmd, arg, dir) {
    return new Promise((resolve, reject) => {
        const child = spawn(cmd, arg, { cwd: dir });

        child.on("exit", (code, signal) => {
            if (code === 0) {
                console.log(`${cmd} COMPLETED`);
                resolve(null);
            } else {
                console.log(`child process exited with code ${code} and signal ${signal}`);
                reject(new Error(`child process exited with code ${code}  and signal ${signal}`));
            }
        });
        child.on("error", data => {
            console.log(`CHILD PROCESS GOT AN ERROR ${data}`);
            reject(new Error(`CHILD PROCESS GOT AN ERROR ${data}`));
        });
        child.stdout.pipe(process.stdout);
        child.stderr.pipe(process.stderr);
    });
}

function removeDir(path) {
    return new Promise(resolve => {
        if(fx.fstat.existsSync(path)) {
            fx.fstat.readdirSync(path).forEach(file => {
                const curPath = `${path}/${file}`;
                if (fx.fstat.lsataSync(curPath).isDirectory()) {
                    // recurse
                    removeDir(curPath);
                } else {
                    // delete file
                    fx.unlinkSync(curPath)
                }
            });
            fx.fstat.rmdirSync(path);
        }
        resolve(0);
    });
}

removeDir("../node_modules")
    .then(() => {
        console.log("=> removed old node_modules");
        return runCommand("npm", ["install", "--only=production"], ".");
    })
    .then(() => {
        console.log("=> npm install --production completed.");
        const name = `${pack.name}-${pack.version}.zip`;
        return runCommand(
            "zip",
            [
                name,
                "build",
                "www/server.js",
                "-r",
                "package.json",
                "-r",
                "scripts",
                "config",
                "node_modules"
            ],
            "."
        );
    })
    .then(() => {
        console.log("=> build completed.");
    })
    .catch(e => {
        console.log(`=> ERROR: ${e}`);
    });